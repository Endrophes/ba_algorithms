package tests;

import static org.junit.Assert.*;
import binaryTree.BinaryTree;
import org.junit.Test;

public class BinaryTreeTest {

	BinaryTree<Integer> tree = new BinaryTree<Integer>();
	
	@Test
	public void start() {
		assert(tree != null);
	}
	
	@Test
	public void insertOneValue() {
		
		tree.insert(1);
		
		assert(tree != null);
	}

}
