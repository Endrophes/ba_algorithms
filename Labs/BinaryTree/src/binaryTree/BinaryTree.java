package binaryTree;

public class BinaryTree<T extends Comparable<T>> {

	private class Node<T extends Comparable<T>>
	{
		T value;
		Node left;
		Node right;
		
		Node(T newVal)
		{
			value = newVal;
		}
		
		T getValue()
		{
			return value;
		}
		
		void setValue(T newValue)
		{
			value = newValue;
		}
		
		Node getLeft()
		{
			return left;
		}
		
		Node getRight()
		{
			return right;
		}
		
		void setLeft(Node newLeft)
		{
			left = newLeft;
		}
		
		void setRight(Node newRight)
		{
			right = newRight;
		}
		
	}
	
	Node root;
	int size;
	
	private boolean insertHelper(Node ancestor, Node currentNode, Node toInsert)
	{
		boolean result = true;
		
		if(currentNode != null)
		{
			int compare = currentNode.getValue().compareTo(toInsert.getValue());
			
			if(compare > 0)
			{
				result = insertHelper(currentNode, currentNode.getRight(), toInsert);
			}
			else if(compare < 0)
			{
				result = insertHelper(currentNode, currentNode.getLeft(), toInsert);
			}
			else
			{
				result = false;
			}
		}
		else
		{
			int compare = ancestor.getValue().compareTo(toInsert.getValue());
			
			if(compare > 0)
			{
				ancestor.setRight(toInsert);
			}
			else if(compare < 0)
			{
				ancestor.setLeft(toInsert);
			}
		}
		
		return result;
	}
	
	private Node rightRotation(Node subRoot)
	{
		Node pivot = subRoot.getLeft();
		subRoot.setLeft(pivot.getRight());
		pivot.setRight(subRoot);
		return pivot;
	}
	
	private Node leftRotatoion(Node subRoot)
	{
		Node pivot = subRoot.getRight();
		subRoot.setRight(pivot.getRight());
		pivot.setLeft(subRoot);
		return pivot;
	}
	
	public boolean insert(T value)
	{
		boolean result = false;
		
		Node newGuy = new Node(value);
		
		if(root != null)
		{
			Node currentNode = root;
			result = insertHelper(currentNode, currentNode, newGuy);
		}
		else
		{
			root = newGuy;
		}
		
		size++;
		result = true;
		return result;
	}
	
	public int getSize()
	{
		return size;
	}
}
