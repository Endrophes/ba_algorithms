package linkedList;

public class LinkedList<T> {
	
	private boolean fixedSize;
	private int maxSize;
	private int currentSize;
	
	private Node head;
	private Node tail;
	
	public class Node<T>
	{
		private T value;
		private Node next;
		
		Node(T info)
		{
			value = info;
		}
		
		Node(T info, Node placeAfter)
		{
			value = info;
			next = placeAfter;
		}

		public void replaceValue(T newValue)
		{
			value = newValue;
		}
		
		public void setNextLink(Node nextTarget)
		{
			next = nextTarget;
		}

		public Node getNextNode()
		{
			return next;
		}
	
		public T getValue()
		{
			return value;
		}
	}
	
	public LinkedList()
	{
		fixedSize = false;
		maxSize = 1;
	}
	
	public LinkedList(boolean fixed, int maxNumber)
	{
		fixedSize = fixed;
		maxSize = maxNumber;
	}
	
	public void makeFixed(int maxNumber)
	{
		fixedSize = true;
		maxSize = maxNumber;
	}
	
	public boolean add(T value)
	{
		boolean worked = false;
		
		if(fixedSize)
		{
			if(currentSize < maxSize)
			{
				if(head != null && tail != null)
				{
					Node newNode = new Node(value);
					head = newNode;
					tail = newNode;
				}
				else
				{
					Node newNode = new Node(value);
					tail.setNextLink(newNode);
					tail = newNode;
				}
				worked = true;
				currentSize++;
			}
		}
		else
		{
			if(head != null && tail != null)
			{
				Node newNode = new Node(value);
				head = newNode;
				tail = newNode;
			}
			else
			{
				Node newNode = new Node(value);
				tail.setNextLink(newNode);
				tail = newNode;
			}
			worked = true;
			currentSize++;
		}
		
		
		return worked;
	}
	
	public T pop()
	{
		T value = (T) head.getValue();
		Node temp = head.getNextNode();
		head.setNextLink(null);
		head = temp;
		return value;
	}
	
	public boolean replaceValue(int AtIndex, T newValue)
	{
	 	boolean valueReplaced = false;
	 	
	 	if(AtIndex < currentSize)
	 	{
	 		Node currentNode = head;
	 		
	 		for(int step = 0; step < currentSize && step != AtIndex; step++)
	 		{
	 			currentNode = currentNode.getNextNode();
	 		}
	 		
	 		currentNode.replaceValue(newValue);
	 		
	 		valueReplaced = true;
	 	}
	 	else
	 	{
	 		//throw an exception
	 	}
	 	
	 	return valueReplaced;
	 }
	
	public boolean addAtIndex(int AtIndex, T newValue)
	{
		boolean worked = false;
		
		if(AtIndex < currentSize)
	 	{
			Node lastNode = head;
	 		Node currentNode = head;
	 		Node nodeAfter = head.getNextNode();
	 		
	 		for(int step = 0; step != AtIndex; step++)
	 		{
	 			lastNode = currentNode;
	 			currentNode = currentNode.getNextNode();
	 			nodeAfter = currentNode.getNextNode();
	 		}
	 		
	 		Node newNode = new Node(newValue, currentNode);
	 		
	 		lastNode.setNextLink(newNode);
	 		
	 		currentSize++;
	 		
	 		worked = true;
	 	}
	 	else
	 	{
	 		//throw an exception
	 	}
		
		return worked;
	}
	
	public boolean removeAtIndex(int AtIndex)
	{
		boolean worked = false;
		
		if(AtIndex < currentSize)
	 	{
			Node lastNode = head;
	 		Node currentNode = head;
	 		Node nodeAfter = head.getNextNode();
	 		
	 		for(int step = 0; step != AtIndex; step++)
	 		{
	 			lastNode = currentNode;
	 			currentNode = currentNode.getNextNode();
	 			nodeAfter = currentNode.getNextNode();
	 		}
	 	
 			lastNode.setNextLink(nodeAfter);
 			
 			//T value = (T) currentNode.getValue();
 			
 			currentNode.setNextLink(null);
 			
	 		worked = true;
	 	}
	 	else
	 	{
	 		//throw an exception
	 	}
		
		return worked;
	}
	
}
