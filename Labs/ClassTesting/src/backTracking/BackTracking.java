package backTracking;

import java.util.ArrayList;
import java.util.List;

public class BackTracking {
	public List<List<Integer>> findPermutations(List array)
	{
		List<List<Integer>> results = new ArrayList<List<Integer>>();
		
		List soFar = new ArrayList();
		List toGo = new ArrayList(array);
		
		findPermutationsHelper(soFar, toGo, results);
		
		return results;
	}
	
	public List findPermutationsHelper(List soFar, List toGo, List successes)
	{
		if(toGo.isEmpty())
		{
			successes.add(soFar);
		}
		else
		{
			for(int index = 0; index < toGo.size(); index++)
			{
				Integer i = (Integer) toGo.get(index);
				
				List newSoFar = new ArrayList(soFar);
				List newToGo = new ArrayList(toGo); ///need to change
				newSoFar.add(i);
				newToGo.remove(index);
				
				findPermutationsHelper( newSoFar, newToGo, successes);
			}
		}
		return successes;
		
	}
}
