package backTracking;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class backtrackingTest {

	BackTracking backTraker;
	
	@Test
	public void test() {
		backTraker = new BackTracking();
		test1();
	}
	
	public void test1()
	{
		List arrayOfInts = new ArrayList();
		arrayOfInts.add(1);
		arrayOfInts.add(2);
		arrayOfInts.add(3);
		arrayOfInts.add(4);
		
		List results = backTraker.findPermutations(arrayOfInts);
		
		for(int stepA = 0; stepA < results.size(); stepA++)
		{
			List currentCunck = (List) results.get(stepA);
			
			for(int stepB = 0; stepB < currentCunck.size(); stepB++)
			{
				System.out.print(currentCunck.get(stepB));
			}
			
			System.out.println("");
		}
	}

}
