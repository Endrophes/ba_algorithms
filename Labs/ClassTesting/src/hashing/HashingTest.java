package hashing;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class HashingTest
{

	Hashing hasher = new Hashing();

	@Test
	public void test()
	{
		ArrayList<String> collection = new ArrayList();
		
		collection.add("Bastien");
		collection.add("Corbin");
		collection.add("Waite");
		collection.add("Wright");
		
		for(String var : collection)
		{
			System.out.println(var + " : " + hasher.hash(var));
		}
	}

}
