package Fip;

import java.util.ArrayList;

public class fipnoch
{

	ArrayList<Integer> seq;
	int currentLength = 0;
	
	public fipnoch()
	{
		seq = new ArrayList<Integer>();
		buider(10,10,0);
	}
	
	public int getNumberInSequence(int n)
	{	
		int results = 0;
		//build
		if(n > currentLength)
		{
			buider(currentLength, n + 1, 0);
			results = seq.get(n);
		}
		else
		{
			results = seq.get(n);
		}
		return 0;
	}
	
	
	public void buider(int currentIndex, int goTo, int currentStep)
	{
		if(currentStep < goTo)
		{
			if(currentIndex == 0)
			{
				seq.add(0);
			}
			else if(currentIndex == 1)
			{
				seq.add(1);
			}
			else
			{
				int v1 = seq.get(currentIndex - 2);
				int v2 = seq.get(currentIndex - 1);
				
				int newValue = v1 + v2;
				seq.add(currentIndex, newValue);
			}
			
			currentLength++;
			buider(currentIndex + 1, goTo, currentStep + 1);
		}
	}
}
