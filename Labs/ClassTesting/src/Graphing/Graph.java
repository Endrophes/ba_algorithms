package Graphing;

public class Graph
{
	private int[][] matrix; //for tracking edges
	private int[] marks; // for coloring vertices
	
	public Graph(int n)
	{
		matrix = new int[n][n];
		marks = new int[n];
	}
	
	public int vCount()
	{
		return marks.length;
	}
	
	public int first(int v)
	{
		int count = vCount();
		int result = count;
		for(int i = 0; i < count; i++)
		{
			if(matrix[v][i] != 0)
			{
				result = i;
			}
		}
		
		return result;
	}
	
	public int next(int vertex, int lastVisitedNeighbor)
	{
		int count = vCount();
		int result = count;
		for(int i = lastVisitedNeighbor + 1; i < count; i++)
		{
			if(matrix[vertex][i] != 0)
			{
				result = i;
			}
		}
		
		return result;
	}
	
	public void addEdge(int vertex, int neighbor, int weight)
	{
		matrix[vertex][neighbor] = weight;
	}
	
	public void removeEdge(int vertex, int neighbor)
	{
		matrix[vertex][neighbor] = 0;
	}
	
	public boolean isEdge(int vertex, int neighbor)
	{
		return matrix[vertex][neighbor] != 0;
	}
	
	public int getMark(int vertex)
	{
		return marks[vertex];
	}
	
	public void setMark(int vertex, int mark)
	{
		marks[vertex] = mark;
	}
}
