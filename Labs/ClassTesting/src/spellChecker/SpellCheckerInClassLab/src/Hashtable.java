package spellChecker.SpellCheckerInClassLab.src;

import java.util.Iterator;
import java.util.function.Consumer;


public class Hashtable<K, V> {
	
	private static class Entry<K, V> {
		private K key;
		private V data;
		private Entry<K,V> next;
		
		public Entry(K key, V data) {
			this.key = key;
			this.data = data;
		}
		
		public String toString() {
			return "{" + key + "=" + data + "}";
		}
	}
	
	private Entry<K,V>[] values;
	private int size;
	
	public Hashtable(int initialCapacity) {
		values = (Entry<K,V>[])new Entry[initialCapacity];
	}
	
	/**
	 * #3b. Implement this (1 point)
	 * 
	 * 1. calculate hash
	 * 2. if no collision, add it it in the tree, else find the the place in the list to put
	 * 3. inc size
	 * 
	 * @param key
	 * @param value
	 */
	public void put(K key, V value) {
				
		Entry newEntry = new Entry(key, value);
		
		int hash = key.hashCode();
		int index = hash % values.length;
		
		Entry currentList = values[index];
		
		if(currentList == null)
		{
			values[index] = newEntry;
			size++;
		}
		else
		{
			findPlace(currentList, newEntry);
		}
		
	}
	
	private void findPlace(Entry list, Entry newGuy)
	{	
		if(list.key == newGuy.key)
		{
			list.data  = newGuy.data;
		}
		else if(list.next == null)
		{
			list.next = newGuy;
			size++;
		}
		else
		{
			findPlace(list.next, newGuy);
		}
	}
	
	/**
	 * #3b. Implement this (1 point)
	 * @param key
	 * @return
	 */
	public V get(K key) {
		
		int hash = key.hashCode();
		int index = hash % values.length;
		
		Entry currentList = values[index];
		
		V target = null;
		
		if(currentList != null)
		{
			if(currentList.key == key)
			{
				target = (V) currentList.data;
			}
			else
			{
				target = findTarget(key, currentList);
			}
		}
		
		return target;
	}
	
	private V findTarget(K key, Entry current)
	{
		V object = null;
		
		if(current.key == key)
		{
			object = (V) current.data;
		}
		else if(current.next != null)
		{
			object = findTarget(key, current.next);
		}
		
		
		return object;
	}

	/**
	 * #3c.  Implement this. (1 point)
	 * @param key
	 * @return
	 */
	public V remove(K key) {
		
		int hash = key.hashCode();
		int index = hash % values.length;
		
		Entry currentList = values[index];

		V target = null;
		
		if(currentList != null)
		{
			if(currentList.key == key)
			{
				target = (V) currentList.data;
				values[index] = currentList.next;
			}
			else
			{
				target = findAndRemove(currentList, currentList.next, key);
			}
		}
		
		return target;
	}
	
	private V findAndRemove(Entry parent, Entry current, K key)
	{
		V target = null;
		
		if(current.key == key)
		{
			parent.next = current.next;
			target = (V)current.data;
		}
		else if(current.next != null)
		{
			target = findAndRemove(current, current.next, key);
		}
		
		return target;
	}
	
	public int size() {
		return size;
	}
	
	public boolean containsKey(K key) {
		return this.get(key) != null; 
	}

	public Iterator<V> values() {
		return new Iterator<V>() {
			private int count = 0;
			private Entry<K, V> currentEntry;
			
			{
				while ( ( currentEntry = values[count] ) == null && count < values.length ) {
					count++;
				}
			}
			
			@Override
			public void forEachRemaining(Consumer<? super V> arg0) {
			}

			@Override
			public boolean hasNext() {
				return count < values.length;
			}

			@Override
			public V next() {
				V toReturn = currentEntry.data;
				currentEntry = currentEntry.next;
				while ( currentEntry == null && ++count < values.length && (currentEntry = values[count]) == null );
				return toReturn;
			}

			@Override
			public void remove() {
			}
			
		};
	}
}