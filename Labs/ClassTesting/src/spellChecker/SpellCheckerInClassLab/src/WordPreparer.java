package spellChecker.SpellCheckerInClassLab.src;

public interface WordPreparer {
	Word prepare(String word);
}
