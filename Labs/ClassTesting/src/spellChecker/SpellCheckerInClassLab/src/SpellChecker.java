package spellChecker.SpellCheckerInClassLab.src;

public interface SpellChecker {
	SpellCorrection checkWord(String word);
}
