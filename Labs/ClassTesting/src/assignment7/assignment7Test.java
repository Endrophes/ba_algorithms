package assignment7;

import static org.junit.Assert.*;

import org.junit.Test;

public class assignment7Test {
	
	private boolean checkIfSorted(int[] array)
	{
		boolean results = true;
		int length = array.length;
		
		for(int step = 0; step < length - 1; step++)
		{
			if(array[step] > array[step + 1])
			{
				results = false;
				break;
			}
		}
		
		return results;
	}
	
	@Test
	public void bubbleSortTest() {
		System.out.println("BubbleSort Test");
		int prime[] = {5,10,11,4,2};
		Assignment7 test = new Assignment7();
		prime = test.bubbleSort(prime);
		boolean result = checkIfSorted(prime);
		assert(result == true);
	}
	
	@Test
	public void selectSortTest() {
		System.out.println("selectSort Test");
		int prime[] = {5,10,11,4,2};
		Assignment7 test = new Assignment7();
		prime = test.selectionSort(prime);
		boolean result = checkIfSorted(prime);
		assert(result == true);
	}
	
	@Test
	public void insertionSortTest() {
		System.out.println("insertionSort Test");
		int prime[] = {5,10,11,4,2};
		Assignment7 test = new Assignment7();
		prime = test.insertionSort(prime);
		boolean result = checkIfSorted(prime);
		assert(result == true);
	}

}
