package assignment7;

public class Assignment7 {
	
	private void printArray(Object[] array)
	{
		for(Object var : array)
		{
			System.out.print(var.toString() + ", ");
		}
	}
	
	private void printArray(int[] array)
	{
		for(Object var : array)
		{
			System.out.print(var + ", ");
		}
		System.out.println();
	}
	
	private boolean checkIfSorted(int[] array)
	{
		boolean results = true;
		int length = array.length;
		
		for(int step = 0; step < length - 1; step++)
		{
			if(array[step] > array[step + 1])
			{
				results = false;
				break;
			}
		}
		
		return results;
	}
	
	public int[] bubbleSort(int[] array)
	{
		int start = 0;
		int target = 0;
		//bubbleHelper(start, target, array);
		int length = array.length;
		
		boolean sorted = checkIfSorted(array);
		
		int stopper = 0;
		while(!sorted)
		{
			for(int step = 0; step < length - 1; step++)
			{
				if(array[step] > array[step + 1])
				{
					int temp =  array[step + 1];
					array[step + 1] = array[step];
					array[step] = temp;
					printArray(array);
				}
			}
			stopper++;
			sorted = checkIfSorted(array);
		}
		return array;
	}
	
	public int[] selectionSort(int[] array)
	{
		int indexMark = 0;
		int length = array.length;
		boolean isSorted = false;
		
		while(!isSorted && indexMark < length)
		{
			int minindex = indexMark;
			int minvalue = array[indexMark];
			
			for(int step = indexMark + 1; step < length; step++)
			{
				int currentValue = array[step];
				if(currentValue < minvalue)
				{
					minindex = step;
					minvalue = currentValue;
				}
			}
			
			//Swap LOL
			int temp = array[indexMark];
			array[indexMark] = minvalue;
			array[minindex] = temp;
			
			indexMark++;
			printArray(array);
			isSorted = checkIfSorted(array);
		}
		
		return array;
	}
		
	private int findIndex(int[] array, int k)
	{
		int index = 0;
		int length = array.length;
		if(length != 0)
		{
			int start = length - 1;
			int end = -1;
			for(int step = start; step > end; step--)
			{
				int value = array[step];
				if(k > value)
				{
					index = step;
					break;
				}
			}
		}
		//System.out.println(index);
		return index;
	}
	
	private int[] insert(int[] array, int atIndex, int k)
	{
		int length = array.length - 1;
		if(length != 0)
		{
			int tempA = array[atIndex];
			int tempB = 0;
			array[atIndex] = k;
			
			int start = atIndex - 1;
			int end = -1;
			for(int step = start; step > end; step--)
			{
				tempB = array[step];
				array[step] = tempA;
				tempA = tempB;
			}
			
		}
		return array;
	}
	
	public int[] insertionSort(int[] array)
	{
		int[] sorted = new int[array.length];
		
		for(int var : array)
		{
			int index = findIndex(sorted, var);
			sorted = insert(sorted, index, var);
			printArray(sorted);
		}
		
		return sorted;
	}
	
}
