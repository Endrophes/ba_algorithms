package lab;

import edu.neumont.ui.Picture;

import java.awt.Color;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SeamCarver
{
	Picture currentPic;
	
	//Map<Integer, Pixel> pixelsSets = new HashMap();
	int yZero = 0;
	Pixel[][] pixels;
	
	//Consts
	
	static final int RIGHT = 1;
	static final int LEFT = 2;
	static final int MID = 3;
	static final int DOWN = 4;
	static final int UP = 5;
	
	private class Pixel
	{
		double energy = 0;
		int index = 0;
		int x = 0;
		int y = 0;
		int red = 0;
		int green = 0;
		int blue = 0;
		Color color;
		boolean removed = false;
		
		Pixel parent = null;
		double pathTotalEnergy = 0;
		
		public Pixel()
		{
			
		}
		
		public void getColorValues()
		{
			color = currentPic.get(x, y);
			red = color.getRed();
			green = color.getGreen();
			blue = color.getBlue();
		}
	}
	
	private class PixelPath
	{
		double totalEnergy = 0;
		ArrayList<Pixel> path = new ArrayList<Pixel>();
	}
	
	private class Coordnets
	{
		int x = 0;
		int y = 0;
		
		public Coordnets(){}
		public Coordnets(int X, int Y)
		{
			x = X;
			y = Y;
		}
		
		public void grabAndSetValues(Coordnets set)
		{
			x = set.x;
			y = set.y;
		}
	}

	//Helper functions
	
	private int coordnetsToIndex(int x, int y)
	{
		int result = (y  * currentPic.width()) + x;
		return result;
	}
	
	private int coordnetsToIndex(Coordnets a)
	{
		
		int result = (a.y  * currentPic.width()) + a.x;
		return result;
	}
	
	private Coordnets indextoCordnets(int index)
	{
		int width = currentPic.width();
		int x = index % width;
		int y = index / width;
		Coordnets newSet = new Coordnets(x, y);
		return newSet;
	}
	
	private void reset(Picture pic)
	{
		currentPic = pic;
		pixels = new Pixel[pic.width()][pic.height()];
		pixelSetup();
		//Calculate energy for all pixels
	}
	
	private void pixelSetup()
	{
		int width = currentPic.width();
		int height = currentPic.height();
		int currentIndex = 0;
		
		for(int stepHeight = 0; stepHeight < height; stepHeight++)
		{
			for(int stepWidth = 0; stepWidth < width; stepWidth++)
			{
				Pixel currentPixel = new Pixel();
				currentPixel.x = stepWidth;
				currentPixel.y = stepHeight;
				currentPixel.index = currentIndex++;
				currentPixel.getColorValues();
				pixels[stepWidth][stepHeight] = currentPixel;
			}
		}
		
		for(int stepHeight = 0; stepHeight < height; stepHeight++)
		{
			for(int stepWidth = 0; stepWidth < width; stepWidth++)
			{
				calculateEnergy(pixels[stepWidth][stepHeight]);
			}
		}
	}
	
	private void calculateEnergy(Pixel givenPixel)
	{
		int width = currentPic.width() - 1;
		int height = currentPic.height() - 1;
		
		int x = givenPixel.x;
		int y = givenPixel.y;
		
		Pixel left;
		Pixel right;
		Pixel up;
		Pixel down;
		
		if(x == width)
		{
			left = pixels[x-1][y];
			right = pixels[0][y]; 
		}
		else if(x == 0)
		{
			left = pixels[width][y];
			right = pixels[x+1][y];
		}
		else
		{
			left = pixels[x-1][y];
			right = pixels[x+1][y];
		}
		
		if(y == height)
		{
			up = pixels[x][y - 1];
			down = pixels[x][ 0 ];
		}
		else if(y == 0)
		{
			up = pixels[x][height];
			down = pixels[x][y + 1];
		}
		else
		{
			up = pixels[x][y - 1];
			down = pixels[x][y + 1];
		}
		
		
		double deltaRed = 0;
		double deltaGreen = 0;
		double deletaBlue = 0;
		
		deltaRed   = left.red   - right.red;
		deltaGreen = left.green - right.green;
		deletaBlue = left.blue  - right.blue;
		
		deltaRed   = Math.pow(deltaRed,   2.0);
		deltaGreen = Math.pow(deltaGreen, 2.0);
		deletaBlue = Math.pow(deletaBlue, 2.0);
		
		double deltaX = deltaRed + deltaGreen + deletaBlue;
		
		deltaRed   = up.red   - down.red;
		deltaGreen = up.green - down.green;
		deletaBlue = up.blue  - down.blue;
		
		deltaRed   = Math.pow(deltaRed,   2.0);
		deltaGreen = Math.pow(deltaGreen, 2.0);
		deletaBlue = Math.pow(deletaBlue, 2.0);
		
		double deltaY = deltaRed + deltaGreen + deletaBlue;
		
		double finalEnergy = deltaX + deltaY;
		
		givenPixel.energy = finalEnergy;
		
	}
	
	private int[] getConnectedIndexes(int currentIndex, int direction)
	{
		int[] indexs = null;
		
		Coordnets position = indextoCordnets(currentIndex);
		int width = currentPic.width();
		int height = currentPic.height();
		
		if(direction == DOWN)
		{
			if((position.y + 1) != height)
			{
				
				if(position.x == 0)
				{
					//left side
					indexs = new int[2];
					indexs[0] = currentIndex + (width);
					indexs[1] = currentIndex + (width + 1);
				}
				else if((position.x + 1) == width)
				{
					//right side
					indexs = new int[2];
					indexs[0] = currentIndex + (width - 1);
					indexs[1] = currentIndex + (width);
				}
				else
				{
					//mid
					indexs = new int[3];
					indexs[0] = currentIndex + (width);
					indexs[1] = currentIndex + (width - 1);
					indexs[2] = currentIndex + (width + 1);
				}
			}
			
		}
		else if(direction == LEFT)
		{
		
			if((position.x + 1) != width)
			{
				if(position.y == 0)
				{
					//top
					indexs = new int[2];
					indexs[0] = currentIndex + (1);
					indexs[1] = currentIndex + (width + 1);
				}
				else if((position.y + 1) == height)
				{
					//bottom
					indexs = new int[2];
					indexs[0] = currentIndex + (1);
					indexs[1] = currentIndex - (width - 1);
				}
				else
				{
					//mid
					indexs = new int[3];
					indexs[0] = currentIndex + (1);
					indexs[1] = currentIndex - (width - 1);
					indexs[2] = currentIndex + (width + 1);
				}
			}
		}
		else if(direction == RIGHT)
		{

			if((position.x - 1) >= 0)
			{
				if(position.y == 0)
				{
					//top
					indexs = new int[2];
					indexs[0] = currentIndex - (1);
					indexs[1] = currentIndex + (width - 1);
				}
				else if((position.y + 1) == height)
				{
					//bottom
					indexs = new int[2];
					indexs[0] = currentIndex - (1);
					indexs[1] = currentIndex - (width + 1);
				}
				else
				{
					//mid
					indexs = new int[3];
					indexs[0] = currentIndex - (1);
					indexs[1] = currentIndex - (width + 1);
					indexs[2] = currentIndex + (width - 1);
				}
			}
		}
		else if(direction == UP)
		{
			if((position.y - 1) >= 0)
			{
				
				if(position.x == 0)
				{
					//left side
					indexs = new int[2];
					indexs[0] = currentIndex - (width);
					indexs[1] = currentIndex - (width - 1);
				}
				else if((position.x + 1) == width)
				{
					//right side
					indexs = new int[2];
					indexs[0] = currentIndex - (width);
					indexs[1] = currentIndex - (width + 1);
				}
				else
				{
					//mid
					indexs = new int[3];
					indexs[0] = currentIndex - (width);
					indexs[1] = currentIndex - (width + 1);
					indexs[2] = currentIndex - (width - 1);
				}
			}
		}
		
		return indexs;
	}
	
	private Pixel[] getPixels(int[] indexs)
	{
		Pixel[] setPixels = new Pixel[indexs.length];
		
		for(int step = 0; step < indexs.length; step++)
		{
			setPixels[step] = getPixel(indexs[step]);
		}
		
		return setPixels;
	}
	
	private Pixel getPixel(int index)
	{
		Coordnets location = indextoCordnets(index);
		Pixel foundIt = pixels[location.x][location.y];
		return foundIt;
	}
	
	private void findLowestParent(Pixel[] possible, int index)
	{
		Pixel lowestEnergyPixel = possible[0];
		
		for(int step = 1; step < possible.length; step++)
		{
			if(lowestEnergyPixel.energy > possible[step].energy)
			{
				lowestEnergyPixel = possible[step];
			}
		}
		
		Pixel target = getPixel(index);
		target.parent = lowestEnergyPixel;
		target.pathTotalEnergy = (lowestEnergyPixel.pathTotalEnergy + target.energy);
	}
	
	private PixelPath pathHorizontalBuilder(PixelPath trail, int direction)
	{
		int width = currentPic.width();
		int height = currentPic.height();
		int currentIndex = 0;
		int stop = (height * width);
		
		//Build Paths
		for(int stepHort = 1; stepHort < width; stepHort++)
		{
			for(int stepVert = 0; stepVert < height; stepVert++)
			{
				if(stepHort > 2)
				{
					int test = 4;
				}
				
				currentIndex = coordnetsToIndex(stepHort, stepVert);
				
				int[] linkedPixelsIndex = getConnectedIndexes(currentIndex, direction);
				
				if(linkedPixelsIndex != null)
				{
					Pixel[] linkedPixels = getPixels(linkedPixelsIndex);
					findLowestParent(linkedPixels, currentIndex);
				}	
				
			}	
		}

		//find Lowest Start
		stop = height * width;
		Pixel lowestTotal = null;
		for(int lastStep = (width - 1); lastStep < stop; lastStep += width)
		{
			Pixel currentPixel = getPixel(lastStep);
			if(lowestTotal == null || currentPixel.pathTotalEnergy < lowestTotal.pathTotalEnergy)
			{
				lowestTotal = currentPixel;
			}
		}
		
		//Build Trail
		if(lowestTotal != null)
		{
			trail.totalEnergy = lowestTotal.pathTotalEnergy;
			
			while(lowestTotal != null)
			{
				trail.path.add(lowestTotal.parent);
				lowestTotal = lowestTotal.parent;
			}
		}
		
		return trail;
	}
	
	private PixelPath pathVerticalBuilder(PixelPath trail, int direction)
	{
		int width = currentPic.width();
		int height = currentPic.height();
		int currentIndex = 0;
		
		//Build Paths
		for(int stepVert = 0; stepVert < height; stepVert += width)
		{
			for(int stepHort = 1; stepHort < width; stepHort++)
			{
				currentIndex = coordnetsToIndex(stepHort, stepVert);
				
				int[] linkedPixelsIndex = getConnectedIndexes(currentIndex, direction);
				
				if(linkedPixelsIndex != null)
				{
					Pixel[] linkedPixels = getPixels(linkedPixelsIndex);
					findLowestParent(linkedPixels, currentIndex);
				}	
				
			}	
		}

		//find Lowest Start
		int stop = height * width;
		int start = (stop - 1) - (width - 1);
		int incrament = 1;
		Pixel lowestTotal = null;
		for(int lastStep = start; lastStep < stop; lastStep += incrament)
		{
			Pixel currentPixel = getPixel(lastStep);
			if(lowestTotal == null || currentPixel.pathTotalEnergy < lowestTotal.pathTotalEnergy)
			{
				lowestTotal = currentPixel;
			}
		}
		
		//Build Trail
		if(lowestTotal != null)
		{
			trail.totalEnergy = lowestTotal.pathTotalEnergy;
			
			while(lowestTotal != null)
			{
				trail.path.add(lowestTotal.parent);
				lowestTotal = lowestTotal.parent;
			}
		}
		
		return trail;
	}
	
	
	private static int[] toPrimitive(Integer[] IntegerArray) {
		 
		int[] result = new int[IntegerArray.length];
		for (int i = 0; i < IntegerArray.length; i++) {
			result[i] = IntegerArray[i].intValue();
		}
		return result;
	}
	
	private static int[] toPrimitiveB(Double[] array) {
		 
		int[] result = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			result[i] = array[i].intValue();
		}
		return result;
	}
	
	//////////////////////Public//////////////////////////////////
	public SeamCarver(Picture pic)
	{
		reset(pic);
	}

	// get the current image		
	public Picture getPicture ()
	{
		return currentPic;
	}

	public int width()
	{
		return currentPic.width();
	}

	public int height()
	{
		return currentPic.height();
	}

	// the energy of a pixel at (x,y)
	public double energy(int x, int y)
	{
		double pixelEnergy = pixels[x][y].energy;
		return pixelEnergy;
	}

	public double energy(int index)
	{
		Coordnets pixelLocation = indextoCordnets(index);
		double pixelEnergy = pixels[pixelLocation.x][pixelLocation.y].energy;
		return pixelEnergy;
	}
	
	// the sequence of indices for a vertical seam
	public int[] findVerticalSeam()
	{
		PixelPath cheapestPath = new PixelPath();
		int[] results = null;
		
		pathVerticalBuilder(cheapestPath, UP);
		
		if(cheapestPath.path.size() != 0)
		{
			results = new int[cheapestPath.path.size()];
			int stop = cheapestPath.path.size();
			for(int step = 0; step < cheapestPath.path.size(); step++)
			{
				results[step] = cheapestPath.path.get(step).index;
			}
			
		}
		
		return results;
	}
	
	// the sequence of indices for a horizontal seam
	public int[] findHorizontalSeam()
	{
		PixelPath cheapestPath = new PixelPath();
		int[] results = null;
		
		pathHorizontalBuilder(cheapestPath, RIGHT);
		
		if(cheapestPath.path.size() != 0)
		{
			results = new int[cheapestPath.path.size()];
			int stop = cheapestPath.path.size();
			for(int step = 0; step < cheapestPath.path.size(); step++)
			{
				results[step] = cheapestPath.path.get(step).index;
			}
		}
		
		return results;
	}

	private void setPixelsRemoved(int[] indices)
	{
		for(int step = 0; step < indices.length; step++)
		{
			Coordnets location = indextoCordnets(indices[step]);
			pixels[location.x][location.y].removed = true;
		}
	}
	
	private void buildImage(int newWidth, int newHeight)
	{
		int oldWidth = currentPic.width();
		int oldHeight = currentPic.height();
		
		Picture newPic = new Picture(newWidth, newHeight);
		
		int currentIndex = 0;
		Coordnets setCoord = indextoCordnets(currentIndex);
		Pixel currentPixel = pixels[setCoord.x][setCoord.y];
		
		for(int stepHeight = 0; stepHeight < newHeight; stepHeight++)
		{
			for(int stepWidth = 0; stepWidth < newWidth; stepWidth++)
			{	
				while(currentPixel.removed)
				{
					setCoord = indextoCordnets(++currentIndex);
					currentPixel = pixels[setCoord.x][setCoord.y];
				}
				
				if(!currentPixel.removed)
				{	
					newPic.set(stepWidth, stepHeight, currentPixel.color);	
				}
				
				setCoord = indextoCordnets(++currentIndex);
				if(setCoord.y < newHeight && setCoord.x < newWidth)
				{
					currentPixel = pixels[setCoord.x][setCoord.y];
				}
				
			}	
		}
		
		
		currentPic = newPic;
		
	}
	
	private void createNewImage(int direction)
	{
		int newWidth = currentPic.width();
		int newHeight = currentPic.height();
		
		if(direction == DOWN)
		{
			newHeight -= 1;
		}
		else if(direction == LEFT)
		{
			newWidth -= 1;
		}
		
		buildImage(newWidth, newHeight);
	}
	
	public void removeHorizontalSeam(int[] indices)
	{
		setPixelsRemoved(indices);
		createNewImage(DOWN);
		reset(currentPic);
	}

	public void removeVerticalSeam(int[] indices)
	{
		setPixelsRemoved(indices);
		createNewImage(LEFT);
		reset(currentPic);
	}


}
