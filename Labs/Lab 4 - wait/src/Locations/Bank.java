package Locations;

import java.util.ArrayList;
import java.util.Iterator;

import edu.neumont.util.QueueableService;
import edu.neumont.util.Client;

public class Bank implements QueueableService {
	
	ArrayList<Client> line;
	int numTellers;
	int beingServiced;
	int amountOfTimePassed;
	
	public Bank(int numberOfTellers)
	{
		line = new ArrayList<Client>();
		numTellers = numberOfTellers;
		amountOfTimePassed = 0;
	}
	
	@Override
	public boolean addClient(Client client) 
	{
		return line.add(client);
	}
	
	public void advanceMinute()
	{
		int numCustom = line.size();
		
		for(int step = 0; step < numTellers && step < numCustom; step++)
		{
			Client currentClinet = line.get(step);
			currentClinet.servedMinute();
			line.set(step, currentClinet);
		}
		
		amountOfTimePassed++;
	}
	
	private void checkForFinishedService()
	{
		Iterator<Client> collection = line.iterator();
		int size = line.size();
		
		//if service time is zero remove clinet
		for(Client var : line)
		{
			int serviceTime = var.getExpectedServiceTime();
			if(serviceTime == 0)
			{
				line.remove(var);
			}
		}
	}
	
	public void addCustomer(Client client)
	{
		line.add(client);
	}
	
	private int findIndex(int[] array, int k)
	{
		int index = 0;
		int length = array.length;
		if(length != 0)
		{
			int start = length - 1;
			int end = -1;
			for(int step = start; step > end; step--)
			{
				int value = array[step];
				if(k > value)
				{
					index = step;
					break;
				}
			}
		}
		//System.out.println(index);
		return index;
	}

	@Override
	public double getClientWaitTime(Client client) 
	{
		double waitTime = 0.0;

		int[] timeOfThoseBeingServed = new int[numTellers];
		
		boolean checkIfClientIsBeingServed = false;
		
		for(int step = 0; step < numTellers; step++)
		{
			Client currentEval = line.get(step);
			
			//MAke sure that the client we are looking for is served
			if(currentEval.equals(client))
			{
				checkIfClientIsBeingServed = true;
				break;
			}
			else
			{
				timeOfThoseBeingServed[step] = line.get(step).getExpectedServiceTime();
			}
		}
		
		if(!checkIfClientIsBeingServed)
		{
			int totalCustomers = line.size();
	
			for(int step = numTellers; step < totalCustomers; step++)
			{
				//Hold the current Client that we are evaluating
				Client currentEval = line.get(step);
				
				//Find smallest teller time
				int index = 0;
				int smallestTime = timeOfThoseBeingServed[0];
				for(int tellStep = 0; tellStep < numTellers; tellStep++)
				{
					if(timeOfThoseBeingServed[tellStep] < smallestTime)
					{
						index = tellStep;
						smallestTime = timeOfThoseBeingServed[tellStep];
					}
				}
				
				//input wait time
				waitTime = smallestTime;
				timeOfThoseBeingServed[index] += client.getExpectedServiceTime();
				
				//Check if this is the client that we wanted to find
				//If so, we have their time
				if(currentEval.equals(client))
				{
					break;
				}
				
			}
		}
		
		return waitTime;
	}

	@Override
	public double getAverageClientWaitTime() 
	{
		
		int num = 0;
		double Average = 0;
		
		for(Client client : line)
		{
			Average = ((Average*num) + client.getExpectedServiceTime())/(num + 1);
			num++;
		}
		
		return Average;
		
	}
}

