package Locations;

import arrayTypes.PriorityQueue;
import edu.neumont.util.Client;

import java.util.ArrayList;
import java.util.Random;

public class Hopsital 
{
	Random randomGenerator = new Random();
	int numberOfDocs;
	PriorityQueue<Patient> waitingRoom;
	ArrayList<Doctor> pharmicy;
	
	public class Doctor
	{
		public Patient currentPatient;
	}
	
	public class Patient implements Comparable<Patient>
	{
		public Client person;
		public int attrition;
		
		Patient(Client incoming)
		{
			person = incoming;
			attrition = randomGenerator.nextInt(100);
		}
		
		Patient(Client incoming, int lengthOflife)
		{
			person = incoming;
			attrition = lengthOflife;
		}

		@Override
		public int compareTo(Patient arg0) {
			int results = 0;
			
			if(arg0.attrition > attrition)
			{
				results = -1;
			}
			else if(arg0.attrition < attrition)
			{
				results = 1;
			}
			
			return results;
		}
		
		@Override
		public String toString()
		{
			String info = "ART: " + attrition + " ST:" + person.getExpectedServiceTime();
			return info;	
		}
	}
	
	public Hopsital(int numOfDocs)
	{
		numberOfDocs = numOfDocs;
		waitingRoom = new PriorityQueue<Patient>();
		pharmicy = new ArrayList<Doctor>();
		for(int step = 0; step < numberOfDocs; step++)
		{
			pharmicy.add(new Doctor());
		}
	}
	
	public void advanceMinute()
	{	
		for(int step = 0; step < pharmicy.size(); step++)
		{
			Doctor doc = pharmicy.get(step);
			doc.currentPatient.person.servedMinute();
		}
		
		for(int step = 0; step < waitingRoom.size(); step++)
		{
			Patient injuerd = waitingRoom.get(step);
			injuerd.attrition--;
		}
		
		serviceCheck();
		emergencyCheck();
	}
	
	private void serviceCheck()
	{
		for(int step = 0; step < pharmicy.size(); step++)
		{
			Doctor doc = pharmicy.get(step);
			if(doc.currentPatient.person == null || doc.currentPatient.person.getExpectedServiceTime() == 0)
			{
				if(waitingRoom.isEmpty())
				{
					doc.currentPatient = waitingRoom.poll();
				}
			}
		}
	}
	
	private void emergencyCheck()
	{
		for(int step = 0; step < waitingRoom.size(); step++)
		{
			Patient injuerd = waitingRoom.get(step);
			if(injuerd.attrition == 1)
			{
				//Is there a doctor in the house
				int index = 0;
				Doctor docBrown = null;
				int HighestArittion = pharmicy.get(0).currentPatient.attrition;
				
				for(int currentDoc = 0; currentDoc < pharmicy.size(); currentDoc++)
				{
					Doctor doc = pharmicy.get(step);
					if(doc.currentPatient.attrition > HighestArittion && doc.currentPatient.attrition != 1)
					{
						docBrown = doc;
						HighestArittion = doc.currentPatient.attrition;
						index = currentDoc;
					}
				}
				
				//swich them out
				if(docBrown != null)
				{
					waitingRoom.add(docBrown.currentPatient);
					docBrown.currentPatient = injuerd;
					pharmicy.set(index, docBrown);
				}
			}
		}
	}
	
	public void addCustomer(Client client, int attrition)
	{
		//Find avalible doctor
		boolean noDocAvalible = true;
		
		for(Doctor doc : pharmicy)
		{
			if(doc.currentPatient == null)
			{
				doc.currentPatient = new Patient(client, attrition);
				noDocAvalible = false;
				break;
			}
		}
		
		if(noDocAvalible)
		{
			waitingRoom.add(new Patient(client, attrition));
		}
	}

	private int findShortestTime(int[] times)
	{
		int index = 0;
		
		for(int step = 0; step < times.length; step++)
		{
			if(times[0] > times[step])
			{
				index = step;
			}
		}
		
		return index;
	}
	
	public double getClientWaitTime(Client client)
	{
		double waitTime = 0.0;
		
		boolean clientIsBeingServised = false;
		
		int[] doctorsServiceTime = new int[numberOfDocs];
		int step = 0;
		//Check if client is being servired
		for(Doctor doc : pharmicy)
		{
			if(doc.currentPatient.person != null)
			{
				if(doc.currentPatient.person.equals(client))
				{
					clientIsBeingServised = true;
					break;
				}
				
				doctorsServiceTime[step++] = doc.currentPatient.person.getExpectedServiceTime();
			}
			else
			{
				doctorsServiceTime[step++] = 0;
			}
		}
		
		if(!clientIsBeingServised)
		{
			if(waitingRoom == null)
			{
				//throw exception
			}
			else
			{
				//Calculate Wait time
				int numWaiting = waitingRoom.size();
				for(int selectedWaiter = 0; selectedWaiter < numWaiting; selectedWaiter++)
				{
					Patient injured = waitingRoom.get(selectedWaiter);
					
					Client injuedPerson = injured.person;
					
					int index = findShortestTime(doctorsServiceTime);
					waitTime = doctorsServiceTime[index];
					
					if(injuedPerson.equals(client))
					{
						break;
					}
					else
					{
						doctorsServiceTime[index] += injured.person.getExpectedServiceTime();
					}
					
				}
				
			}
		}
		
		return waitTime;
	}
	
	public double getAverageClientWaitTime()
	{		
		int num = 0;
		double Average = 0;
		
		for(Doctor doc : pharmicy)
		{
			if(doc.currentPatient != null)
			{
				Average = ((Average*num) + doc.currentPatient.person.getExpectedServiceTime())/(num + 1);
				num++;
			}
			
		}
		
		for(Patient injured : waitingRoom)
		{
			Average = ((Average*num) + injured.person.getExpectedServiceTime())/(num + 1);
			num++;
		}
		
		return Average;
	}
	
}
