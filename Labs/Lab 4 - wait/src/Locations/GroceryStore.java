package Locations;

import java.util.ArrayList;
import java.util.Iterator;
import edu.neumont.util.QueueableService;
import edu.neumont.util.Client;

public class GroceryStore {

	ArrayList <ArrayList<Client>> lanes;
	int numberOfLines;
	int amountOFTimePassed;
	
	public GroceryStore(int numLaines)
	{
		lanes = new ArrayList<ArrayList<Client>>();
		numberOfLines = numLaines;
		for(int step = 0; step < numLaines; step++)
		{
			lanes.add(new ArrayList<Client>());
		}
	}
	
	public void advanceMinute()
	{
		for(ArrayList<Client> lane : lanes)
		{
			Client shopper = lane.get(0);
			shopper.servedMinute();
			int serviceTime = shopper.getExpectedServiceTime();
			if(serviceTime != 0)
			{
				lane.set(0, shopper);
			}
			else
			{
				lane.remove(shopper);
			}
		}
		
		amountOFTimePassed++;
	}
	
	private void checkForFinishedService()
	{
		for(ArrayList<Client> lane : lanes)
		{
			for(Client shopper : lane)
			{
				int serviceTime = shopper.getExpectedServiceTime();
				if(serviceTime == 0)
				{
					lane.remove(shopper);
				}
			}
		}
	}
	
	public void addCustomer(Client client)
	{
		//Add to smallest lane
		ArrayList<Client> smalestLane = lanes.get(0);
		int smalestSize = 0;
		
		for(int laneNum = 0; laneNum < numberOfLines; laneNum++)
		{
			ArrayList<Client> lane = lanes.get(laneNum);
			int currentLaneSize = lane.size();
			
			if(currentLaneSize == 0)
			{
				smalestLane = lane;
				smalestSize = lane.size() + 1;
				break;
			}
			else if(currentLaneSize <= smalestSize)
			{
				smalestLane = lane;
				smalestSize = currentLaneSize + 1;
			}
			
		}
		
		
		
		smalestLane.add(client);
		
	}
	
	public double getClientWaitTime(Client client)
	{
		double waitTime = 0.0;
		//find where the customer Is
		ArrayList<Client> lineClientIsIn = null;
		for(ArrayList<Client> line : lanes)
		{
			if(line.contains(client))
			{
				lineClientIsIn = line;
				break;
			}
		}
		
		if(lineClientIsIn == null)
		{
			//throw exception
		}
		else
		{
			//Calculate Wait time
			double totalServiceTimeOfThoseAhead = 0.0;
			for(Client shopper : lineClientIsIn)
			{
				if(shopper.equals(client))
				{
					break;
				}
				else
				{
					totalServiceTimeOfThoseAhead += shopper.getExpectedServiceTime();
				}
			}
			waitTime = totalServiceTimeOfThoseAhead;
		}
		
		return waitTime;
	}
	
	
	public double getAverageClientWaitTime()
	{
		int totalNumClients = 0;
		int totalWaitTime = 0;
		
		for(ArrayList<Client> laine : lanes)
		{
			int sizeOfLine = laine.size();
			totalNumClients += sizeOfLine;
			
			///Get total wait time
			for(int step = 1; step < sizeOfLine; step++)
			{
				Client currentClient = laine.get(step);
				totalWaitTime += getClientWaitTime(currentClient);
			}
			///
		}
		
		double Average = (double)totalWaitTime / (double)totalNumClients;
		return Average;
	}

	
}