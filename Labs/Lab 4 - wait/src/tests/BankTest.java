package tests;

import static org.junit.Assert.*;
import helperFunctions.genericHelpers;

import org.junit.Test;

import Locations.Bank;
import Locations.GroceryStore;
import edu.neumont.util.Client;

public class BankTest {

	genericHelpers hp = new genericHelpers();
	Bank usaa;
	
	@Test
	public void testConstruction() {
		hp.println("Check if construction completes");
		usaa = new Bank(4);
		assertTrue(usaa != null);
	}
	
	@Test
	public void testAddCustomer() {
		hp.println("Add Customer test");
		usaa = new Bank(4);
		Client bond = new Client(5);
		usaa.addCustomer(bond);
		
		assertTrue(usaa != null);
	}
	
	private void waitTimeAssert(Client findCLient, double timeShoudBe)
	{
		double waitTime = usaa.getClientWaitTime(findCLient);
		assertTrue(waitTime == timeShoudBe);
	}
	
	@Test
	public void testWaitTime() {
		hp.println("Get CLient Wait Time");
		usaa = new Bank(1);
		
		Client james = new Client(5);
		Client bond = new Client(5);
		Client nerd = new Client(5);
		
		usaa.addCustomer(james);
		usaa.addCustomer(bond);
		usaa.addCustomer(nerd);
		
		waitTimeAssert(james, 0.00);
		waitTimeAssert(bond, 5.00);
		waitTimeAssert(nerd, 10.00);
	}
	
	@Test
	public void testAverageWaitTime() 
	{
		usaa = new Bank(1);
		
		Client james = new Client(5);
		Client bond = new Client(5);
		Client nerd = new Client(5);
		
		usaa.addCustomer(james);
		usaa.addCustomer(bond);
		usaa.addCustomer(nerd);
		
		double avgWaitTime = usaa.getAverageClientWaitTime();
		assertTrue(avgWaitTime == 5.0);
	}
	
	@Test
	public void testAdvanceMinute() 
	{
		usaa = new Bank(1);
		
		Client james = new Client(5);
		Client bond = new Client(5);
		Client nerd = new Client(5);
		
		usaa.addCustomer(james);
		usaa.addCustomer(bond);
		usaa.addCustomer(nerd);
		
		usaa.advanceMinute();
		
		waitTimeAssert(james, 0.00);//ST 4
		waitTimeAssert(bond, 4.00); //ST 5 WT 4
		waitTimeAssert(nerd, 9.00); //ST 5 WT 9
		
		double avgWaitTime = usaa.getAverageClientWaitTime();
		assertTrue((int)avgWaitTime == 4);
	}

}
