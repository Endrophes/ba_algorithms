package tests;

import static org.junit.Assert.*;
import java.util.Iterator;

import helperFunctions.genericHelpers;

import org.junit.Test;

import arrayTypes.PriorityQueue; 

public class PriorityQueueTest {

	genericHelpers hp = new genericHelpers();
	PriorityQueue<Integer> collection = new PriorityQueue<Integer>();

	private void checkSize(int size)
	{
		int currentSize = collection.size();
		hp.println("Size is: " + currentSize);
		assertTrue(currentSize == size);
	}
	
	private void peekCheck(int topValueShouldBe)
	{
		int results = collection.peek();
		hp.println("Top value returned: " + results);
		assertTrue(results == topValueShouldBe);
	}
	
	private void offerToCollection(int number)
	{
		boolean results = collection.offer(number);
		hp.println("offer returned: " + results + ", value offered: " + number);
		hp.println("Rank is:" + collection.rank);
		assertTrue(results);
	}
	
	private void printIterator()
	{
		Iterator<Integer> currentSet = collection.iterator();
		
		int slotnum = 0;
		int size = collection.size();
		while(slotnum < size)
		{
			int value = currentSet.next();
			hp.println(slotnum + ":" + value);
			slotnum++;
		}
	}
	
	@Test
	public void buildQueTest() {
		hp.println("Checking for null collection");
		assertTrue(collection != null);
	}

	@Test
	public void testOffer() {
		hp.println("adding to queue");
		boolean results = collection.offer(21);
		int currentSize = collection.size();
		assertTrue(results);
		assertTrue(currentSize == 1);
	}
	
	@Test
	public void testOrder() {
		hp.println("adding two objects to queue for testing order");
		offerToCollection(21);
		offerToCollection(1);
		peekCheck(1);
		checkSize(2);
		
		offerToCollection(42);
		checkSize(3);
		peekCheck(1);
		
	}
	
	@Test
	public void testIterator() {
		hp.println("Itrator test");
		offerToCollection(21);
		offerToCollection(1);
		offerToCollection(42);
		
		Iterator<Integer> currentSet = collection.iterator();
		
		int slotnum = 0;
		int size = collection.size();
		while(slotnum < size)
		{
			int value = currentSet.next();
			hp.println(slotnum + ":" + value);
			slotnum++;
		}
	}
	
	@Test
	public void testPoll() 
	{
		hp.println("Poll test");
		offerToCollection(21);
		offerToCollection(1);
		offerToCollection(42);
		
		int value = collection.poll();
		assertTrue(value == 1);
		printIterator();
	}
	
}
