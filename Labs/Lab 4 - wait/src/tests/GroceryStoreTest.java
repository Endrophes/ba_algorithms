package tests;

import static org.junit.Assert.*;
import helperFunctions.genericHelpers;



import org.junit.Test;

import edu.neumont.util.Client;
import Locations.GroceryStore;

public class GroceryStoreTest {

	genericHelpers hp = new genericHelpers();
	GroceryStore wallmart;
	
	@Test
	public void testConstruction() {
		hp.println("Check if construction completes");
		wallmart = new GroceryStore(4);
		assertTrue(wallmart != null);
	}
	
	@Test
	public void testAddCustomer() {
		hp.println("Add Customer test");
		wallmart = new GroceryStore(4);
		Client bond = new Client(5);
		wallmart.addCustomer(bond);
		
		assertTrue(wallmart != null);
	}
	
	private void waitTimeAssert(Client findCLient, double timeShoudBe)
	{
		double waitTime = wallmart.getClientWaitTime(findCLient);
		assertTrue(waitTime == timeShoudBe);
	}
	
	@Test
	public void testWaitTime() {
		hp.println("Get CLient Wait Time");
		wallmart = new GroceryStore(1);
		
		Client james = new Client(5);
		Client bond = new Client(5);
		Client nerd = new Client(5);
		
		wallmart.addCustomer(james);
		wallmart.addCustomer(bond);
		wallmart.addCustomer(nerd);
		
		waitTimeAssert(james, 0.00);
		waitTimeAssert(bond, 5.00);
		waitTimeAssert(nerd, 10.00);
	}
	
	@Test
	public void testAverageWaitTime() 
	{
		wallmart = new GroceryStore(1);
		
		Client james = new Client(5);
		Client bond = new Client(5);
		Client nerd = new Client(5);
		
		wallmart.addCustomer(james);
		wallmart.addCustomer(bond);
		wallmart.addCustomer(nerd);
		
		double avgWaitTime = wallmart.getAverageClientWaitTime();
		assertTrue(avgWaitTime == 5.0);
	}
	
	@Test
	public void testAdvanceMinute() 
	{
		wallmart = new GroceryStore(1);
		
		Client james = new Client(5);
		Client bond = new Client(5);
		Client nerd = new Client(5);
		
		wallmart.addCustomer(james);
		wallmart.addCustomer(bond);
		wallmart.addCustomer(nerd);
		
		wallmart.advanceMinute();
		
		waitTimeAssert(james, 0.00);//ST 4
		waitTimeAssert(bond, 4.00); //ST 5 WT 4
		waitTimeAssert(nerd, 9.00); //ST 5 WT 9
		
		double avgWaitTime = wallmart.getAverageClientWaitTime();
		assertTrue((int)avgWaitTime == 4);
	}
}
