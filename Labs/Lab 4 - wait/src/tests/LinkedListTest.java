package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import arrayTypes.LinkedList;

public class LinkedListTest {

	LinkedList collection;
	
	private void setUp()
	{
		collection = new LinkedList<Integer>();
	}
	
	@Test
	public void EstablishCollection() 
	{
		setUp();
		assertTrue(collection != null);
	}
	
	@Test
	public void offerTolist()
	{
		setUp();
		collection.offer(21);
		int sizeTest = collection.getSize();
		assertTrue(sizeTest == 1);
	}
	
	@Test
	public void poll() 
	{
		setUp();
		assertNull(collection.poll());
		collection.offer(21);
		collection.offer(21);
		collection.offer(21);
		collection.offer(21);
		collection.offer(21);
		collection.offer(21);
		
		int test = (int) collection.poll();
		assertTrue(test == 21);
		
		int sizeTest = collection.getSize();
		assertTrue(sizeTest == 5);
	}
	
	@Test
	public void removeAt() 
	{
		setUp();
		collection.offer(21);
		collection.offer(21);
		collection.offer(21);
		collection.offer(21);
		collection.offer(21);
		collection.offer(21);
		
		collection.removeAtIndex(2);
		//assertTrue(test == 21);
		int sizeTest = collection.getSize();
		
		assertTrue(sizeTest == 5);
	}
	
}
