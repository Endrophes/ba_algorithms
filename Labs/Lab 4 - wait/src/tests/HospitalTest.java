package tests;

import static org.junit.Assert.*;

import java.util.Iterator;

import helperFunctions.genericHelpers;

import org.junit.Test;

import Locations.Hopsital;
import Locations.Hopsital.Patient;
import edu.neumont.util.Client;

public class HospitalTest {

	genericHelpers hp = new genericHelpers();
	Hopsital saintGorge;
	Client[] setOFCLients;
	
	//used for debuging
	private void printIterator()
		{
			//Iterator<Patient> currentSet = saintGorge.waitingRoom.iterator();
			
			//int slotnum = 0;
			//int size = saintGorge.waitingRoom.size();
			//while(slotnum < size)
			//{
			//	Patient person = currentSet.next();
			//	hp.println(person.toString());
			//	slotnum++;
			//}
		}
	
	private void setUp(int numClients)
	{
		saintGorge = new Hopsital(1);
		setOFCLients = new Client[numClients];
		
		for(int step = 0; step < numClients; step++)
		{
			setOFCLients[step] = new Client(5); 
			saintGorge.addCustomer(setOFCLients[step], (5 + step));
		}
	}
	
	private void waitTimeAssert(Client findCLient, double timeShoudBe)
	{
		double waitTime = saintGorge.getClientWaitTime(findCLient);
		assertTrue(waitTime == timeShoudBe);
	}
	
	@Test
	public void testConstruction() {
		hp.println("Check if construction completes");
		saintGorge = new Hopsital(4);
		assertTrue(saintGorge != null);
	}
	
	@Test
	public void testAddCustomer() {
		hp.println("Add Customer test");
		saintGorge = new Hopsital(4);
		Client bond = new Client(5);
		saintGorge.addCustomer(bond, 5);
		
		assertTrue(saintGorge != null);
	}
	
	@Test
	public void testWaitTime() {
		hp.println("Get CLient Wait Time");
		setUp(3);
		
		//printIterator();
		
		waitTimeAssert(setOFCLients[0], 0.00);
		waitTimeAssert(setOFCLients[1], 5.00);
		waitTimeAssert(setOFCLients[2], 10.00);
	}
	
	@Test
	public void testAverageWaitTime() 
	{
		saintGorge = new Hopsital(1);
		
		setUp(3);
		
		double avgWaitTime = saintGorge.getAverageClientWaitTime();
		assertTrue(avgWaitTime == 5.0);
	}
	
	@Test
	public void testAdvanceMinute() 
	{
		saintGorge = new Hopsital(1);
		
		setUp(3);
		
		//printIterator();
		
		saintGorge.advanceMinute();
		
		//printIterator();
		
		waitTimeAssert(setOFCLients[0], 0.00);
		waitTimeAssert(setOFCLients[1], 4.00);
		waitTimeAssert(setOFCLients[2], 9.00);
		
		double avgWaitTime = saintGorge.getAverageClientWaitTime();
		assertTrue((int)avgWaitTime == 4);
	}


}
