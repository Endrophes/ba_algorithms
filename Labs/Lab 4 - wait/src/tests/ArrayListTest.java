package tests;

import static org.junit.Assert.*;
import org.junit.Test;
import arrayTypes.ArrayList;

public class ArrayListTest {

	ArrayList collection;
	
	//Helper Functions
	private void println(String message)
	{
		System.out.println(message);
	}
	
	private void print(String message)
	{
		System.out.print(message);
	}
	
	private void setUp()
	{
		collection = new ArrayList<Integer>();
	}
	
	
	//Tests
	//@Test
	//public void test() {
	//	println("Test one two, SEGA");
	//}
	
	@Test
	public void buildingArrayList() {
		println("Construct Array List");
		setUp();
		assertTrue(collection != null);
	}
	
	@Test
	public void adddingToArrayList() {
		println("adding one Object to array list");
		setUp();
		collection.add(21);
		int currentSize = collection.size();
		assertTrue(currentSize == 1);
	}

	@Test
	public void testGet() {
		println("getting one object from array list");
		setUp();
		collection.add(21);
		int test = (int) collection.get(0);
		int currentSize = collection.size();
		assertTrue(currentSize == 1);
		assertFalse(currentSize == 21);
	}
	
	@Test
	public void removeFromArrayList() {
		println("removing one Object from array list");
		setUp();
		collection.remove(0);
		collection.add(21);
		collection.remove(0);
		int currentSize = collection.size();
		assertTrue(currentSize == 0);
	}
	
	public void run()
	{
		buildingArrayList();
		adddingToArrayList();
		testGet();
		removeFromArrayList();
	}

}
