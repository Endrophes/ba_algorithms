package arrayTypes;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

import javax.naming.OperationNotSupportedException;

public class PriorityQueue<T extends Comparable<T>> implements Queue<T>{
	
	private int maxSize;
	private int currentSize;
	
	private boolean fixedSize;
	private boolean lowToHeigh;
	
	private Node head;
	private Node tail;
	
	public int rank = 0;
	
	
	public class Node<T extends Comparable<T>>
	{
		private T value;
		private Node next;
		
		Node(T info)
		{
			value = info;
		}
		
		Node(T info, Node placeAfter)
		{
			value = info;
			next = placeAfter;
		}

		public void replaceValue(T newValue)
		{
			value = newValue;
		}
		
		public void setNextLink(Node nextTarget)
		{
			next = nextTarget;
		}

		public Node getNextNode()
		{
			return next;
		}
	
		public T getValue()
		{
			return value;
		}
	}
	
	private class iterator implements Iterator<T>
	{
		private final PriorityQueue<T> myArray;
			
		private Node current;
		
		iterator(PriorityQueue<T> collection, Node<T> head)
		{
			myArray = collection;
			current = head;
		}
		
		@Override
		public boolean hasNext() {
			boolean results = false;
			
			if(current.getNextNode() != null)
			{
				results = true;
			}
			
			return results;
		}

		@Override
		public T next() {
			T value = (T) current.getValue();
			current = current.getNextNode();
			return value;
		}

		@Override
		public void remove() {
			try {
				throw new OperationNotSupportedException();
			} catch (OperationNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public Iterator<T> iterator()
	{
		Iterator<T> temp = new iterator(this, head);	
		return temp;
	}
	
	public T poll()
	{
		T value = (T) head.getValue();
		Node temp = head.getNextNode();
		head.setNextLink(null);
		head = temp;
		currentSize--;
		return value;
	}
		
	public T peek()
	{
		T value = (T) head.getValue();
		return value;
	}

	@Override
	public boolean isEmpty() {
		boolean result = false;
		
		if(currentSize == 0)
		{
			result = true;
		}
		
		return false;
	}

	@Override
	public int size() {
		return currentSize;
	}
	
	public Node findPlaceToinsert(Comparable<T> value)
	{
		Node pointToIndex = head;
		
		while(pointToIndex.getNextNode() != null)
		{
			int comparison = value.compareTo((T)pointToIndex.getValue());
			if(comparison == -1)
			{
				//Iterate
				pointToIndex = pointToIndex.getNextNode();
			}
			else
			{
				//Break
				break;
			}
		}
		
		return null;
	}
	
	public T get(int index) {
		
		T target = null;
		
		if(index < currentSize)
		{
			Node currentNode = head;
			for(int step = 0; step < index; step++)
			{
				currentNode = currentNode.getNextNode();
			}
			target = (T) currentNode.getValue();
		}
		else
		{
			//throw exception
		}
		
		return target;
	}
	
	private void insertHelper(Node newNode)
	{
		Node lastNode = head;
		Node currentNode = head;
		//Find the current node
		if(head.getNextNode() == null)
		{
			T valueTocheckWith = (T) currentNode.getValue();
			T value = (T) newNode.getValue();
			rank = value.compareTo(valueTocheckWith); 
			
			if(rank == 1 || rank == 0)
			{
				head.setNextLink(newNode);
			}
			else
			{
				newNode.setNextLink(head);
				head = newNode;
			}
		}
		else
		{
			//Type test = (Type) value;

			for(int step = 0; step <= currentSize; step++)
			{
				if(step < currentSize) //in the list
				{
					T valueTocheckWith = (T) currentNode.getValue();
					T value = (T) newNode.getValue();
					rank = value.compareTo(valueTocheckWith); 
					
					if(rank == 1 || rank == 0)
					{
						//Step over
						Node temp = currentNode;
						lastNode = temp;
						currentNode = currentNode.getNextNode();
					}
					else
					{
						//insert here
						lastNode.setNextLink(newNode);
						newNode.setNextLink(currentNode);
					}
					
				}
				else //at the end of the list
				{
					lastNode.setNextLink(newNode);
					tail = newNode;
				}
			}
			
			
			
		}
		
	}
	
	@Override
	public boolean offer(T value) {
		boolean worked = false;
		
		if(fixedSize)
		{
			if(currentSize < maxSize)
			{
				Node newNode = new Node((Comparable) value);
				if(head != null)
				{
					head = newNode;
				}
				else
				{
					insertHelper(newNode);
				}
				
				worked = true;
				currentSize++;
			}
		}
		else
		{
			Node newNode = new Node(value);
			if(head == null)
			{
				head = newNode;
			}
			else
			{
				insertHelper(newNode);
			}
			
			worked = true;
			currentSize++;
		}
		
		
		return worked;
	}
	
	@Override
	public T remove() {
		
		T value = null;
		if(currentSize != 0)
		{
			value = (T) head.getValue();
			Node temp = head.getNextNode();
			head.setNextLink(null);
			head = temp;
			currentSize--;
		}
		else
		{
			//throw an exception
		}
		
		return value;
	}

	@Override
	public boolean add(T value) {
		boolean worked = false;
		
		if(fixedSize)
		{
			if(currentSize < maxSize)
			{
				Node newNode = new Node((Comparable) value);
				if(head != null)
				{
					head = newNode;
				}
				else
				{
					insertHelper(newNode);
				}
				
				worked = true;
				currentSize++;
			}
			else
			{
				//Throw exception
			}
		}
		else
		{
			Node newNode = new Node(value);
			if(head == null)
			{
				head = newNode;
			}
			else
			{
				insertHelper(newNode);
			}
			
			worked = true;
			currentSize++;
		}
		
		
		return worked;
	}

	@Override
	public T element() {
		T value = (T) head.getValue();
		return value;
	}

	@Override
	public void clear() {
		head = null;
		currentSize = 0;
	}
	
	@Override
	public boolean contains(Object arg0) {
		boolean result = false;
		
		Node currentNode = head;
		
		while(currentNode.getNextNode() != null)
		{
			Comparable<T> value = currentNode.getValue();
			if(value.compareTo((T) arg0) == 0)
			{
				result = true;
			}
			
			currentNode = currentNode.getNextNode();
		}
		
		return result;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		boolean result = true;
		
		for(Object var : arg0)
		{
			result = contains(var);
			if(!result)
			{
				break;
			}
		}
		
		return result;
	}
	
	@Override
	public boolean addAll(Collection<? extends T> arg0) {
		boolean result = true;
		
		for(Object var : arg0)
		{
			result = offer((T) var);
			if(!result)
			{
				break;
			}
		}
		
		return result;
	}
	
	@Override
	public boolean remove(Object arg0) {
		boolean found = false;
		
		Node lastNode = head;
		Node currentNode = head;
		
		while(currentNode.getNextNode() != null)
		{
			Comparable<T> value = currentNode.getValue();
			
			if(value.compareTo((T) arg0) == 0)
			{
				found = true;
				break;
			}
			else
			{
				Node temp = currentNode;
				currentNode = currentNode.getNextNode();
				lastNode = temp;
			}
		}
		
		if(found)
		{
			lastNode.setNextLink(currentNode.getNextNode());
			currentNode.setNextLink(null);
		}
		
		return false;
	}
	
	@Override
	public boolean removeAll(Collection<?> arg0) {
		boolean foundeach = true;
		
		for(Object var : arg0)
		{
			foundeach = remove(var);
		}
		
		return foundeach;
	}

	@Override
	public Object[] toArray() {
		Object[] collection = new Object[currentSize];
		
		Node currentNode = head;
		int step = 0;
		
		while(currentNode.getNextNode() != null)
		{
			collection[step++] = currentNode.getValue();
		}
		
		return collection;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		T[] collection = (T[]) new Object[currentSize];
		
		Node currentNode = head;
		int step = 0;
		
		while(currentNode.getNextNode() != null)
		{
			collection[step++] = (T) currentNode.getValue();
		}
		
		return collection;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		boolean worked = true;
		
		Node lastNode = head;
		Node currentNode = head;
		
		while(currentNode.getNextNode() != null)
		{
			if(!arg0.contains(currentNode.getValue()))
			{
				lastNode.setNextLink(currentNode.getNextNode());//link over
				currentNode.setNextLink(null);//remove the node
				currentNode = lastNode.getNextNode(); //set to leading node
				
			}
			else
			{
				Node temp = currentNode;
				currentNode = currentNode.getNextNode();
				lastNode = temp;
			}
		}
		
		return false;
	}
	
}
