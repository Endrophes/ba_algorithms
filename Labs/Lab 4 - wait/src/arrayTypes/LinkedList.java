package arrayTypes;

import java.util.Arrays;
import java.util.Iterator;

import javax.naming.OperationNotSupportedException;

import edu.neumont.util.Queue;

public class LinkedList<T> implements Queue<T>  {
	
	private boolean fixedSize;
	private int maxSize;
	private int currentSize;
	
	private Node head;
	private Node tail;
	
	public class Node<T>
	{
		private T value;
		private Node next;
		
		Node(T info)
		{
			value = info;
		}
		
		Node(T info, Node placeAfter)
		{
			value = info;
			next = placeAfter;
		}

		public void replaceValue(T newValue)
		{
			value = newValue;
		}
		
		public void setNextLink(Node nextTarget)
		{
			next = nextTarget;
		}

		public Node getNextNode()
		{
			return next;
		}
	
		public T getValue()
		{
			return value;
		}
	}
	
	public LinkedList()
	{
		fixedSize = false;
		maxSize = 1;
	}
	
	public LinkedList(boolean fixed, int maxNumber)
	{
		fixedSize = fixed;
		maxSize = maxNumber;
	}
	
	public void makeFixed(int maxNumber)
	{
		fixedSize = true;
		maxSize = maxNumber;
	}
	
	public boolean replaceValue(int AtIndex, T newValue)
	{
	 	boolean valueReplaced = false;
	 	
	 	if(AtIndex < currentSize)
	 	{
	 		Node currentNode = head;
	 		
	 		for(int step = 0; step < currentSize && step != AtIndex; step++)
	 		{
	 			currentNode = currentNode.getNextNode();
	 		}
	 		
	 		currentNode.replaceValue(newValue);
	 		
	 		valueReplaced = true;
	 	}
	 	else
	 	{
	 		//throw an exception
	 	}
	 	
	 	return valueReplaced;
	 }
	
	public boolean offerAtIndex(int AtIndex, T newValue)
	{
		boolean worked = false;
		
		if(AtIndex < currentSize)
	 	{
			Node lastNode = head;
	 		Node currentNode = head;
	 		Node nodeAfter = head.getNextNode();
	 		
	 		for(int step = 0; step != AtIndex; step++)
	 		{
	 			lastNode = currentNode;
	 			currentNode = currentNode.getNextNode();
	 			nodeAfter = currentNode.getNextNode();
	 		}
	 		
	 		Node newNode = new Node(newValue, currentNode);
	 		
	 		lastNode.setNextLink(newNode);
	 		
	 		currentSize++;
	 		
	 		worked = true;
	 	}
	 	else
	 	{
	 		//throw an exception
	 	}
		
		return worked;
	}
	
	public boolean removeAtIndex(int AtIndex)
	{
		boolean worked = false;
		
		if(AtIndex < currentSize)
	 	{
			Node lastNode = head;
	 		Node currentNode = head;
	 		Node nodeAfter = head.getNextNode();
	 		
	 		for(int step = 0; step != AtIndex; step++)
	 		{
	 			lastNode = currentNode;
	 			currentNode = currentNode.getNextNode();
	 			nodeAfter = currentNode.getNextNode();
	 		}
	 	
 			lastNode.setNextLink(nodeAfter);
 			
 			//T value = (T) currentNode.getValue();
 			
 			currentNode.setNextLink(null);
 			
 			currentSize--;
 			
	 		worked = true;
	 	}
	 	else
	 	{
	 		//throw an exception
	 	}
		
		return worked;
	}
	
	public int getSize()
	{
		return currentSize;
	}

	private class iterator implements Iterator<T>
	{
		private final LinkedList<T> myArray;
			
		private Node current;
		
		iterator(LinkedList<T> collection, Node<T> head)
		{
			myArray = collection;
			current = head;
		}
		
		@Override
		public boolean hasNext() {
			boolean results = false;
			
			if(current.getNextNode() != null)
			{
				results = true;
			}
			
			return results;
		}

		@Override
		public T next() {
			T value = (T) current.getValue();
			current = current.getNextNode();
			return value;
		}

		@Override
		public void remove() {
			try {
				throw new OperationNotSupportedException();
			} catch (OperationNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public Iterator<T> iterator()
	{
		Iterator<T> temp = new iterator(this, head);
		return temp;
	}

	public T getAtIndex(int index)
	{
		Node currentNode = head;
		if(index < currentSize)
		{
			for(int step = 0; step < index && currentNode.getNextNode() != null; step++)
			{
				currentNode = currentNode.getNextNode();
			}
		}
		else
		{
			//ThrowException
		}
		
		T target = (T) currentNode.getValue();
		
		return target;
	}
	
	@Override
	public T poll() {
		T value = null;
		if(head != null)
		{
			value = (T) head.getValue();
			Node temp = head.getNextNode();
			head.setNextLink(null);
			head = temp;
			currentSize--;
		}
		return value;
	}

	@Override
	public boolean offer(T value) 
	{
		boolean worked = false;
		
		if(fixedSize)
		{
			if(currentSize < maxSize)
			{
				Node newNode = new Node(value);
				if(head != null)
				{
					head = newNode;
				}
				else
				{
					tail.setNextLink(newNode);
				}
				
				tail = newNode;
				
				worked = true;
				currentSize++;
			}
		}
		else
		{
			Node newNode = new Node(value);
			if(head == null)
			{
				head = newNode;
			}
			else
			{
				tail.setNextLink(newNode);
			}
			
			tail = newNode;
			
			worked = true;
			currentSize++;
		}
		
		
		return worked;
	}

	@Override
	public T peek() {
		T value = (T) head.getValue();
		return value;
	}
}