package arrayTypes;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import arrayTypes.LinkedList.Node;

public class ArrayList<T> implements List<T> {

	int currentContanierSize;
	int numContents;
	int currentEndIndex;
	T[] contanier;
	
	public ArrayList()
	{
		contanier = (T[]) new Object[10];
		currentEndIndex = 0;
		currentContanierSize = 10;
		numContents = 0;
	}
	
	public ArrayList(int startingSize)
	{
		contanier = (T[]) new Object[startingSize];
		currentEndIndex = 0;
		currentContanierSize = 10;
		numContents = 0;
	}
	
	public void expainContanierSize(int newSize)
	{
		T[] newContanier = (T[]) new Object[newSize];
		
		for(int step = 0; step < currentContanierSize && step < newSize; step++)
		{
			newContanier[step] = contanier[step];
		}
		
		currentContanierSize = newSize;
		contanier = newContanier;
		numContents = 0;
	}
	
	@Override
	public boolean add(T value) {
		boolean resultOfOperation = false;
	
		if(currentEndIndex < currentContanierSize)
		{
			if(contanier[currentEndIndex] == null)
			{
				contanier[currentEndIndex] = value;
			}
			else
			{
				int nullIndex = ++currentEndIndex;
				while(contanier[nullIndex] != null && nullIndex < currentContanierSize)
				{
					nullIndex++;
				}
				if(nullIndex < currentContanierSize)
				{
					contanier[nullIndex] = value;
				}
				else
				{
					expainContanierSize(2 * currentContanierSize);
					contanier[nullIndex] = value;
				}
			}
			currentEndIndex++;
		}
		else
		{
			int expandBy = 2 * currentContanierSize;
			expainContanierSize(expandBy);
			contanier[currentEndIndex] = value;
			currentEndIndex++;
		}
		
		resultOfOperation = true;
		numContents++;
		
		
		return resultOfOperation;
	}

	@Override
	public void add(int index, T value) {	
		
		if(currentEndIndex < currentContanierSize)
		{
			if(index < currentContanierSize)
			{
				T[] newContanier = (T[]) new Object[currentContanierSize];
				for(int step = 0; step < currentContanierSize; step++)
				{
					if(step == index)
					{
						newContanier[step] = value;
						newContanier[step + 1] = contanier[step];
						step++;
					}
					else
					{
						newContanier[step] = contanier[step];
					}
				}
				contanier = newContanier;
				currentEndIndex++;
			}
			else
			{
				//throw exception
			}
		}
		else
		{
			int expandBy = 2 * currentContanierSize;
			expainContanierSize(expandBy);
			add(index, value);
		}
		numContents++;
		
	}

	@Override
	public boolean addAll(Collection<? extends T> arg0) {
		boolean worked = true;
		
		for(T var : arg0)
		{
			worked = add(var);
			if(!worked)
			{
				break;
			}
		}
		
		return worked;
	}

	@Override
	public boolean addAll(int arg0, Collection<? extends T> arg1) {
		boolean worked = true;
		
		int currentIndex = arg0;
		for(T var : arg1)
		{
			add(currentIndex++, var);
		}
		
		return worked;
	}

	@Override
	public void clear() {
		contanier = (T[]) new Object[10];
		currentEndIndex = 0;
	}

	@Override
	public boolean contains(Object arg0) {
		boolean wasFound = false;
		T objectToFind = (T) arg0;
		
		for(T var : contanier)
		{
			if(var.equals(objectToFind))
			{
				wasFound = true;
				break;
			}
		}
		
		
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		boolean allWasFound = true;
		
		for(T var : contanier)
		{
			allWasFound = arg0.contains(arg0);
			if(!allWasFound)
			{
				break;
			}
		}
		
		return allWasFound;
	}

	@Override
	public T get(int index) {
		
		T target = null;
		
		if(index < currentContanierSize)
		{
			target = contanier[index];
		}
		else
		{
			//throw exception
		}
		
		return target;
	}

	@Override
	public int indexOf(Object arg0) {
		int index = 0;
		T objectToFind = (T) arg0;
		
		for(int step = 0; step < currentContanierSize; step++)
		{
			if(contanier[index].equals(objectToFind))
			{
				index = step;
				break;
			}
		}
		
		return index;
	}

	@Override
	public boolean isEmpty() {
		boolean truth = false;
		
		if(currentEndIndex == 0)
		{
			truth = true;
		}
		
		return truth;
	}

	@Override
	public Iterator<T> iterator() {
		return Arrays.asList(contanier).iterator();
	}

	@Override
	public int lastIndexOf(Object arg0) {
		
		int lastIndex = -1;
		T objectToFind = (T) arg0;
		
		for(int step = 0; step < currentContanierSize; step++)
		{
			if(contanier[step].equals(objectToFind))
			{
				lastIndex = step;
			}
		}
		
		return lastIndex;
	}
	
	@Override
	public int size() {
		return numContents;
	}
	
	@Override
	public ListIterator<T> listIterator() {
		List<T> temp = Arrays.asList(contanier);
		return temp.listIterator();
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		List<T> temp = Arrays.asList(contanier);
		
		for(int step = 0; step < index; step++)
		{
			temp.remove(0);
		}
		
		return temp.listIterator();
	}

	@Override
	public boolean remove(Object arg0) {
		boolean worked = false;
		
		int targetLocation = indexOf(arg0);
		
		if(targetLocation != -1 && numContents != 0)
		{
			numContents--;
			if(targetLocation < currentContanierSize)
			{
				for(int step = targetLocation; step < currentContanierSize; step++)
				{
					if((step + 1) < currentContanierSize)
					{
						contanier[step] = contanier[step++];
					}
					else
					{
						contanier[step] = null;
					}
				}
			}
		}
		worked = true;
		
		return worked;
	}

	@Override
	public T remove(int index) {
		T target = null;
		
		if(index < currentContanierSize && numContents != 0)
		{
			target = contanier[index];
			
			numContents--;
			
			for(int step = index; step < currentContanierSize; step++)
			{
				if((step + 1) < currentContanierSize)
				{
					contanier[step] = contanier[step++];
				}
				else
				{
					contanier[step] = null;
				}
			}
			
		}
		
		return target;
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		boolean results = true;
		
		for(Object var : arg0)
		{
			T target = (T) var;
			for(int step = 0; step < currentContanierSize; step++)
			{
				if(contanier[step].equals(target))
				{
					T value = remove(step);
					if(value == null)
					{
						results = false;
					}
				}
				
				if(!results)
				{
					break;
				}
			}
		}
		
		return results;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		boolean results = true;
		
		for(int step = 0; step < currentContanierSize; step++)
		{
			boolean found = arg0.contains(contanier[step]);
			if(!found)
			{
				remove(step);
			}
		}
		
		return results;
	}

	@Override
	public T set(int index, T newValue) {
		T oldValue = null;
		
		if(index < currentContanierSize)
		{
			oldValue = contanier[index];
			contanier[index] = newValue;
		}
		
		return oldValue;
	}

	@Override
	public List<T> subList(int arg0, int arg1) {
		List<T> results = Arrays.asList(contanier);
		
		//remove up to
		for(int step = 0; step < arg0; step++)
		{
			results.remove(0);
		}
		
		//remove all past
		for(int step = 0; arg1 < step && step < currentContanierSize; step++)
		{
			results.remove(arg1);
		}
		
		return results;
	}

	@Override
	public Object[] toArray() {
		
		Object[] collection = contanier;
		return collection;
	}

	@Override
	public <T> T[] toArray(T[] arg0) {
		return (T[]) contanier;
	}

}
