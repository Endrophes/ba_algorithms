package priorityQueue;

import java.util.ArrayList;

public class AvlBasedPriorityQueue<T extends Comparable> {

	private class Node<T extends Comparable>
	{
		T value;
		
		Node Parent;
		
		Node left;
		Node right;
		
		public Node(T newValue)
		{
			value = newValue;
			left = null;
			right = null;
		}
		
		public Node()
		{
			value = null;
			left = null;
			right = null;
		}
	}
	
	Node root;
	
	Node parentOfUnbalenced;
	Node unbalencedNode;
	boolean left = false;
	

	private void println(String stuff)
	{
		System.out.println(stuff);
	}
	
	private void balenceTree()
	{
		//balenceTreeAtTarget
		int nodeAbalence = findBalenceFactor(unbalencedNode);
		int nodeBbalence = 0;
		
		Node pivot = null;
		
		if(nodeAbalence >= 2)
		{
			nodeBbalence =  findBalenceFactor(unbalencedNode.left);
		}
		else if(nodeAbalence <= -2)
		{
			nodeBbalence =  findBalenceFactor(unbalencedNode.right);
		}

		println("nodeAbalence: " + nodeAbalence);
		println("nodeBbalence: " + nodeBbalence);
		
		
		if(nodeAbalence == -2 && (nodeBbalence == -1 || nodeBbalence == 0))
		{
			//left
			pivot = leftRotatoion(unbalencedNode, false);
			
		}
		else if(nodeAbalence == 2 && (nodeBbalence == 1 || nodeBbalence == 0))
		{
			//right
			pivot = rightRotation(unbalencedNode, false);
		}
		else if(nodeAbalence == -2 && nodeBbalence == 1)
		{
			//right
			pivot = rightRotation(unbalencedNode, true);
			
			//left
			pivot = leftRotatoion(unbalencedNode, false);
		}
		else if(nodeAbalence == 2 && nodeBbalence == -1)
		{	
			//left
			pivot = leftRotatoion(unbalencedNode, true);
			
			//right
			pivot = rightRotation(unbalencedNode, false);
		}
		else if((nodeAbalence == 2 && nodeBbalence == 2) || (nodeBbalence == 2 || nodeBbalence == -2) || (nodeBbalence == -2 || nodeBbalence == 2) || (nodeBbalence == 3 || nodeBbalence == 1) || (nodeBbalence == 3 || nodeBbalence == 0))
		{
			println("nodeA value: " + unbalencedNode.value);
			println("nodeB value: " + unbalencedNode.right.value);
		}
		
		
		
	
		if(pivot.Parent == null)
		{
			root = pivot;
		}
		else
		{
			T currentChildValue = (T) unbalencedNode.value;
			Node parentOfTree = pivot.Parent;
			
			if(parentOfTree.left != null)
			{
				int valuecheck = parentOfTree.left.value.compareTo(currentChildValue);
				if(valuecheck == 0)
				{
					left = true;
				}
				else
				{
					left = false;
				}
			}
			else
			{
				left = false;
			}
			
			
			if(left)
			{
				parentOfTree.left = pivot;
			}
			else
			{
				parentOfTree.right = pivot;
			}
			
		}
		
		parentOfUnbalenced = null;
		unbalencedNode = null;
		left = false;
		
	}
	
	private Node rightRotation(Node subRoot, boolean toStick)
	{
		Node newRoot = null;
		Node masterParent = subRoot.Parent;
		
		if(toStick)
		{
			newRoot = subRoot;
			
			Node nodeA = newRoot.right;
			Node nodeB = nodeA.left;
			
			newRoot.right = nodeB;
			nodeA.left = nodeB.right;
			
			nodeB.right = nodeA;
		}
		else
		{
			newRoot = subRoot.left;
			
			Node nodeA = subRoot;
			Node nodeB = newRoot.left;
			
			nodeA.left = newRoot.right;
			
			newRoot.left = nodeB;
			newRoot.right = nodeA;
			
			nodeA.Parent = newRoot;
			nodeB.Parent = newRoot;
			
		}
		
		newRoot.Parent = masterParent;
		
		return newRoot;
		
	}
	
	private Node leftRotatoion(Node subRoot, boolean toStick)
	{
		Node newRoot = null;
		Node masterParent = subRoot.Parent;
		
		if(toStick)
		{
			newRoot = subRoot;
			
			Node nodeA = subRoot.left;
			Node nodeB = nodeA.right;
			
			nodeA.right = nodeB.left;
			
			
			newRoot.left = nodeB;
			nodeB.left = nodeA;
		}
		else
		{
			newRoot = subRoot.right;
			
			Node nodeA = subRoot;
			Node nodeB = newRoot.right;
			
			nodeA.right = newRoot.left;
			
			newRoot.left = nodeA;
			newRoot.right = nodeB;
			
			nodeA.Parent = newRoot;
			nodeB.Parent = newRoot;
		}
		
		newRoot.Parent = masterParent;
		
		return newRoot;
	}
	
	public boolean offer(T data)
	{
		boolean result = false;
		
		Node newNode = new Node(data);
		
		if(root == null)
		{
			root = newNode;
			result = true;
		}
		else
		{
			result = placeNewNode(root, newNode);
		}
		
		//boolean allIsGood = checkTreeBalence(root);
		//if(!allIsGood)
		//{
		//	balenceTree();
		//}
		
		return result;
	}

	boolean subcheck = true;
	
	private boolean placeNewNode(Node currentNode, Node newNode)
	{
		boolean result = true;
		
		int compare = newNode.value.compareTo(currentNode.value);
		
		if(compare < 0)
		{
			//go left
			if(currentNode.left != null)
			{
				result = placeNewNode(currentNode.left, newNode);
			}
			else
			{
				newNode.Parent = currentNode;
				currentNode.left = newNode;
			}
		}
		else if(compare > 0)
		{
			//go right
			if(currentNode.right != null)
			{
				result = placeNewNode(currentNode.right, newNode);
			}
			else
			{
				newNode.Parent = currentNode;
				currentNode.right = newNode;
			}
		}
		else
		{
			result = false;
		}
		
		//if(!subcheck)
		//{
			//parentOfUnbalenced = currentNode;
			//System.out.println("UnbalencedNode: " + currentNode.value + " unbalecesFactor: " + unbalecesFactor);
			//balenceTree();
			//subcheck = true;
		//}
		
		//subcheck = checkTreeBalence(currentNode);
		
		boolean balenced = checkTreeBalence(currentNode);
		//println("currentNode Vale: " + currentNode.value);
		if(!balenced)
		{
			//System.out.println("UnbalencedNode: " + currentNode.value + " unbalecesFactor: " + unbalecesFactor);
			balenceTree();
		}
		
		return result;
	}
int unbalecesFactor = 0;
	
	private boolean checkTreeBalence(Node currentNode)
	{
		boolean result = true;
		int  balenceFactor = findBalenceFactor(currentNode);
		
		//System.out.println("currentNode Value: " + currentNode.value);
		//System.out.println("balenceFactor: " + balenceFactor);
		
		if(balenceFactor > 1 || balenceFactor < -1)
		{
			result = false;
			unbalencedNode = currentNode;
			unbalecesFactor = balenceFactor;
		}
		else
		{
			if(currentNode.left != null)
			{
				result = checkTreeBalence(currentNode.left);
				//if(!result)
				//{
				//	parentOfUnbalenced = currentNode;
				//	left = true;
				//}
			}
			
			if(currentNode.right != null && result)
			{
				result = checkTreeBalence(currentNode.right);
				//if(!result)
				//{
				//	parentOfUnbalenced = currentNode;
				//	left = false;
				//}
			}
		}
		
		return result;
	}
	
	private int getMax(int a, int b)
	{
		int results = a;
		if(b > a)
		{
			results = b;
		}
		return results;
	}

	public int findBalanceFactor() {
		return findBalenceFactor(root);
	}
	
	private int findBalenceFactor(Node currentNode)
	{
		
		int bfL = 0;
		int bfR = 0;
		
		if(currentNode != null)
		{
			if(currentNode.left != null)
			{
				bfL = traverseCounter(currentNode.left, 1);
			}
			
			if(currentNode.right != null)
			{
				bfR = traverseCounter(currentNode.right, 1);
			}
		}
		
		//System.out.println("bfL: " + bfL + " bfR: " + bfR);
		
		int finalBF =  bfL - bfR;
		return finalBF;
	}
	
	private int traverseCounter(Node currentNode, int count)
	{
		int tempCount =  count + 1;
		if(currentNode.left != null && currentNode.right != null)
		{
			int left = traverseCounter(currentNode.left, tempCount);
			int right = traverseCounter(currentNode.right, tempCount);
			count = getMax(left, right);
		}
		else
		{
			if(currentNode.left != null)
			{
				count = traverseCounter(currentNode.left, tempCount);
			}
			else if(currentNode.right != null)
			{
				count = traverseCounter(currentNode.right, tempCount);
			}
		}
		
		return count;
	}
	
	
	public  ArrayList<T> getValues(int selectOrder)
	{
		ArrayList<T> values = new ArrayList<T>();
		 
		if(selectOrder == 1)
		{
			traversePreOrder(root, values);
		}
		else if(selectOrder == 2)
		{
			traverseInOrder(root, values);
		}
		else if(selectOrder == 3)
		{
			traversePostOrder(root, values);
		}
		
		return values;
	}
	
	private void traverseInOrder(Node currentNode, ArrayList<T> values)
	{
		if(currentNode != null)
		{
			System.out.println("Node value: " + currentNode.value);
			if(currentNode.left != null)
			{
				traverseInOrder(currentNode.left, values);
			}
			
			
			values.add((T) currentNode.value);
			
			if(currentNode.right != null)
			{
				traverseInOrder(currentNode.right, values);
			}
		}
	}
	
	private void traversePreOrder(Node currentNode, ArrayList<T> values)
	{
		values.add((T) currentNode.value);
		
		if(currentNode.left != null)
		{
			traverseInOrder(currentNode.left, values);
		}
		
		if(currentNode.right != null)
		{
			traverseInOrder(currentNode.right, values);
		}
	}
	
	private void traversePostOrder(Node currentNode, ArrayList<T> values)
	{
		if(currentNode.left != null)
		{
			traverseInOrder(currentNode.left, values);
		}
		
		if(currentNode.right != null)
		{
			traverseInOrder(currentNode.right, values);
		}
		
		values.add((T) currentNode.value);
	}
	
	private Node findSmallestValue(Node currentNode, Node previos)
	{
		if(currentNode != null)
		{
			if(currentNode.left != null)
			{
				previos = currentNode;
				previos = findSmallestValue(currentNode.left, previos);
			}
		}
		return previos;
	}
	
	public T poll()
	{
		T value = (T) root.value;
		
		Node parentToSmallest = root;
		Node smallest = null;
		
		parentToSmallest = findSmallestValue(root, parentToSmallest);
		
		if(parentToSmallest != null)
		{
			if(parentToSmallest.value.compareTo(root.value) != 0 || parentToSmallest.left != null)
			{
				smallest = parentToSmallest.left;
				value = (T) smallest.value;
				
				//remove
				parentToSmallest.left = smallest.left;
				
				if(smallest.right != null)
				{
					placeNewNode(root, smallest.right);
				}
				
			}
			else
			{
				root = root.right;
			}
		}
		else
		{
			root = null;
		}
		
		if(root != null)
		{
			//balence
			boolean allIsGood = checkTreeBalence(root);
			if(!allIsGood)
			{
				balenceTree();
			}
		}
		
		return value;
	}
	
	
	
	
	public T peek()
	{
		T value = (T) root.value; 
		return value;
	}
}
