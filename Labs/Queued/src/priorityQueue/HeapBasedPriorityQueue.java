package priorityQueue;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HeapBasedPriorityQueue<T extends Comparable>
{

	private class Node<T extends Comparable>
	{
		T value;

		Node left;
		Node right;

		public Node(T newValue)
		{
			value = newValue;
			left = null;
			right = null;
		}

		public Node()
		{
			value = null;
			left = null;
			right = null;
		}
	}

	private T[] heap;
	private int currentSize = 0;
	private int currentSlot = 1;
	//private Class<T> clazz;
	private int totalValues = 0;
	
	public HeapBasedPriorityQueue(int initialSize)
	{
		//clazz = type;
		heap = getArray(initialSize);
		currentSize = initialSize;
	}

	
	
	@SuppressWarnings("hiding")
	public <T> T[] getArray(int size)
	{
		@SuppressWarnings("unchecked")
		//T[] arr = (T[]) Array.newInstance(clazz, size+1);
		T[] arr = (T[]) new Comparable[size+1];
		return arr;
	}

	public List<T> getCollection()
	{
		return Arrays.asList(heap);
	}

	public boolean offer(T data)
	{
		boolean result = false;

		int checkOverFlow = heap.length + 1;
		if (checkOverFlow > currentSize)
		{
			heap[currentSlot++] = data;
			result = true;
		}
		else
		{
			doubleSize();
			heap[currentSlot++] = data;
			result = true;
		}
		
		totalValues++;
		balenceTree(totalValues);
		return result;
	}

	void swap(int indexA, int indexB)
	{
		T a = heap[indexA];
		T b = heap[indexB];
		
		heap[indexA] = b;
		heap[indexB] = a;
	}

	void balenceTree(int currentIndex)
	{
		if(currentIndex != 1)
		{
			int parentIndex = currentIndex / 2;
			
			T parentValue = heap[parentIndex];
			T currentValue = heap[currentIndex];
			
			if(currentValue.compareTo(parentValue) > 0)
			{
				swap(parentIndex, currentIndex);
			}
			
			balenceTree(parentIndex);
		}
	}
	
	private void doubleSize()
	{
		int newSize = currentSize * 2;
		T[] newHeap = getArray(newSize);

		for (int step = 0; step < currentSize; step++)
		{
			newHeap[step] = heap[step];
		}

		heap = newHeap;
		currentSize = newSize;
	}

	private void balenceTreeTopDown(int root)
	{
		//find children
		int leftChildIndex = (2 * root);
		int rightChildIndex = (2 * root) + 1;
		
	
		if(leftChildIndex < currentSlot && rightChildIndex < currentSlot)
		{
			
			//getValues
			T rootValue = heap[root];
			T leftChildValue = heap[leftChildIndex];
			T rightChildValue = heap[rightChildIndex];
			
			if(leftChildValue != null)
			{
				//Check and swap
				if(rootValue.compareTo(leftChildValue) < 0)
				{
					swap(root, leftChildIndex);
					rootValue = heap[root];
				}
			}
			
			if(rightChildValue != null)
			{
				if(rootValue.compareTo(rightChildValue) < 0)
				{
					swap(root, rightChildIndex);
					rootValue = heap[root];
				}
			}
			
			
			if(rightChildValue != null && leftChildValue != null)
			{
				//recurse left 
				balenceTreeTopDown(leftChildIndex);
				
				//recurse right
				balenceTreeTopDown(rightChildIndex);
			}
		}
	}
	
	public T poll()
	{
		T result = heap[1];

		// Remove and balence
		int truePlace = (currentSlot - 1);
		swap(1, truePlace);
		heap[truePlace] = null;
		currentSlot--;
		
		balenceTreeTopDown(1);
		
		return result;
	}

	public T peek()
	{
		T result = heap[1];

		return result;
	}

}
