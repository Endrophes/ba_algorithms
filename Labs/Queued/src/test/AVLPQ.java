package test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Random;

import junit.framework.Assert;

import org.junit.Test;

import priorityQueue.AvlBasedPriorityQueue;

public class AVLPQ {

	//genericHelpers hp = new genericHelpers();
	AvlBasedPriorityQueue<Integer> avlPQ;
	
	private void insertCheck(int value)
	{
		boolean result = avlPQ.offer(value);
		//assertTrue(result);
	}
	
	private void printArrayList(ArrayList<Integer> results)
	{
		for(Integer var : results)
		{
			System.out.print(var + ", ");
		}
		System.out.println("");
	}
	
	private void pollCheck(int shouldBe)
	{
		int result = avlPQ.poll();
		System.out.println("result: " + result);
		assertTrue(result == shouldBe);
	}
	
	//@Test
	public void construction() {
		
		avlPQ = new AvlBasedPriorityQueue<Integer>();
		
		assertTrue(avlPQ != null);
	}
	
	public final static void clearConsole()
	{
	    try
	    {
	        final String os = System.getProperty("os.name");

	        if (os.contains("Windows"))
	        {
	            Runtime.getRuntime().exec("cls");
	        }
	        else
	        {
	            Runtime.getRuntime().exec("clear");
	        }
	    }
	    catch (final Exception e)
	    {
	        //  Handle any exceptions.
	    }
	}
	
	@Test
	public void insertion()
	{
		avlPQ = new AvlBasedPriorityQueue<Integer>();
		
		//boolean result = avlPQ.offer(21);
		//assertTrue(result);
		
		Random r = new Random();
		
		for (int i = 0; i < 200; i++ ) {
			int randomValue = r.nextInt(1000); 
			System.out.println("randomValue: %% " + randomValue + " %%");
			insertCheck(randomValue);
			//ArrayList<Integer> results = avlPQ.getValues(2);
			//printArrayList(results);
		}
		
		//insertCheck(21);
		//insertCheck(50);
		//insertCheck(45);
		//insertCheck(99);
		//insertCheck(49);
		//insertCheck(100);
		//insertCheck(23);
		//insertCheck(150);  //breaks here
		//insertCheck(125);
		//insertCheck(40);
		//insertCheck(10);
		//insertCheck(20);
		//insertCheck(42);
		
		
		//int bfCheck = Math.abs(avlPQ.findBalanceFactor());
		//System.out.println("bfCheck: " + bfCheck);
		//assertTrue(bfCheck < 2);

		
		//ArrayList<Integer> results = avlPQ.getValues(2);
		//printArrayList(results);
	}
	
	//@Test
	public void printTree()
	{
		avlPQ = new AvlBasedPriorityQueue<Integer>();
		
		insertCheck(21);
		insertCheck(50);
		insertCheck(45);
		insertCheck(99);
		insertCheck(12);
		
		ArrayList<Integer> results = avlPQ.getValues(2);
		//printArrayList(results);
		
		assertTrue(avlPQ != null);
	}

	private void getAndPrint()
	{
		ArrayList<Integer> results = avlPQ.getValues(2);
		printArrayList(results);
	}
	
	//@Test
	public void pollTest()
	{
		avlPQ = new AvlBasedPriorityQueue<Integer>();
		
		insertCheck(21);
		insertCheck(50);
		insertCheck(45);
		insertCheck(99);
		insertCheck(12);
		
		getAndPrint();
		
		pollCheck(12);
		getAndPrint();
		
		pollCheck(21);
		getAndPrint();
		
		pollCheck(45);
		getAndPrint();
		
		pollCheck(50);
		getAndPrint();
		
		pollCheck(99);
		getAndPrint();
		
		//System.out.print("polled: " + polled);
		
		assertTrue(avlPQ != null);
	}
	
}
