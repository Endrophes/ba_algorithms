package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import priorityQueue.HeapBasedPriorityQueue;

public class HeapPQ {

	HeapBasedPriorityQueue<Integer> heapPQ;
	
	
	private void insertCheck(int value)
	{
		boolean result = heapPQ.offer(value);
		assertTrue(result);
	}
	
	private void peekCheck(int shouldBe)
	{
		int value = heapPQ.peek();
		assertTrue(value == shouldBe);
	}
	
	private void pollCheck(int shouldBe)
	{
		int value = heapPQ.poll();
		assertTrue(value == shouldBe);
	}
	
	private void setUP(int size)
	{
		heapPQ = new HeapBasedPriorityQueue<Integer>(size);
	}
	
	@Test
	public void construction() {
		setUP(7);
		
		assertTrue(heapPQ != null);
	}
	
	@Test
	public void offer()
	{
		setUP(7);
		
		boolean result = heapPQ.offer(21);
		
		assertTrue(result);
	}
	
	@Test
	public void peek()
	{
		setUP(7);
		
		boolean result = heapPQ.offer(21);
		
		int value = heapPQ.peek();
		
		assertTrue(result);
		assertTrue(value == 21);
	}
	
	@Test
	public void printHeap()
	{
		setUP(7);
		
		insertCheck(24);
		insertCheck(9);
		insertCheck(45);
		insertCheck(7);
		insertCheck(13);
		insertCheck(10);
		insertCheck(64);
		
		//peekCheck(64);
		
		List<Integer> heap = heapPQ.getCollection();
		
		System.out.println("Length: " + (heap.size()));
		
		for(Integer var : heap)
		{
			if(var != null)
			{
				System.out.print(var + ", ");
			}
		}
		
		System.out.println();
	}
	
	@Test
	public void pollTest()
	{
		setUP(7);
		
		insertCheck(24);
		insertCheck(9);
		insertCheck(45);
		insertCheck(7);
		insertCheck(13);
		insertCheck(10);
		insertCheck(64);
		
		pollCheck(64);
		pollCheck(45);
		pollCheck(24);
		pollCheck(13);
		pollCheck(10);
		pollCheck(9);
		pollCheck(7);
		
		List<Integer> heap = heapPQ.getCollection();
		
		System.out.println("Length: " + (heap.size()));
		
		for(Integer var : heap)
		{
			if(var != null)
			{
				System.out.print(var + ", ");
			}
		}
		
		System.out.println();
	}
}
