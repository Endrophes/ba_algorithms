package searchAlgo;

import graph.Graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class DfsGraphTraversal
{
	Graph currentGraph;
	int numVerts = 0;
	int currentVert = 0;
	
	boolean isAcycile = true;
	
	List<Integer> stack = new ArrayList<Integer>();
	List<Integer> currentSet = new ArrayList<Integer>();
	List<List<Integer>> results = new ArrayList<List<Integer>>();
	
	public boolean isGraphAcycile()
	{
		return isAcycile;
	}
	
	public List<List<Integer>> traverse(Graph g)
	{
		//set up
		numVerts = g.vcount();
		currentGraph = g;
		//q = new LinkedList<Integer>();
		
		//action
		exicute();
		
		//reset
		//q.clear();
		numVerts = 0;
		currentGraph.clearMarks();
		currentGraph = null;
		return results;
	}
	
	private boolean checkVerts()
	{
		boolean allVisted = true;
		for(int step = 0; step < numVerts; step++)
		{
			int currentMark = currentGraph.getMark(step);
			if(currentMark == 0)
			{
				allVisted = false;
				currentVert = step;
				break;
			}
		}

		if(allVisted)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	private void exicute()
	{
		boolean allComponetsNotFound = true;
		
		while(allComponetsNotFound)
		{
			currentSet = new ArrayList<Integer>();
			dive(currentVert);
			results.add(currentSet);
			allComponetsNotFound = checkVerts();
		}
	}
	
	private void preVisit(int v)
	{
		stack.add(v);
	}
	
	private void postVisit(int v)
	{
		stack.remove((Object)v);
		currentSet.add(v);
	}
	
	private void checkAcycile()
	{
		
	}
	
	private void dive(int currentVert)
	{
		preVisit(currentVert);
		int currentMark = currentGraph.getMark(currentVert);
		currentGraph.setMark(currentVert, ++currentMark);
		
		for(int step = currentGraph.first(currentVert); step < currentGraph.vcount(); step = currentGraph.next(currentVert, step))
		{
			if(stack.contains((Object)step))
			{
				isAcycile = false;
			}
			
			if(currentGraph.getMark(step) == 0)
			{
				dive(step);
			}
		}
		postVisit(currentVert);
	}
	
}
