package graph;

public class Graph
{
	int numVertices;
	
	private int[][] matrix; //for tracking edges
	private int[] marks;    //for coloring vertices
	private int totalEdgeCount = 0;
	
	// initializes a graph of v vertices with no edges
	public Graph(int v)
	{
		numVertices = v;
		matrix = new int[v][v];
		marks = new int[v];
	}
	
	// returns number of vertices (whether connected or not) in the graph
	public int vcount()
	{
		return numVertices;
	}
	
	// returns the number of edges in the graph
	public int ecount()
	{		
		return totalEdgeCount;
	}
	
	 // returns the first vertex (in natural order) connected to vertex v.  
	//	If there are none, then vcount() is returned
	public int first(int v)
	{
		int count = numVertices;
		int result = count;
		for(int i = 0; i < count; i++)
		{
			if(matrix[v][i] != 0)
			{
				result = i;
				break;
			}
		}
		return result;
	}
	
	// returns the vertex (in natural order) connected to vertex v after vertex w. 
	// If there are no more edges after w, vcount() is returned
	public int next(int vertex, int lastVisitedNeighbor)
	{
		int count = numVertices;
		int result = count;
		for(int i = lastVisitedNeighbor + 1; i < count; i++)
		{
			if(matrix[vertex][i] != 0)
			{
				result = i;
				break;
			}
		}
		return result;
	}
	
	public int degree(int vertex)
	{
		int numNabors = 0;
		
		int naborIndex = 0;
		
		while(naborIndex != numVertices)
		{
			naborIndex = next(vertex, naborIndex);
			if(naborIndex != numVertices)
			{
				numNabors++;
			}
		}
		
		if(numNabors == 0)
		{
			numNabors = numVertices;
		}
		
		return numNabors;
	}
	
	public void addEdge(int vertex, int neighbor, int weight)
	{
		matrix[vertex][neighbor] = weight;
		totalEdgeCount++;
	}
	
	public void addMirrorEdge(int vertex, int neighbor, int weight)
	{
		matrix[vertex][neighbor] = weight;
		matrix[neighbor][vertex] = weight;
		totalEdgeCount += 2;
	}
	
	public void removeEdge(int vertex, int neighbor)
	{
		matrix[vertex][neighbor] = 0;
		totalEdgeCount--;
	}
	
	public void removeMirrorEdge(int vertex, int neighbor)
	{
		matrix[vertex][neighbor] = 0;
		matrix[neighbor][vertex] = 0;
		totalEdgeCount -= 2;
	}
	
	public boolean isEdge(int vertex, int neighbor)
	{
		return matrix[vertex][neighbor] != 0;
	}
	
	public int getMark(int vertex)
	{
		return marks[vertex];
	}
	
	public void setMark(int vertex, int mark)
	{
		marks[vertex] = mark;
	}

	public void clearMarks()
	{ 
		for(int step = 0; step < numVertices; step++)
		{
			marks[step] = 0;
		}
	}
	
	public int[][] getGrid()
	{
		return matrix;
	}
}
