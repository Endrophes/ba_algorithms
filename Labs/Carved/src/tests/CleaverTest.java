package tests;

import static org.junit.Assert.*;
import lab.SeamCarver;
import org.junit.Test;
import edu.neumont.ui.Picture;

public class CleaverTest
{

	SeamCarver carver;
	String picLocation = "D:\\TestPics\\";
	
	//Helpers
	private void setup(Picture pic)
	{
		carver = new SeamCarver(pic);
		assertTrue(carver != null);
	}
	
	private Picture loadImage(String file)
	{
		Picture pic = new Picture(file);
		assertTrue(pic != null);
		return pic;
	}
	
	private void outPutImage(Picture pic, String location)
	{
		pic.save(location);
	}
	
	private void println(String stuff)
	{
		System.out.println(stuff);
	}
	
	private void print(String stuff)
	{
		System.out.print(stuff);
	}
	
	
	//Tests
	//@Test
	public void construction()
	{
		Picture testPicOne = loadImage(picLocation + "JB.png");
		setup(testPicOne);
	}
	
	//@Test
	public void getImageBack()
	{
		Picture testPicOne = loadImage(picLocation + "JB.png");
		setup(testPicOne);
		
		Picture imageReturned =  carver.getPicture();
		
		assertTrue(imageReturned == testPicOne);
	}

	//@Test
	public void checkSize()
	{
		Picture testPicOne = loadImage(picLocation + "JB.png");
		setup(testPicOne);
		
		int trueWidth = 399;
		int trueHeight = 480;
		
		int gotWidth = carver.width();
		int gotHeight = carver.height();
		
		//println("gotWidth: " + gotWidth);
		//println("gotHeight: " + gotHeight);
		
		assertTrue(gotWidth == trueWidth);
		assertTrue(gotHeight == trueHeight);
	}
	
	//@Test
	public void energyTestEquals()
	{
		Picture testPicOne = loadImage(picLocation + "JB.png");
		setup(testPicOne);
		
		//Position One
		int x1 = 84;
		int y1 = 60;
		
		//Position Two
		int x2 = 36;
		int y2 = 50;
		
		double result1 = carver.energy(x1, y1);
		double result2 = carver.energy(x2, y2);
		
		println("result1: " + result1);
		println("result2: " + result2);
		
		assertTrue(result1 == result2);
	}
	
	//@Test
	public void energyTestNotEquals()
	{
		Picture testPicOne = loadImage(picLocation + "JB.png");
		setup(testPicOne);
		
		//Position One
		int x1 = 204;
		int y1 = 110;
		
		//Position Two
		int x2 = 36;
		int y2 = 50;
		
		double result1 = carver.energy(x1, y1);
		double result2 = carver.energy(x2, y2);
		
		println("result1: " + result1);
		println("result2: " + result2);
		
		assertTrue(result1 != result2);
	}
	
	//@Test
	public void verticalFindSeamTest()
	{
		Picture testPicOne = loadImage(picLocation + "JB.png");
		setup(testPicOne);
		
		int[] results = carver.findVerticalSeam();
		assertTrue(results != null);
	}
	
	//@Test
	public void horizontalFindSeamTest()
	{
		Picture testPicOne = loadImage(picLocation + "JB.png");
		setup(testPicOne);
		
		int[] results = carver.findHorizontalSeam();
		assertTrue(results != null);
	}
	
	//@Test
	public void verticalRemoveSeamTest()
	{
		Picture testPicOne = loadImage(picLocation + "JB.png");
		setup(testPicOne);
		
		int[] results = carver.findVerticalSeam();
		assertTrue(results != null);
		
		carver.removeVerticalSeam(results);
		
		testPicOne = carver.getPicture();
		outPutImage(testPicOne,picLocation + "Output\\JB-R1-V.png");
	}
	
	//@Test
	public void horizontalRemoveSeamTest()
	{
		Picture testPicOne = loadImage(picLocation + "JB.png");
		setup(testPicOne);
		
		int[] results = carver.findHorizontalSeam();
		assertTrue(results != null);
		
		carver.removeHorizontalSeam(results);
		testPicOne = carver.getPicture();
		outPutImage(testPicOne, picLocation + "Output\\JB-R1-H.png");
	}

	@Test
	public void secretMessege()
	{
		Picture testPicOne = loadImage(picLocation + "SM.png");
		setup(testPicOne);
		
		
		int cutVertNum = 0;
		int cutHorzNum = 0;
		
		//cutVertNum = 160;
		//cutHorzNum = 200;
		
		//cutVertNum = 172;
		//cutHorzNum = 133;
		
		cutVertNum = 150;
		cutHorzNum = 100;
		
		if(cutVertNum != 0)
		{
			println("////Vertical Cuts/////");
		}
		
		for(int step = 0; step < cutVertNum; step++)
		{
			int[] results = carver.findVerticalSeam();
			//printEnergyofIndexPixels(results);
			carver.removeVerticalSeam(results);
		}
		
		if(cutHorzNum != 0)
		{
			println("////Horizontal Cuts/////");
		}
		
		for(int step = 0; step < cutHorzNum; step++)
		{
			int[] results = carver.findHorizontalSeam();
			//printEnergyofIndexPixels(results);
			carver.removeHorizontalSeam(results);
		}
		
		String extra = "-";
		
		if(cutVertNum != 0)
		{
			extra += "V";
		}
		
		
		if(cutHorzNum != 0)
		{
			extra += "H";
		}
		
		
		testPicOne = carver.getPicture();
		outPutImage(testPicOne, picLocation + "Output\\SM" + extra + ".png");
		println("!!!!!!Done!!!!!!");
	}
	
	private void printEnergyofIndexPixels(int[] indexes)
	{
		for(int var : indexes)
		{
			print(var + ", ");
		}
		
		println("");
		
		for(int var : indexes)
		{
			double energy = carver.energy(var);
			print(energy + ", ");
		}
		println("");
	}
	
	//@Test
	public void smallImage()
	{
		String fileName = "test3";
		String fileType = ".png";
		String extra = "-";
		Picture testPicOne = loadImage(picLocation + fileName + fileType);
		setup(testPicOne);
		
		int cutVertNum = 0;
		int cutHorzNum = 0;
		
		//cutVertNum = 2;
		cutHorzNum = 2;
		
		if(cutVertNum != 0)
		{
			println("////Vertical Cuts/////");
		}
		for(int step = 0; step < cutVertNum; step++)
		{
			int[] results = carver.findVerticalSeam();
			printEnergyofIndexPixels(results);
			carver.removeVerticalSeam(results);
		}
		
		if(cutHorzNum != 0)
		{
			println("////Horizontal Cuts/////");
		}
		for(int step = 0; step < cutHorzNum; step++)
		{
			int[] results = carver.findHorizontalSeam();
			printEnergyofIndexPixels(results);
			carver.removeHorizontalSeam(results);
		}
		
		if(cutVertNum != 0)
		{
			extra += "V";
		}
		
		
		if(cutHorzNum != 0)
		{
			extra += "H";
		}
		
		testPicOne = carver.getPicture();
		outPutImage(testPicOne, picLocation + "Output\\" + fileName + extra + "-smT.png");
	}
	
}
