package tests;

import java.util.List;

import graph.Graph;

import org.junit.Test;

import searchAlgo.DfsGraphTraversal;
import sorting.TropologicalSort;
import static org.junit.Assert.*;

public class TropologicalSortTest
{

	TropologicalSort tps = new TropologicalSort();
	DfsGraphTraversal dfs;
	Graph g;
	
	//////Helpers///////
	private void assertSetUp(int numVertices)
	{
		g = new Graph(numVertices);
		dfs = new DfsGraphTraversal();
		assertTrue(g != null);
		assertTrue(tps != null);
		assertTrue(dfs != null);
	}
	
	private void println(String stuff)
	{
		System.out.println(stuff);
	}
	
	private void print(String stuff)
	{
		System.out.print(stuff);
	}
	
	private void printGrid(int[][] theGrid)
	{
		int height = theGrid.length;
		int width = theGrid.length;
		for(int stepA = 0; stepA < height; stepA++)
		{
			for(int stepB = 0; stepB < width; stepB++)
			{
				print("" + theGrid[stepA][stepB] + ", ");
			}
			println("");
		}
	}
	
	private void printListListInt(List<List<Integer>> toPrint)
	{
		int num = 1;
		for(List<Integer> set : toPrint)
		{
			print("Connected Componet # " + num + " : ");
			for(Integer var : set)
			{
				print(var + ", ");
			}
			println("");
			num++;
		}
	}
	
	////Tests////
	@Test
	public void construction()
	{
		assertTrue(tps != null);
	}
	
	@Test
	public void testSorting()
	{
		assertSetUp(5);
		
		//one
		g.addEdge(0, 1, 1);
		
		//two
		//g.addEdge(1, 0, 1);
		g.addEdge(1, 2, 1);
		
		//three
		//g.addEdge(2, 1, 1);
		g.addEdge(2, 3, 1);
		
		//four
		//g.addEdge(3, 2, 1);
		g.addEdge(3, 4, 1);
		
		//five
		//g.addEdge(4, 3, 1);
		
		println("=================");
		printGrid(g.getGrid());
		println("=================");
		
		//First run
		println("Depth Search:");
		List<List<Integer>> dfSResult = dfs.traverse(g);
		if(dfSResult != null)
		{
			printListListInt(dfSResult);
		}
		
		//Second run
		List<Integer> tsResults;
		try
		{
			tsResults = tps.sort(g);
			print("Tropological Sort: ");
			println(tsResults.toString());
		} catch (Exception e)
		{
			println("Exception wast thrown: " + e.getMessage());
		}
		
	}
	
	@Test
	public void nonAcyclic()
	{
		assertSetUp(7);
		
		//one
		g.addEdge(0, 1, 1);
		g.addEdge(1, 6, 1);
		g.addEdge(6, 5, 1);
		g.addEdge(5, 0, 1);
		
		println("=================");
		printGrid(g.getGrid());
		println("=================");
		
		//First run
		println("Depth Search:");
		List<List<Integer>> dfSResult = dfs.traverse(g);
		if(dfSResult != null)
		{
			printListListInt(dfSResult);
		}
		
		//Second run
		List<Integer> tsResults;
		try
		{
			tsResults = tps.sort(g);
			print("Tropological Sort: ");
			println(tsResults.toString());
		} catch (Exception e)
		{
			println("Exception wast thrown: " + e.getMessage());
		}
	}
}
