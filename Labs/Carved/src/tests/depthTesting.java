package tests;

import graph.Graph;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import searchAlgo.DfsGraphTraversal;

public class depthTesting
{
	Graph grid;
	DfsGraphTraversal dfs;
	
	//helpers
	private void setup(int numVertices)
	{
		grid = new Graph(numVertices);
		dfs = new DfsGraphTraversal();
	}
	
	private void assertSetUp(int numVertices)
	{
		setup(numVertices);
		assertTrue(grid != null);
		assertTrue(dfs != null);
	}
	
	private void mirror(int a, int b, int c)
	{
		grid.addEdge(a, b, c);
		grid.addEdge(b, a, c);
	}
	
	private void println(String stuff)
	{
		System.out.println(stuff);
	}
	
	private void print(String stuff)
	{
		System.out.print(stuff);
	}
	
	private void printGrid(int[][] theGrid)
	{
		int height = theGrid.length;
		int width = theGrid.length;
		for(int stepA = 0; stepA < height; stepA++)
		{
			for(int stepB = 0; stepB < width; stepB++)
			{
				print("" + theGrid[stepA][stepB] + ", ");
			}
			println("");
		}
	}
	
	private void printListListInt(List<List<Integer>> toPrint)
	{
		int num = 1;
		for(List<Integer> set : toPrint)
		{
			print("Connected Componet # " + num + " : ");
			for(Integer var : set)
			{
				print(var + ", ");
			}
			println("");
			num++;
		}
	}
	
	//Testers
	//@Test
	public void construction()
	{
		assertSetUp(5);
	}
	
	//@Test
	public void Traverse()
	{
		assertSetUp(5);
		
		//one
		grid.addEdge(0, 1, 1);
		
		//two
		grid.addEdge(1, 0, 1);
		grid.addEdge(1, 2, 1);
		
		//three
		grid.addEdge(2, 1, 1);
		grid.addEdge(2, 3, 1);
		
		//four
		grid.addEdge(3, 2, 1);
		grid.addEdge(3, 4, 1);
		
		//five
		grid.addEdge(4, 3, 1);
		
		
		//Test?
		println("=================");
		printGrid(grid.getGrid());
		println("=================");
		
		List<List<Integer>> testResult = dfs.traverse(grid);
		if(testResult != null)
		{
			printListListInt(testResult);
		}
	}
	
	@Test
	public void Traverse2()
	{
		assertSetUp(8);
		
		mirror(0, 1, 1);
		mirror(1, 2, 1);
		mirror(0, 2, 1);
		mirror(0, 3, 1);
		mirror(3, 4, 1);
		mirror(3, 5, 1);
		mirror(4, 5, 1);
		mirror(6, 7, 1);
		
		//Test?
		println("=================");
		printGrid(grid.getGrid());
		println("=================");
		
		List<List<Integer>> testResult = dfs.traverse(grid);
		if(testResult != null)
		{
			printListListInt(testResult);
		}
	}
}
