package sorting;


import graph.Graph;

import java.util.List;
import java.util.ArrayList;

import searchAlgo.DfsGraphTraversal;

public class TropologicalSort
{	
	DfsGraphTraversal dfs = new DfsGraphTraversal();
	
	public TropologicalSort()
	{
		
	}
	
	private List<Integer> finalForm(List<List<Integer>> dfsResult)
	{
		List<Integer> finalResults = new ArrayList<Integer>();
		
		int numberOfLists = dfsResult.size() - 1;
		
		for(int listStep = numberOfLists; listStep >= 0; listStep--)
		{
			List<Integer> currentList = dfsResult.get(listStep);
			int numberofInts = currentList.size() - 1;
			
			for(int intStep = numberofInts; intStep >= 0; intStep--)
			{
				finalResults.add(currentList.get(intStep));
			}
			
		}
		
		return finalResults;
	}
	
	public List<Integer> sort(Graph g) throws Exception
	{		
		List<List<Integer>> dfsResult = dfs.traverse(g);
		if(!dfs.isGraphAcycile())
		{
			throw new Exception("Graph is not Acyclit");
		}
		List<Integer> finalResults = new ArrayList<Integer>();
		finalResults = finalForm(dfsResult);
		return finalResults;
	}
}
