package primeIterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PrimeIterator implements Iterator<Integer> {
	
	int currentIndex;
	public List<Integer> collectionOfPrimes;
	
	public PrimeIterator(int max) 
	{
		/**
		* Find all the prime numbers
		**/	
		//Set up markers
		boolean[] isPrime = new boolean[max];
		for(int i = 0; i < max; i++)
		{
			isPrime[i] = true;
		}
		// 1 is not prime
		isPrime[0] = false;
		
		collectionOfPrimes = new ArrayList<Integer>();
		
		for (int i = 2; i < max; i++)
		{
			if(isPrime[i-1])
			{
				collectionOfPrimes.add(i);
				
				//disregard multibles of the prime
				for (int j = 2 * i; j < max; j += i)
				{
					isPrime[j - 1] = false;
				}
			}
		}
		
	}
	
	public boolean hasNext() 
	{
		boolean newNext = false; 
		/**
		* Returns whether this iterator has any more prime numbers less than max
		**/
		if(currentIndex < collectionOfPrimes.size())
		{
			newNext = true;
		}
		
		return newNext;
	}

	public Integer next() 
	{
		/**
		* Gets the next prime number
		**/
		Integer result = 0;
		result = collectionOfPrimes.get(currentIndex);
		currentIndex++;
		return result;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	} 
}

