package steganog;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import primeIterator.PrimeIterator;
import edu.neumont.ui.Picture;

class ClockStopper
{
	private long startTime;
	private long endTime;
	
	public ClockStopper()
	{
		startTime = 0;
		endTime = 0;
	}
	
	private long calcTime()
	{
		return (endTime - startTime);
	}
	
	public void start()
	{
		startTime = System.nanoTime();
	}
	public void end()
	{
		endTime = System.nanoTime();
		System.out.println("Total Time: " + calcTime());
	}	
	public void end(String functionName)
	{
		endTime = System.nanoTime();
		System.out.println("Total Time for "+ functionName  + ": " + calcTime());
	}
}

public class Steganog {
	
	List<ColorPoint> primeIndexPixels;
	
	Steganog()
	{
		primeIndexPixels = new ArrayList<ColorPoint>();
	}
	
	private class ColorPoint
	{
		public Color pixelColor;
		public int xPosition;
		public int yPositon;
		public String hex;
		public String binary;
	}
	
	private String intToHex(int input)
	{
		String result = "";
		String tableOfChar = "0123456789ABCDEF";
		
		if(input == 0)
		{
			result = "00";
		}
		else
		{
			result += tableOfChar.charAt((input-input%16)/16);
			result += tableOfChar.charAt(input%16);
		}
		
		return result;
	}
	
	private String rgbToHex(int red, int green, int blue)
	{
		String resultHex = "";
		
		resultHex += intToHex(red);
		resultHex += intToHex(green);
		resultHex += intToHex(blue);
		
		return resultHex;
	}
	
	private String HexToBinary(String hex)
	{
		String result = "";
		int lengthOfHex = 6;
		String tableOfChar = "0123456789ABCDEF";
		//Length of a hex value is 6 char
		for(int stepper = 0; stepper < lengthOfHex; stepper++)
		{
			//tableOfChar.charAt
			if(hex.charAt(stepper) == tableOfChar.charAt(0))
			{
				result += "0000";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(1))
			{
				result += "0001";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(2))
			{
				result += "0010";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(3))
			{
				result += "0011";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(4))
			{
				result += "0100";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(5))
			{
				result += "0101";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(6))
			{
				result += "0110";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(7))
			{
				result += "0111";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(8))
			{
				result += "1000";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(9))
			{
				result += "1001";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(10))
			{
				result += "1010";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(11))
			{
				result += "1011";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(12))
			{
				result += "1100";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(13))
			{
				result += "1101";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(14))
			{
				result += "1110";
			}
			else if(hex.charAt(stepper) == tableOfChar.charAt(15))
			{
				result += "1111";
			}
		}
		return result;
	}

	private int binaryToInt(String binaryCode)
	{
		int length = binaryCode.length();
		int lastIndex = length - 1;
		int maxValue = 1;
		int decimal = 0;
		
		for(int stepper = 0; stepper < length; stepper++)
		{
			char bit = binaryCode.charAt(lastIndex - stepper);
			if(bit == '1')
			{
				decimal += maxValue;
			}
			
			//set for the next step
			maxValue *= 2;
		}
		
		return decimal;
	}
	
	private String decToBinary(int number)
	{
		String result = "";
		
		if(number == 0)
		{
			result = "0";
		}
		else
		{
			while(number != 0)
			{
				result += (number % 2);
				number = number/2;
			}
			
			String reverse = "";
			
			int index = result.length() - 1;
			
			while(index >= 0)
			{
				reverse += result.charAt(index--);
			}
			
			result = reverse;
		}
		
		return result;
	}
	
	private String getHexValue(String Binary)
	{
		String result = "";
		
		//tableOfChar.charAt
		if(Binary.equals("0000"))
		{
			result += "0";
		}
		else if(Binary.equals("0001"))
		{
			result += "1";
		}
		else if(Binary.equals("0010"))
		{
			result += "2";
		}
		else if(Binary.equals("0011"))
		{
			result += "3";
		}
		else if(Binary.equals("0100"))
		{
			result += "4";
		}
		else if(Binary.equals("0101"))
		{
			result += "5";
		}
		else if(Binary.equals("0110"))
		{
			result += "6";
		}
		else if(Binary.equals("0111"))
		{
			result += "7";
		}
		else if(Binary.equals("1000"))
		{
			result += "8";
		}
		else if(Binary.equals("1001"))
		{
			result += "9";
		}
		else if(Binary.equals("1010"))
		{
			result += "A";
		}
		else if(Binary.equals("1011"))
		{
			result += "B";
		}
		else if(Binary.equals("1100"))
		{
			result += "C";
		}
		else if(Binary.equals("1101"))
		{
			result += "D";
		}
		else if(Binary.equals("1110"))
		{
			result += "E";
		}
		else if(Binary.equals("1111"))
		{
			result += "F";
		}
		
		return result;
	}
	
	private String BinaryToHex(String input)
	{
		String result = "";
		
		int numberOfCuncks = 6;
		List<String> sectionStorage = new ArrayList<String>();
		int charStep = 0;
		for(int chuckStep = 0; chuckStep < numberOfCuncks; chuckStep++)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
		{
			String section = "";
			section += input.charAt(charStep++);
			section += input.charAt(charStep++);
			section += input.charAt(charStep++);
			section += input.charAt(charStep++);
			sectionStorage.add(section);
		}
		
		for(int section = 0; section < 6; section++)
		{
			result += getHexValue(sectionStorage.get(section));
		}
	
		return result;
	}

	private void getPrimePixels(Picture image)
	{
		int heightOfImage = image.height();
		int widthOfImage = image.width();
		int numberOfPixels = heightOfImage * widthOfImage;
		PrimeIterator iteratorControler = new PrimeIterator(numberOfPixels);
		
		//get prime Pixels
		int pixelCount = 0;
		Integer currentPrime = iteratorControler.next();
		for(int stepperY = 0; stepperY < heightOfImage; stepperY++)
		{
			for(int stepperX = 0; stepperX < widthOfImage; stepperX++)
			{
				if(pixelCount == currentPrime)
				{
					ColorPoint prime = new ColorPoint();
					prime.pixelColor = image.get(stepperX, stepperY);
					prime.xPosition = stepperX;
					prime.yPositon = stepperY;
					primeIndexPixels.add(prime);
					
					if(iteratorControler.hasNext())
					{
						currentPrime = iteratorControler.next();
					}
					else
					{
						break;
					}
				}
				pixelCount++;
			}
		}
		
		//get Hex and binary values
		for(int stepper = 0; stepper < primeIndexPixels.size(); stepper++)
		{
			Color CurrentColor = primeIndexPixels.get(stepper).pixelColor;
			primeIndexPixels.get(stepper).hex = "";//rgbToHex(CurrentColor.getRed(), CurrentColor.getGreen(),CurrentColor.getBlue());
			
			int decRed = CurrentColor.getRed();
			int decGreen = CurrentColor.getGreen();
			int decBlue = CurrentColor.getBlue();
			
			String binRed = decToBinary(decRed);
			String binGreen = decToBinary(decGreen);
			String binBlue = decToBinary(decBlue);
			
			while(binRed.length() < 8)
			{
				binRed = "0" + binRed;
			}
			
			while(binGreen.length() < 8)
			{
				binGreen = "0" + binGreen;
			}
			
			while(binBlue.length() < 8)
			{
				binBlue = "0" + binBlue;
			}
			
			
			
			primeIndexPixels.get(stepper).binary = binRed + binGreen + binBlue;
		}
		
	}
	
	public Picture embedIntoImage(Picture cleanImage, String message) throws IOException 
	{
		/**
		* Takes a clean image and changes the prime-indexed pixels to secretly carry the message
		**/
		
		//Find all the prime pixels
		getPrimePixels(cleanImage);
		
		//Embed
		message = message.toUpperCase();
		int length = message.length();
		int numPrime = primeIndexPixels.size();
		for(int stepper = 0; stepper < length && stepper < numPrime; stepper++)
		{
			
			ColorPoint currentPixel = primeIndexPixels.get(stepper);
			
			char part = message.charAt(stepper);
			
			int decValue = (int)part;
			if(decValue != 24)
			{
				decValue -= 32;
			}
			
			String decInBinary = decToBinary(decValue);
			
			String currentBinary = currentPixel.binary;
			
			while(decInBinary.length()  < 6)
			{
				decInBinary = "0" + decInBinary;
			}
			
			int charStepper = 0;
			
			String newBinary = "";
			
			while(charStepper < 24)
			{
				if(charStepper == 6)
				{
					newBinary += decInBinary.charAt(0);
				}
				else if(charStepper == 7)
				{
					newBinary += decInBinary.charAt(1);
				}
				else if(charStepper == 14)
				{
					newBinary += decInBinary.charAt(2);
				}
				else if(charStepper == 15)
				{
					newBinary += decInBinary.charAt(3);
				}
				else if(charStepper == 22)
				{
					newBinary += decInBinary.charAt(4);
				}
				else if(charStepper == 23)
				{
					newBinary += decInBinary.charAt(5);
				}
				else
				{
					newBinary += currentBinary.charAt(charStepper);
				}
				charStepper++;
			}
			
			String newHex = BinaryToHex(newBinary);
			newHex = "0x" + newHex;
			Color newColor = Color.decode(newHex);
			
			currentPixel.binary = newBinary;
			currentPixel.hex = newHex;
			currentPixel.pixelColor = newColor;
			
			primeIndexPixels.set(stepper, currentPixel);
			
		}
		
		for(int stepper = 0; stepper < primeIndexPixels.size(); stepper++)
		{
			ColorPoint newPixel = primeIndexPixels.get(stepper);
			cleanImage.set(newPixel.xPosition, newPixel.yPositon, newPixel.pixelColor);
		}
		
		return cleanImage;
	}

	public String retreiveFromImage(Picture imageWithSecretMessage) throws IOException 
	{
		String result = "";
		/**
		* Retreives the embedded secret from the secret-carrying image
		*/ 
		
		getPrimePixels(imageWithSecretMessage);
		
		//get measeage
		
		int numberOfPrimePixs =  primeIndexPixels.size();
		
		for(int stepper = 0; stepper < numberOfPrimePixs; stepper++)
		{
			String letter = "";
			String currentBinary = primeIndexPixels.get(stepper).binary;
			letter += currentBinary.charAt(6);
			letter += currentBinary.charAt(7);
			letter += currentBinary.charAt(14);
			letter += currentBinary.charAt(15);
			letter += currentBinary.charAt(22);
			letter += currentBinary.charAt(23);
			
			int decValue = binaryToInt(letter);
			
			if(decValue != 24)
			{
				decValue += 32;
			}
			else
			{
				break;
			}
			result += (char)decValue;
		}
		
		return result;
	}
	
	public static void main(String[] args) throws IOException
	{
		//System.out.println("ITS ALIVE!!!!!");
		
		String levelOne = "D:\\imageOne.png";
		String levelTwo = "D:\\Jet.png";
		String levelThere = "D:\\ETNW.png";
		
		Picture retreavedPhoto = new Picture(levelOne);
		Picture fixedPhoto = new Picture(levelOne);
		Steganog testingGround = new Steganog();
		ClockStopper timer = new ClockStopper();
		
		int operation = 2;
		
		char terminator = (char)24;
		
		String messageToWrite = "The tenth Doctor Is my favorite because he reminds me of who I really am.";
		messageToWrite += terminator; 
		String Output = "";
		
		if(operation == 1)
		{
			timer.start();
				Output = testingGround.retreiveFromImage(retreavedPhoto);
			timer.end("retreiveFromImage");
			
			System.out.println(Output);
		}
		else if(operation == 2)
		{
			//timer.start();
			fixedPhoto = testingGround.embedIntoImage(retreavedPhoto, messageToWrite);
			//timer.end("embedIntoImage");
			
			timer.start();
			Output = testingGround.retreiveFromImage(fixedPhoto);
			timer.end("retreiveFromImage");
			
			System.out.println(Output);
		}
		
		fixedPhoto.save("D:\\BASM.png");
	}
}
