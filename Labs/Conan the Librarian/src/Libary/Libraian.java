package Libary;

import java.util.List;

import edu.neumont.csc250.lab4.Book;
import edu.neumont.csc250.lab4.Bookcase;
import edu.neumont.csc250.lab4.Shelver;



public class Libraian implements Shelver{

	int INF = Integer.MAX_VALUE;
	
	void solveWordWrap (int l[], int n, int M)
	{
	    int size = n + 1;
		int extras[][] = new int[size][size];   
	    int lc[][] = new int[size][size];
	    int c[] = new int[size]; 
	    int p[] = new int[size]; 
	 
	    int i, j;
	    
	    for (i = 1; i <= n; i++)
	    {
	        extras[i][i] = M - l[i-1];
	        for (j = i+1; j <= n; j++)
	            extras[i][j] = extras[i][j-1] - l[j-1] - 1;
	    }
	 
	    for (i = 1; i <= n; i++)
	    {
	        for (j = i; j <= n; j++)
	        {
	            if (extras[i][j] < 0)
	                lc[i][j] = INF;
	            else if (j == n && extras[i][j] >= 0)
	                lc[i][j] = 0;
	            else
	                lc[i][j] = extras[i][j]*extras[i][j];
	        }
	    }
	 
	    c[0] = 0;
	    for (j = 1; j <= n; j++)
	    {
	        c[j] = INF;
	        for (i = 1; i <= j; i++)
	        {
	            if (c[i-1] != INF && lc[i][j] != INF && (c[i-1] + lc[i][j] < c[j]))
	            {
	                c[j] = c[i-1] + lc[i][j];
	                p[j] = i;
	            }
	        }
	    }
	 
	    printSolution(p, n);
	}
	
	private <T> void println(T output)
	{
		System.out.println(output.toString());
	}
	
	int printSolution (int p[], int n)
	{
	    int k;
	    if (p[n] == 1)
	        k = 1;
	    else
	        k = printSolution (p, p[n]-1) + 1;
	    println("Line number " + k +": From word no." + p[n] + " to " + n);
	    return k;
	}

	@Override
	public void shelveBooks(Bookcase bookcase, List<Book> books) 
	{
		int bookShelfMark = 0;
		int numberOFSelfs = bookcase.getNumberOfShelves();
		
		int widthOfShelf = bookcase.getShelfWidth();
		int remaingSpace = widthOfShelf;
		
		for(Book book : books)
		{
			if(remaingSpace < book.getWidth())
			{
				bookShelfMark++;
				if(bookShelfMark > numberOFSelfs)
				{
					break;
				}
				else
				{
					remaingSpace = widthOfShelf;
					bookcase.addBook(bookShelfMark, book);
					remaingSpace -= book.getWidth();
				}
			}
			else
			{
				bookcase.addBook(bookShelfMark, book);
				remaingSpace -= book.getWidth();
			}
		}
	}
	
}
