package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import Libary.Libraian;
import sorting.Merge;
import sorting.Quick;
import sorting.Selection;
import edu.neumont.csc250.lab4.Book;
import edu.neumont.csc250.lab4.Bookcase;
import edu.neumont.csc250.lab4.Shelver;

public class sorting {

	List<Integer> collection = new ArrayList<Integer>();
	
	Selection selectSort = new Selection();
	Quick quicksort = new Quick();
	Merge mergeSort = new Merge();
	
	private <T> void println(T output)
	{
		System.out.println(output.toString());
	}
	
	private <T> void printList(List<T> set)
	{
		int size = set.size();
		for(int step = 0; step < size; step++)
		{
			System.out.println(set.get(step).toString());
		}
	}
	
	private void setUp()
	{
		collection.add(5);
		collection.add(2);
		collection.add(9);
		collection.add(7);
	}
	
	private void assertingTests()
	{
		assertTrue(collection.get(0) == 2);
		assertTrue(collection.get(1) == 5);
		assertTrue(collection.get(2) == 7);
		assertTrue(collection.get(3) == 9);
	}
	
	//@Test
	public void SelectionSort() {
		setUp();
		selectSort.sort(collection);
		printList(collection);
		
		assertingTests();
	}
	
	//@Test
	public void QuickSort() 
	{
		//setUp();
		collection.add(5);
		collection.add(2);
		collection.add(21);
		collection.add(3);
		collection.add(4);
		collection.add(9);
		collection.add(21);
		collection.add(3);
		collection.add(4);
		collection.add(7);
		collection.add(21);
		collection.add(3);
		collection.add(4);
		
		quicksort.sort(collection);
		printList(collection);
		
		//assertingTests();
	}
	
	@Test
	public void mergeSortTest()
	{
		setUp();
		collection.add(4);
		collection.add(3);
		mergeSort.sort(collection);
		printList(collection);
	}

	//@Test
	public void libraianTest()
	{
		Random ran = new Random();
		int numShelfs = 5;
		int shelfWidth = 5;
		
		Bookcase bookCase = new Bookcase(5,5);
		List<Book> books = new ArrayList<Book>();
		int numBooks = 5;
		
		for(int step = 0; step < numBooks; step++)
		{
			books.add(new Book("Tron", ran.nextInt(5),ran.nextInt(6)));
		}
		
		selectSort.sort(collection);
		
		Libraian conan = new Libraian();
		conan.shelveBooks(bookCase, books);
		println(bookCase.toString());
	}
	
}
