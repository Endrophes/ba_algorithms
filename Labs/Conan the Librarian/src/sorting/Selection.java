package sorting;

import java.util.List;

import edu.neumont.csc250.lab4.Sorter;

public class Selection<T extends Comparable<T>> implements Sorter<T> {
	
	@Override
	public void sort(List<T> unSorted) {
		int indexMark = 0;
		int length = unSorted.size();

		while(indexMark < length)
		{
			int minindex = indexMark;
			T minvalue = (T) unSorted.get(indexMark);
			
			for(int step = indexMark + 1; step < length; step++)
			{
				T currentValue =  (T) unSorted.get(step);
				int compareValue = currentValue.compareTo(minvalue);
				//System.out.println("current Value: " + currentValue  + " Compared to: " + minvalue + " results: " + compareValue);
				if(compareValue < 0)
				{
					minindex = step;
					minvalue = currentValue;
				}
			}
			
			//Swap LOL
			T temp = (T) unSorted.get(indexMark);
			unSorted.set(indexMark, minvalue);
			unSorted.set(minindex, temp);
			
			indexMark++;
		}
	}
	
}
