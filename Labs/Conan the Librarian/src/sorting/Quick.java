package sorting;

import java.util.List;

import edu.neumont.csc250.lab4.Sorter;

public class Quick<T extends Comparable<T>> implements Sorter<T> {

	int size;
	
	private int findMid(int size)
	{
		//Simple Version
		int result = size / 2;
		
		return result;
	}
	
	private int findPivot(int min, int max)
	{
		return (min + max) / 2; 
	}
	
	private void swap(List<T> Array, int indexOne, int indexTwo)
	{
		T tempA = Array.get(indexOne);
		T tempB = Array.get(indexTwo);
		
		Array.set(indexOne, tempB);
		Array.set(indexTwo, tempA);
	}
	
	private <T> void printList(List<T> set)
	{
		int size = set.size();
		for(int step = 0; step < size; step++)
		{
			System.out.print(set.get(step).toString() + ", ");
		}
	}
	
	private int partition(List<T> Array, int min, int max)
	{
		int i = min;
		int j = max;
		int pivotPoint = findPivot(min, max);
		T pivotValue = Array.get(pivotPoint);
		
		while (i <= j)
		{
			while (Array.get(i).compareTo(pivotValue) < 0)
			{
				i++;
			}
			
			while (Array.get(j).compareTo(pivotValue) > 0)
			{
				j--;
			}
			
			if(i <= j)
			{
				swap(Array, i, j);
				i++;
				j--;
			}
		}
		
		return i;
	}
	
	private void quickSort(List<T> Array, int min, int max)
	{
		int index = partition(Array, min, max);
		if(min < (index - 1))
		{
			quickSort(Array, min, index - 1);
		}
		if(index < max)
		{
			quickSort(Array, index, max);
		}
	}
	
	@Override
	public void sort(List<T> toBeSorted) 
	{
		size = toBeSorted.size() - 1;
		quickSort(toBeSorted, 0, (size));
	}

}