package sorting;

import java.util.List;
import java.io.*;
import java.util.*;
import java.lang.*;
import edu.neumont.csc250.lab4.Sorter;

public class Merge<T extends Comparable<T>> implements Sorter<T> {

	private int findMid(int size)
	{
		//Simple Version
		int result = size / 2;
		
		return result;
	}
	
	private void swap(List<T> Array, int indexOne, int indexTwo)
	{
		T tempA = Array.get(indexOne);
		T tempB = Array.get(indexTwo);
		
		Array.set(indexOne, tempB);
		Array.set(indexTwo, tempA);
	}
	
	private int findPivot(int min, int max)
	{
		return (min + max) / 2; 
	}
	
	private <T> void println(T output)
	{
		System.out.println(output.toString());
	}
	
	private <T> void print(T output)
	{
		System.out.print(output.toString());
	}
	
	private <T> void printList(List<T> set)
	{
		int size = set.size();
		for(int step = 0; step < size; step++)
		{
			System.out.print(set.get(step).toString() + ", ");
		}
	}
	
	private <T> void printList(String messege, List<T> set)
	{
		int size = set.size();
		System.out.print(messege);
		for(int step = 0; step < size; step++)
		{
			System.out.print(set.get(step).toString() + ", ");
		}
		System.out.println("");
	}
	
	private void sectionMerge(List<T> Array, int left, int leftMax, int right, int rightMax)
	{
		println("left: " + left + " left Max: " + leftMax + " right: " + right + " right Max: " + rightMax);
		printList("Before: ", Array);
		
		while(left <= leftMax && right <= rightMax )
		{
			
			T valA = Array.get(left);
			T valB = Array.get(right);
			
			int compareVal = valA.compareTo(valB);
			//if(left < leftMax && right < rightMax)
			{
				if(compareVal > 0)
				{
					//Array.set(left, valA);
					swap(Array, left, right);
					left++;
				}
				else if(compareVal < 0)
				{
					//Array.set(right, valA);
					swap(Array, left, right);
					right++;
				}
				else
				{
					//Array.set(left, valA);
					swap(Array, left, right);
					left++;
				}
			}


			printList("After: ", Array);
			println("valA: " + valA + " valB: " + valB + " compareVal: " + compareVal);
		}
	}
	
	private void sectionAllocation(List<T> Array)
	{
		int size = Array.size();
		int sectionSize = 1;
		
		println("sectionSize: " + sectionSize);
		while(sectionSize > (0))
		{
			int left = 0;
			int leftMax = 0;
			int right = 0;
			int rightMax = 0;
			
			for(int step = 0; step < (size / 2) && sectionSize < (size / 2); step += (sectionSize))
			{

				left = step;
				leftMax = (step + sectionSize - 1);
				right = (step + sectionSize);
				rightMax = (step + (2 * sectionSize) - 1);
				
				sectionMerge(Array, left, leftMax, right, rightMax);
				
				if(left == leftMax && right == rightMax)
				{
					//break;
				}
				
			}
			sectionSize += 1;
		}
		
	}
	
	public void DoMerge(List<T> numbers, int left, int mid, int right)
	{
		T[] temp = (T[]) new Object[25];
       int i, left_end, num_elements, tmp_pos;
   
       left_end = (mid - 1);
       tmp_pos = left;
       num_elements = (right - left + 1);
   
       while ((left <= left_end) && (mid <= right))
       {
           if (numbers.get(left).compareTo(numbers.get(right)) <= 0)
               temp[tmp_pos++] = numbers.get(left++);
           else
               temp[tmp_pos++] = numbers.get(mid++);
       }
   
       while (left <= left_end)
           temp[tmp_pos++] = numbers.get(left++);

       while (mid <= right)
           temp[tmp_pos++] = numbers.get(mid++);

       for (i = 0; i < num_elements; i++)
       {
    	   numbers.set(right, (T) temp[right]);
           right--;
       }
   }

	public void MergeSort_Recursive(List<T> numbers, int left, int right)
   {
     int mid;
   
     if (right > left)
     {
       mid = (right + left) / 2;
       MergeSort_Recursive(numbers, left, mid);
       MergeSort_Recursive(numbers, (mid + 1), right);
   
       DoMerge(numbers, left, (mid+1), right);
     }
   }
	
	@Override
	public void sort(List<T> toBeSorted) {
		
		MergeSort_Recursive(toBeSorted, 0, toBeSorted.size() - 1);
		//sectionAllocation(toBeSorted);
	}

}
