package game;

import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

import graph.Graph;


public class DotsAndBoxes
{
	
	Graph g;
	
	int numRows;
	int numColums;
	
	int xZero;
	int yZero;
	int fullSizeZero;
	
	int coreStartIndex = 0;
	int coreWidth  = 0;
	int coreHeight = 0;
	int coreSize   = 0;
	
	int edgesToCut = 0;
	
	Map<Integer, Integer> players = new HashMap();
	
	private class Coordnets
	{
		int x = 0;
		int y = 0;
		
		public Coordnets(){}
		public Coordnets(int X, int Y)
		{
			x = X;
			y = Y;
		}
		
		public void grabAndSetValues(Coordnets set)
		{
			x = set.x;
			y = set.y;
		}
	}
	//
	private boolean checkIfPlaying(int ID)
	{
		boolean isAPlayer = players.containsKey(ID);
		return isAPlayer;
	}
	
	private int coordnetsToIndex(int x, int y)
	{
		int result = (y  * yZero) + x;
		return result;
	}
	
	private int coordnetsToIndex(Coordnets a)
	{
		
		int result = (a.y  * yZero) + a.x;
		return result;
	}
	
	private Coordnets indextoCordnets(int index)
	{
		int x = index % yZero;
		int y = index / yZero;
		Coordnets newSet = new Coordnets(x, y);
		return newSet;
	}
	
	private void setUpEdges()
	{		
		int colStep = 0;
		int nodeHit = 0;
		for(int step = coreStartIndex; step < fullSizeZero && nodeHit < coreSize; step++)
		{
			int up = (step - yZero);
			int down = (step + yZero);
			int left = step - 1;
			int right = step + 1;
			
			g.addMirrorEdge(step, up, 1);
			g.addMirrorEdge(step, down, 1);
			g.addMirrorEdge(step, left, 1);
			g.addMirrorEdge(step, right, 1);
			
			//g.addEdge(step, right, 1);
			
			edgesToCut += 4;
			
			colStep++;
			nodeHit++;
			
			if(colStep >= coreWidth)
			{
				colStep = 0;
				step += 2;
			}
			
		}
	}
	
	private int findDirection(Coordnets a, Coordnets b)
	{
		int xValue = b.x - a.x;
		int yValue = b.y - a.y;
		
		int direction = 0; //Up: 1 Down: 2 Left: 3 right: 4
		
		if(xValue == -1 && yValue == 0)
		{
			direction = 3;
		}
		else if(xValue == 0 && yValue == -1)
		{
			direction = 1;
		}
		else if(xValue == 1 && yValue == 0)
		{
			direction = 4;
		}
		else if(xValue == 0 && yValue == 1)
		{
			direction = 2;
		}
		
		return direction;
	}
	
	private int getNumberOfNabors(int index)
	{
		int results = 0;
		
		int naborIndex = 0;
		
		while(naborIndex != fullSizeZero)
		{
			naborIndex = g.next(index, naborIndex);
			if(naborIndex != fullSizeZero)
			{
				results++;
			}
		}
		
		return results;
	}
	
	private boolean checkIfInCore(Coordnets a)
	{
		
		boolean testX = false;
		boolean testY = false;
		if(a.x > 0 && a.x < numColums)
		{
			testX = true;
		}
		if(a.y > 0 && a.y < numRows)
		{
			testY = true;
		}
		
		boolean result = false;
		
		if(testX && testY)
		{
			result = true;
		}
		
		return result;
	}
		
	private boolean depthSearchCycle(int currentIndex, int lastVisited, HashSet<Integer> visited)
	{
		boolean isCycle = false;
		boolean hasBeenVisited = visited.contains(currentIndex);
		
		if(hasBeenVisited)
		{
			isCycle = true;
		}
		else
		{
			visited.add(currentIndex);
			int numNabors = g.degree(currentIndex);
			
			if(numNabors == 2)
			{
				int first = g.first(currentIndex);
				int next = g.next(currentIndex, first);
				
				if(first != lastVisited)
				{
					isCycle = depthSearchCycle(first, currentIndex, visited);
				}
				
				if(next != lastVisited)
				{
					isCycle = depthSearchCycle(next, currentIndex, visited);
				}
			}
		}
		
		return isCycle;
	}
	
	private boolean depthChain(int currentIndex, int lastedVisited, HashSet<Integer> visited)
	{
		boolean result = false;
		boolean hasBeenVisited = visited.contains(currentIndex);
		
		if(!hasBeenVisited)
		{
			visited.add(currentIndex);
			int numNabors = g.degree(currentIndex);
			if(numNabors == 2)
			{
				int first = g.first(currentIndex);
				int next = g.next(currentIndex, first);
				
				if(first != lastedVisited && first != fullSizeZero)
				{
					result = depthChain(first, currentIndex, visited);
				}
				else if(next != lastedVisited && next != fullSizeZero)
				{
					result = depthChain(next, currentIndex, visited);
				}
			}
			else //if(numNabors == 3 || numNabors == 1)
			{
				if(numNabors == 1)
				{
					Coordnets check = indextoCordnets(currentIndex);
					boolean isInCore = checkIfInCore(check);
					if(!isInCore)
					{
						result = true;
					}
				}
				else
				{
					result = true;
				}
			}
		}
		else
		{
			result = true;
		}
		
		return result;
	}
	
	
	public DotsAndBoxes(int rows, int columns)
	{
		numRows = rows;
		numColums = columns;
		
		xZero = (rows + 1);
		yZero = (columns + 1);
		fullSizeZero = xZero * yZero;
		g = new Graph(fullSizeZero);
		
		coreWidth = columns - 1;
		coreHeight = rows - 1;
		coreSize = coreWidth * coreHeight;
		coreStartIndex = yZero + 1;
		
		setUpEdges();
	}

	// draws a line from (x1, y1) to (x2, y2) (0,0) is in the upper-left corner, returning how many points were earned, if any
	public int drawLine(int player, int x1, int y1, int x2, int y2)
	{
		Coordnets locationOne = new Coordnets(x1, y1);
		Coordnets locationTwo = new Coordnets(x2, y2);
		
		int direction = findDirection(locationOne, locationTwo);
		
		Coordnets targetOne = new Coordnets();
		Coordnets targetTwo = new Coordnets();
		
		if(direction == 1)
		{
			targetOne.grabAndSetValues(locationOne);
			targetTwo.grabAndSetValues(locationOne);
			targetTwo.x += 1;
		}
		else if(direction == 2)
		{
			targetOne.grabAndSetValues(locationTwo);
			targetTwo.grabAndSetValues(locationTwo);
			targetTwo.x += 1;
		}
		else if(direction == 3)
		{
			targetOne.grabAndSetValues(locationOne);
			targetTwo.grabAndSetValues(locationOne);
			targetTwo.y += 1;
		}
		else if(direction == 4)
		{
			targetOne.grabAndSetValues(locationTwo);
			targetTwo.grabAndSetValues(locationTwo);
			targetTwo.y += 1;
		}
		
		int indexOne = coordnetsToIndex(targetOne);
		int indexTwo = coordnetsToIndex(targetTwo);
		
		g.removeMirrorEdge(indexOne, indexTwo);
		edgesToCut -= 2;
		
		//Count new blocks
		int results = 0;
		
		boolean setOne = checkIfInCore(targetOne);
		boolean setTwo = checkIfInCore(targetTwo);
		
		int tarGetOneNabors = g.first(indexOne);
		int tarGetTwoNabors = g.first(indexTwo);
		
		if(tarGetOneNabors == fullSizeZero && setOne)
		{
			results += 1;
		}
		
		if(tarGetTwoNabors == fullSizeZero && setTwo)
		{
			results += 1;
		}
		
		//Player track
		boolean isPlaying = checkIfPlaying(player);
		if(isPlaying)
		{
			int currentScore = players.get(player);
			int update = currentScore + results;
			players.put(player, update);
		}
		else
		{
			players.put(player, results);
		}
		
		return results;
	}
	
	// returns the score for a player
	public int score(int player)
	{
		boolean isAPlayer = checkIfPlaying(player);
		
		int result = 0;
		
		if(!isAPlayer)
		{
			players.put(player, 0);
		}
		else
		{
			result = players.get(player);
		}
		
		return result;
	}
	
	// returns whether or not there are any lines to be drawn
	public boolean areMovesLeft()
	{
		boolean haveMoves = false;
		
		//If any moves
		//if(edgesToCut <= 0)
		//{
		//	haveMoves = false;
		//}
		
		for(int step = 0; step < fullSizeZero; step++)
		{	
			int numNabors = g.degree(step);
			if(numNabors != fullSizeZero)
			{
				int first = g.first(step);
				haveMoves = true;
				break;
			}
		}
		
		return haveMoves;
	}
	
	// returns the number of double-crosses on the board
	public int countDoubleCrosses()
	{		
		int results = 0;
		
		HashSet<Integer> visited = new HashSet<Integer>();
		
		int colStep = 0;
		int nodeHit = 0;
		for(int step = coreStartIndex; step < fullSizeZero && nodeHit < coreSize; step++)
		{	
			
			///Logic gose here////
			
			int numNabors = g.degree(step);
			
			if(numNabors == 1)
			{
				
				//get that nabor
				int nabor = g.first(step);
				
				//Have visited
				boolean hasVisited = visited.contains(nabor);
				
				if(!hasVisited)
				{
					
					numNabors = g.degree(nabor);
					int naborLinkedTo = g.first(nabor);
					if(numNabors == 1 && naborLinkedTo == step)
					{
						results += 1;
						visited.add(step);
						visited.add(nabor);
					}
				}
				
			}
			
			//////////////////////
			
			colStep++;
			nodeHit++;
			if(colStep > coreWidth)
			{
				colStep = 0;
				step += 2;
			}
			
		}
		
		return results;
	}

	// returns the number of cycles on the board
	public int countCycles()
	{
		int results = 0;
		HashSet<Integer> visited = new HashSet<Integer>();
		
		int colStep = 0;
		int nodeHit = 0;
		for(int step = coreStartIndex; step < fullSizeZero && nodeHit < coreSize; step++)
		{	
			
			///Logic gose here////
			int numNabors = g.degree(step);
			
			if(numNabors == 2)
			{
				boolean hasBeenVisited = visited.contains(step);
				if(!hasBeenVisited)
				{
					boolean isCycle = depthSearchCycle(step, step, visited);
					if(isCycle)
					{
						results++;
					}
				}
			}
			
			//////////////////////
			
			colStep++;
			nodeHit++;
			if(colStep > coreWidth)
			{
				colStep = 0;
				step += 2;
			}
			
		}
		
		return results;
	}
	
	// returns the number of open chains on the board
	public int countOpenChains()
	{
		int result = 0;
		HashSet<Integer> visited = new HashSet<Integer>();

		
		for(int step = 0; step < fullSizeZero; step++)
		{	
			
			///Logic gose here////
			//boolean hasBeenVisited = visited.contains(step);
			
			//if(!hasBeenVisited)
			//{
				int numNabors = g.degree(step);
				
				if(numNabors != 2)
				{
					int naborOne = g.first(step);
					int naborTwo = g.next(step, naborOne);
					int naborThree = g.next(step, naborTwo);
					
					int oneNabors = 0;
					int twoNabors = 0;
					int threeNabors = 0;
					
					if(naborOne != fullSizeZero && !visited.contains(naborOne))
					{
						oneNabors = g.degree(naborOne);
					}
					
					if(naborTwo != fullSizeZero && !visited.contains(naborTwo))
					{
						twoNabors = g.degree(naborTwo);
					}
					
					if(naborThree != fullSizeZero && !visited.contains(naborThree))
					{
						threeNabors = g.degree(naborThree);
					}
					
					//Find of one of them has only two nabors
					boolean isChain = false;
					if(oneNabors == 2)
					{
						isChain = depthChain(naborOne, step, visited);
					}
					else if(twoNabors == 2)
					{
						isChain = depthChain(naborTwo, step, visited);
					}
					else if(threeNabors == 2)
					{
						isChain = depthChain(naborThree, step, visited);
					}
					
					if(isChain && visited.size() > 4)
					{
						result++;
					}///FIX!!!!!!!!!
				}
		//}
			//////////////////////
			
			
			
		}
		
		return result;
	}
	
}