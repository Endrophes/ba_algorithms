package tests;

import static org.junit.Assert.*;
import junit.framework.Assert;
import game.DotsAndBoxes;

import org.junit.Test;

public class CummingsDBTest
{

	DotsAndBoxes board;
	
	
	//Helper functions
	private void setUp(int x, int y)
	{
		board = new DotsAndBoxes(x, y);
		assertTrue(board != null);
	}
	
	private void println(String output)
	{
		System.out.println(output);
	}
	
	private void print(String output)
	{
		System.out.print(output);
	}
	
	private void drawPrint(int a, int b, int c, int d, int e)
	{
		int currentScore = board.drawLine(a, b, c, d, e);
		//println("Players " + a + " player gained: " + currentScore);
		//Add Assert?
	}
	
	private void checkPlayerScore(int player, int shouldBe)
	{
		int scoreReturned = board.score(player);
		println("Player: " + player + " score should be: " 
		+ shouldBe + " got: " + scoreReturned);
		assertEquals(shouldBe, scoreReturned);
	}
	
	private void checkDoubleNumber(int shouldBe)
	{
		int doubleCrossCount = board.countDoubleCrosses();
		println("double cross count should be: " + shouldBe + " got: " + doubleCrossCount);
		assertEquals(shouldBe, doubleCrossCount);
	}
	
	private void checkCycleCount(int shouldBe)
	{
		int cycleCount = board.countCycles();
		println("Cycle count should be: " + shouldBe + " got: " + cycleCount);
		assertEquals(shouldBe, cycleCount);
	}
	
	private void checkChainCount(int shouldBe)
	{
		int chainCount = board.countOpenChains();
		println("Chain count should be: " + shouldBe + " got: " + chainCount);
		assertEquals(shouldBe, chainCount);
	}
	
	private void checkMovesLeft(boolean shouldbe)
	{
		boolean results = board.areMovesLeft();
		println("Check moves should be: " + shouldbe + " got: " + results);
		assertTrue(results == shouldbe);
	}
	
	private void cutAllTies()
	{
		int player = 1;
		//		  x1 y1 x2 y2
		drawPrint(player, 0, 0, 0, 1);
		drawPrint(player, 0, 1, 0, 2);
		drawPrint(player, 0, 2, 0, 3);
		drawPrint(player, 0, 3, 0, 4);
		
		drawPrint(player, 0, 4, 1, 4);
		drawPrint(player, 1, 4, 2, 4);
		drawPrint(player, 2, 4, 3, 4);
		drawPrint(player, 3, 4, 4, 4);
		
		drawPrint(player, 4, 3, 4, 4);
		drawPrint(player, 4, 2, 4, 3);
		drawPrint(player, 4, 1, 4, 2);
		drawPrint(player, 4, 0, 4, 1);
		
		drawPrint(player, 3, 0, 4, 0);
		drawPrint(player, 2, 0, 3, 0);
		drawPrint(player, 1, 0, 2, 0);
		drawPrint(player, 0, 0, 1, 0);
		
		drawPrint(player, 1, 1, 1, 2);
		drawPrint(player, 1, 2, 1, 3);
		
		drawPrint(player, 1, 3, 2, 3);
		drawPrint(player, 2, 3, 3, 3);
		
		drawPrint(player, 3, 2, 3, 3);
		drawPrint(player, 3, 1, 3, 2);
		
		drawPrint(player, 2, 1, 3, 1);
		drawPrint(player, 1, 1, 2, 1);
//		.-.-.-.-.
//		|       |
//		. .-.-. .
//		| |   | |
//		. . . . .
//		| |   | |
//		. .-.-. .
//		|       |
//		.-.-.-.-.
		
		//New
		drawPrint(player, 1, 1, 1, 0);
		drawPrint(player, 2, 1, 2, 0);
		drawPrint(player, 3, 1, 3, 0);
//		.-.-.-.-.
//		| | | | |
//		. .-.-. .
//		| |   | |
//		. . . . .
//		| |   | |
//		. .-.-. .
//		|       |
//		.-.-.-.-.
		
		
		drawPrint(player, 0, 1, 1, 1);
		drawPrint(player, 0, 2, 1, 2);
		drawPrint(player, 0, 3, 1, 3);
//		.-.-.-.-.
//		| | | | |
//		.-.-.-. .
//		| |   | |
//		.-. . . .
//		| |   | |
//		.-.-.-. .
//		|       |
//		.-.-.-.-.
		
		drawPrint(player, 3, 1, 4, 1);
		drawPrint(player, 3, 2, 4, 2);
		drawPrint(player, 3, 3, 4, 3);
//		.-.-.-.-.
//		| | | | |
//		.-.-.-.-.
//		| |   | |
//		.-. . .-.
//		| |   | |
//		.-.-.-.-.
//		|       |
//		.-.-.-.-.
		
		drawPrint(player, 1, 3, 1, 4);
		drawPrint(player, 2, 3, 2, 4);
		drawPrint(player, 3, 3, 3, 4);
//		.-.-.-.-.
//		| | | | |
//		.-.-.-.-.
//		| |   | |
//		.-. . .-.
//		| |   | |
//		.-.-.-.-.
//		| | | | |
//		.-.-.-.-.
		
		drawPrint(player, 3, 1, 3, 2);
		drawPrint(player, 3, 2, 3, 3);
//		.-.-.-.-.
//		| | | | |
//		.-.-.-.-.
//		| | | | |
//		.-. . .-.
//		| | | | |
//		.-.-.-.-.
//		| | | | |
//		.-.-.-.-.
		
		drawPrint(player, 1, 3, 2, 3);
		drawPrint(player, 2, 3, 3, 3);
//		.-.-.-.-.
//		| | | | |
//		.-.-.-.-.
//		| | | | |
//		.-.-.-.-.
//		| | | | |
//		.-.-.-.-.
//		| | | | |
//		.-.-.-.-.
		
		drawPrint(player, 1, 2, 2, 2);
		drawPrint(player, 2, 1, 2, 2);
		drawPrint(player, 2, 2, 3, 2);
		drawPrint(player, 2, 1, 2, 2);
		drawPrint(player, 0, 3, 1, 3);
		drawPrint(player, 2, 2, 2, 3);
	}
	
	//All the Tests
	@Test
	public void testOwner() {
		setUp(5, 5);
		
		int player = 1;
		
		// 				  x1 y1 x2 y2
		drawPrint(player, 1, 0, 2, 0); // right
		drawPrint(player, 2, 0, 3, 0); // right
		drawPrint(player, 1, 1, 2, 1); // right 
		drawPrint(player, 2, 1, 3, 1); // right
		
		drawPrint(player, 1, 0, 1, 1); // down
		drawPrint(player, 3, 0, 3, 1); // down
		drawPrint(player, 0, 2, 0, 3); // down
		drawPrint(player, 0, 3, 0, 4); // down
		
		drawPrint(player, 1, 2, 1, 3); // down
		drawPrint(player, 1, 3, 1, 4); // down
		drawPrint(player, 0, 2, 1, 2); // right
		drawPrint(player, 0, 4, 1, 4); // right
		
		drawPrint(player, 2, 0, 2, 1); // draw line in between a double-cross
		
		int target = 2;
		checkPlayerScore(player,target);
		//Hi
	}
	
	@Test
	public void testDoubleCrosses() {
		setUp(5, 5);
		
		int player = 1;
		
		// 				  x1 y1 x2 y2
		drawPrint(player, 1, 0, 2, 0); // right
		drawPrint(player, 2, 0, 3, 0); // right
		drawPrint(player, 1, 1, 2, 1); // right 
		drawPrint(player, 2, 1, 3, 1); // right
		
		drawPrint(player, 1, 0, 1, 1); // down
		drawPrint(player, 3, 0, 3, 1); // down
		drawPrint(player, 0, 2, 0, 3); // down
		drawPrint(player, 0, 3, 0, 4); // down
		
		drawPrint(player, 1, 2, 1, 3); // down
		drawPrint(player, 1, 3, 1, 4); // down
		drawPrint(player, 0, 2, 1, 2); // right
		drawPrint(player, 0, 4, 1, 4); // right
		
		checkDoubleNumber(2);
	}
	
	@Test
	public void testGameOver()
	{
		setUp(5, 5);
		checkMovesLeft(true);
		cutAllTies();
		checkMovesLeft(false);
	}
	
	@Test
	public void testCycle() {
		setUp(5, 5);
		
//		.-.-.-.-.
//		|       |
//		. .-.-. .
//		| |   | |
//		. . . . .
//		| |   | |
//		. .-.-. .
//		|       |
//		.-.-.-.-.
		
		int player = 1;
		
				//		  x1 y1 x2 y2
		drawPrint(player, 0, 0, 0, 1);
		drawPrint(player, 0, 1, 0, 2);
		drawPrint(player, 0, 2, 0, 3);
		drawPrint(player, 0, 3, 0, 4);
		
		drawPrint(player, 0, 4, 1, 4);
		drawPrint(player, 1, 4, 2, 4);
		drawPrint(player, 2, 4, 3, 4);
		drawPrint(player, 3, 4, 4, 4);
		
		drawPrint(player, 4, 3, 4, 4);
		drawPrint(player, 4, 2, 4, 3);
		drawPrint(player, 4, 1, 4, 2);
		drawPrint(player, 4, 0, 4, 1);
		
		drawPrint(player, 3, 0, 4, 0);
		drawPrint(player, 2, 0, 3, 0);
		drawPrint(player, 1, 0, 2, 0);
		drawPrint(player, 0, 0, 1, 0);
		
		drawPrint(player, 1, 1, 1, 2);
		drawPrint(player, 1, 2, 1, 3);
		
		drawPrint(player, 1, 3, 2, 3);
		drawPrint(player, 2, 3, 3, 3);
		
		drawPrint(player, 3, 2, 3, 3);
		drawPrint(player, 3, 1, 3, 2);
		
		drawPrint(player, 2, 1, 3, 1);
		drawPrint(player, 1, 1, 2, 1);
		
		checkCycleCount(2);
	}

	@Test
	public void testChains() {
		
//		. . .-.-.
//		| |     |
//		. .-.-. .
//		| |   | |
//		. . . . .
//		| |   | |
//		. .-.-. .
//		|       |
//		.-.-.-.-.
		
		setUp(5, 5);
		
		int player = 1;
				//		  x1 y1 x2 y2		
		drawPrint(player, 0, 0, 0, 1);
		drawPrint(player, 0, 1, 0, 2);
		drawPrint(player, 0, 2, 0, 3);
		drawPrint(player, 0, 3, 0, 4);
		
		drawPrint(player, 0, 4, 1, 4);
		drawPrint(player, 1, 4, 2, 4);
		drawPrint(player, 2, 4, 3, 4);
		drawPrint(player, 3, 4, 4, 4);
		
		drawPrint(player, 4, 3, 4, 4);
		drawPrint(player, 4, 2, 4, 3);
		drawPrint(player, 4, 1, 4, 2);
		drawPrint(player, 4, 0, 4, 1);
		
		drawPrint(player, 3, 0, 4, 0);
		drawPrint(player, 2, 0, 3, 0);
		drawPrint(player, 1, 0, 1, 1);
		
		drawPrint(player, 1, 1, 1, 2);
		drawPrint(player, 1, 2, 1, 3);
		
		drawPrint(player, 1, 3, 2, 3);
		drawPrint(player, 2, 3, 3, 3);
		
		drawPrint(player, 3, 2, 3, 3);
		drawPrint(player, 3, 1, 3, 2);
		
		drawPrint(player, 2, 1, 3, 1);
		drawPrint(player, 1, 1, 2, 1);
		
		checkChainCount(1);	
	}
	
	@Test
	public void testTwoChains() {
		
//		. . .-.-.
//		| |     |
//		. .-.-. .
//		| |   | |
//		. . . . .
//		| |   | |
//		. .-.-. .
//		|   |   |
//		.-. . .-.
		
		setUp(5, 5);
		
		drawPrint(1, 0, 0, 0, 1);
		drawPrint(1, 0, 1, 0, 2);
		drawPrint(1, 0, 2, 0, 3);
		drawPrint(1, 0, 3, 0, 4);
		
		drawPrint(1, 0, 4, 1, 4);
		drawPrint(1, 2, 3, 2, 4);
		drawPrint(1, 3, 4, 4, 4);
		
		drawPrint(1, 4, 3, 4, 4);
		drawPrint(1, 4, 2, 4, 3);
		drawPrint(1, 4, 1, 4, 2);
		drawPrint(1, 4, 0, 4, 1);
		
		drawPrint(1, 3, 0, 4, 0);
		drawPrint(1, 2, 0, 3, 0);
		drawPrint(1, 1, 0, 1, 1);
		
		drawPrint(1, 1, 1, 1, 2);
		drawPrint(1, 1, 2, 1, 3);
		
		drawPrint(1, 1, 3, 2, 3);
		drawPrint(1, 2, 3, 3, 3);
		
		drawPrint(1, 3, 2, 3, 3);
		drawPrint(1, 3, 1, 3, 2);
		
		drawPrint(1, 2, 1, 3, 1);
		drawPrint(1, 1, 1, 2, 1);
		
		checkChainCount(2);		
	}

	@Test
	public void testInternalChains() {
		
//		.-.-.-.-.
//		|       |
//		. .-.-. .
//		| |     |
//		. . .-. .
//		| |     |
//		. .-.-. .
//		|       |
//		.-.-.-.-.
		
		setUp(5, 5);
		
		int player = 1;
				//		  x1 y1 x2 y2	
		drawPrint(player, 0, 0, 0, 1);
		drawPrint(player, 0, 1, 0, 2);
		drawPrint(player, 0, 2, 0, 3);
		drawPrint(player, 0, 3, 0, 4);
		
		drawPrint(player, 0, 4, 1, 4);
		drawPrint(player, 1, 4, 2, 4);
		drawPrint(player, 2, 4, 3, 4);
		drawPrint(player, 3, 4, 4, 4);
		
		drawPrint(player, 4, 3, 4, 4);
		drawPrint(player, 4, 2, 4, 3);
		drawPrint(player, 4, 1, 4, 2);
		drawPrint(player, 4, 0, 4, 1);
		
		drawPrint(player, 3, 0, 4, 0);
		drawPrint(player, 2, 0, 3, 0);
		drawPrint(player, 1, 0, 2, 0);
		drawPrint(player, 0, 0, 1, 0);
		
		drawPrint(player, 1, 1, 1, 2);
		drawPrint(player, 1, 2, 1, 3);
		
		drawPrint(player, 1, 3, 2, 3);
		drawPrint(player, 2, 3, 3, 3);
		
		drawPrint(player, 2, 2, 3, 2);
		
		drawPrint(player, 2, 1, 3, 1);
		drawPrint(player, 1, 1, 2, 1);
		
		checkChainCount(2);	
		//checkChainCount(1);	
	}

	@Test
	public void testInternalChain() {
		
//		. . . . .
//		
//		. .-.-. .
//		  |     
//		. . ._. .
//		  |     
//		. .-.-. .
//		       
//		. . . . .
		
		setUp(5, 5);
		
		int player = 1;
		
				//		  x1 y1 x2 y2	
		drawPrint(player, 1, 1, 1, 2);
		drawPrint(player, 1, 2, 1, 3);
		
		drawPrint(player, 1, 3, 2, 3);
		drawPrint(player, 2, 3, 3, 3);
		
		drawPrint(player, 2, 2, 3, 2);
		//drawPrint(player, 3, 2, 3, 3);
		
		drawPrint(player, 2, 1, 3, 1);
		drawPrint(player, 1, 1, 2, 1);
		
		checkChainCount(1);		
	}
	
	@Test
	public void testNotAChain() {
		
//		. . . . .
//		
//		. .-.-. .
//		       
//		. ._._. .
//		      
//		. . . . .
//		       
//		. . . . .
		
		setUp(5, 5);

		int player = 1;
		
		drawPrint(player, 1, 2, 2, 2);
		drawPrint(player, 2, 2, 3, 2);
		
		drawPrint(player, 2, 1, 3, 1);
		drawPrint(player, 1, 1, 2, 1);
		
		checkChainCount(0);
		checkCycleCount(0);
		checkDoubleNumber(0);
	}
}