package tests;

//Objects
import graph.Graph;

//Testing
import org.junit.Test;
import static org.junit.Assert.*;

public class GraphTesting
{
	Graph grid;
	
	//helpers
	private void setup(int numVertices)
	{
		grid = new Graph(numVertices);
	}
	
	private void assertSetUp(int numVertices)
	{
		setup(numVertices);
		assertTrue(grid != null);
	}
	
	private void addMirrorsEdge(int a, int b, int weight)
	{
		grid.addEdge(a, b, weight);
		grid.addEdge(b, a, weight);
	}
	
	private void removeMirrosEdge(int a, int b)
	{
		grid.removeEdge(a, b);
		grid.removeEdge(b, a);
	}
	
	private void println(String stuff)
	{
		System.out.println(stuff);
	}
	
	private void print(String stuff)
	{
		System.out.print(stuff);
	}
	
	private void printGrid(int[][] theGrid)
	{
		int height = theGrid.length;
		int width = theGrid.length;
		for(int stepA = 0; stepA < height; stepA++)
		{
			for(int stepB = 0; stepB < width; stepB++)
			{
				print("" + theGrid[stepA][stepB] + ", ");
			}
			println("");
		}
	}
	
	private void assertIsEdge(int vertex, int link, boolean toBeOrNot)
	{
		boolean edgecheck = grid.isEdge(vertex, link);
		assertTrue(edgecheck == toBeOrNot);
	}
	
	//testers
	@Test
	public void construction()
	{
		assertSetUp(5);
	}
	
	@Test
	public void addEdges()
	{
		assertSetUp(5);
		
		//zero based minus one
		
		//one
		grid.addEdge(0, 1, 1);
		
		//two
		grid.addEdge(1, 0, 1);
		grid.addEdge(1, 2, 1);
		
		//three
		grid.addEdge(2, 1, 1);
		grid.addEdge(2, 3, 1);
		
		//four
		grid.addEdge(3, 2, 1);
		grid.addEdge(3, 4, 1);
		
		//five
		grid.addEdge(4, 3, 1);
		
		
		//Test?
		println("=================");
		printGrid(grid.getGrid());
		println("=================");
	}
	
	@Test
	public void removeEdges()
	{
		assertSetUp(5);
		
		//zero based minus one
		
		//one
		grid.addEdge(0, 1, 1);
		
		//two
		grid.addEdge(1, 0, 1);
		grid.addEdge(1, 2, 1);
		
		//three
		grid.addEdge(2, 1, 1);
		grid.addEdge(2, 3, 1);
		
		//four
		grid.addEdge(3, 2, 1);
		grid.addEdge(3, 4, 1);
		
		//five
		grid.addEdge(4, 3, 1);
		
		
		//Test?
		println("=================");
		println("Before Remove:");
		printGrid(grid.getGrid());
		println("=================");
		
		
		//remove
		grid.removeEdge(1, 2);
		grid.removeEdge(2, 1);
		
		grid.removeEdge(3, 4);
		grid.removeEdge(4, 3);
		
		println("=================");
		println("After Remove:");
		printGrid(grid.getGrid());
		println("=================");
		
	
	}

	@Test
	public void checkEdge()
	{
		assertSetUp(5);
		
		//zero based minus one
		
		//one
		grid.addEdge(0, 1, 1);
		
		//two
		grid.addEdge(1, 0, 1);
		grid.addEdge(1, 2, 1);
		
		//three
		grid.addEdge(2, 1, 1);
		grid.addEdge(2, 3, 1);
		
		//four
		grid.addEdge(3, 2, 1);
		grid.addEdge(3, 4, 1);
		
		//five
		grid.addEdge(4, 3, 1);
		
		assertIsEdge(1,0, true);
		assertIsEdge(2,0, false);
	}

}
