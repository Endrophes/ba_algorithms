package exhaustiveDecoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import edu.neumont.nlp.DecodingDictionary;
import edu.neumont.nlp.MorseCodeEncoder;
public class ExhaustiveDecoder {

	DecodingDictionary dd;
	List<String>answersGiven;
	List<finalResults> finalPrime;
	float thresHold;
	float divisor;
	
	private class finalResults implements Comparable<finalResults>
	{
		String sentence;
		float value;
		@Override
		public int compareTo(finalResults arg0) {
			//0 same
			//-1 lower
			//1 higher
			int result = 0;
			
			if(arg0.value > this.value)
			{
				result = 1;
			}
			else if(arg0.value < this.value)
			{
				result = -1;
			}
			
			return result;
		}
		
		@Override
		public String toString()
		{
			String result = sentence + " grade: " + value;
			return result;
		}
		
		public finalResults()
		{
			
		}
		
		public finalResults(String newSentence)
		{
			sentence = newSentence;
			value = checkFrequency (newSentence);
		}
		
		public finalResults(String newSentence, float val)
		{
			sentence = newSentence;
			value = val;
		}
	}
	
	ExhaustiveDecoder(DecodingDictionary newDic, float breakValue, float divisorValue)
	{
		dd = newDic;
		if(breakValue == 0 && breakValue < -1)
		{
			thresHold = 0.0001f;
		}
		else
		{
			thresHold = breakValue;
		}
		
		if(divisor == 0 && divisor < -1)
		{
			divisor = 10000.0f;
		}
		else
		{
			divisor = divisorValue;
		}
		
		answersGiven = new ArrayList<String>();
		finalPrime = new ArrayList<finalResults>();	
	}
	
	public List<String> decode(String message)
	{
		String currentCode = message;
		FindWords(currentCode, "");
		finalEvaluation();
		return answersGiven;
	}
	
	private void FindWords(String currentCode, String sentence)
	{
		float value = checkFrequency(sentence);
		if(sentence.isEmpty() || value > thresHold)
		{
			if(currentCode.isEmpty()) 
			{
				finalPrime.add(new finalResults(sentence, value));
				//answersGiven.add(sentence);
				//System.out.println(sentence);
			}
			else
			{
				int index = 0;
				int length = currentCode.length();
				while(index <= length)
				{
					String currentChar = currentCode.substring(0,index);
					Set<String> possibleWords =  dd.getWordsForCode(currentChar);
					
					if(possibleWords != null)
					{
						String remainingMorseCode = currentCode.substring((index));
						
						for(String var : possibleWords)
						{
								String newSentence  = sentence + var + " ";
								FindWords(remainingMorseCode, newSentence);
						}
					}
					
					index++;
				}
			}
		}
		
		
	}
		
	private void finalEvaluation()
	{		
		if(finalPrime.size() != 0)
		{			
			answersGiven = new ArrayList<String>();
			Collections.sort(finalPrime);
			int size = finalPrime.size();
			int index = 0;
			
			while(index < 20 && index < size)
			{
				finalResults current = finalPrime.get(index);
				String temp = (index+1) + ". " + current.toString();
				answersGiven.add(temp);
				index++;
			}
			
			if(index < 20)
			{
				System.out.println("Input size maybe do to large or FreqThresh is to low");
			}
			
		}
		else
		{
			System.out.println("No results were found. Input size maybe do to large or FreqThresh is to low");
		}
	}
	
	private float checkFrequency(String sentence)
	{
		float result = 1;
		
		String[] split = sentence.split(" ");
		
		String firstWord = "";
		
		for(String var : split)
		{
			if(firstWord.isEmpty())
			{
				firstWord = var;
			}
			else
			{
				result *= (dd.frequencyOfFollowingWord(firstWord, var) / divisor);
				firstWord = var;
			}
		}
		
		return result;
	}
	
	
}
