package exhaustiveDecoder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import edu.neumont.nlp.DecodingDictionary;
import edu.neumont.nlp.MorseCodeEncoder;

public class decoderTest {

	DecodingDictionary dd;
	ExhaustiveDecoder decoder;
	List<String>answersGiven;
	List<finalResults> finalPrime;
	int threshHold;
	int FreqThresh;
	
	@Test
	public void testControll() throws Exception {
		String code = "...-...-..-....-....--.-.-.-.";
		String[] codes = new String[6];
		codes[0] = ".......---.----.-.....------..-.-.";
		codes[1] = "...-...-..-....-....--.-.-.-.";
		codes[2] = ".-......-........-....-.-...--...----..----..-.-.-.....----.-.---.-...--....";
		codes[3] = "-----....---.-.-.---------....";
		codes[4] = "-.-.---..--.-..--.-.....--.-...-...-.....-.-...--....";
		codes[5] = "...----.-.---..-.-...-..--......-.";
		//String secretCode = "....-.-.-.---..--.-..--..-...--..-.........-.....-.-.----.........-----.-.-.-...-.--..--..-...-..-..----...--.-...--..-.-.--.-.----.";
		dd = new DecodingDictionary();	
		ExhaustiveDecoder ed = new ExhaustiveDecoder(dd, 0.0001f, 10000.0f);
		answersGiven = ed.decode(codes[2]);
		printArrayList(answersGiven);
		
		//MorseCodeEncoder mce = new MorseCodeEncoder();
		//answersGiven = new ArrayList<String>();
		//finalPrime = new ArrayList<finalResults>();	
		//thr\eshHold = 100;
		//FreqThresh = 5;
		//testConcept1GettingFirstWord(code);
	}
	
	private void printArrayList(List<String> collection)
	{
		for(String var : collection)
		{
			System.out.println(var);
		}
	}
	
	private class finalResults implements Comparable<finalResults>
	{
		String sentence;
		float value;
		@Override
		public int compareTo(finalResults arg0) {
			//0 same
			//-1 lower
			//1 higher
			int result = 0;
			
			if(arg0.value > this.value)
			{
				result = 1;
			}
			else if(arg0.value < this.value)
			{
				result = -1;
			}
			
			return result;
		}
		
		@Override
		public String toString()
		{
			String result = sentence + " grade: " + value;
			return result;
		}
		
		public finalResults()
		{
			
		}
		
		public finalResults(String newSentence)
		{
			sentence = newSentence;
			value = checkFrequency (newSentence);
		}
	}
	
	private void testFrqencyOfWords(String firstWord, String lastWord)
	{
		int frq = dd.frequencyOfFollowingWord(firstWord, lastWord);
		System.out.println("Frqency: " + frq);
	}
	
	private void testingGetWordsForCode(String codeSample)
	{
		Set<String> words = dd.getWordsForCode(codeSample);
		if(words != null)
		{
			for(String var : words)
			{
				//System.out.println(var);
				answersGiven.add(var);
			}
		}
		else
		{
			System.out.println("No Word was found");
		}
	}
	
	private void testConcept1GettingFirstWord(String encoded)
	{
		String currentCode = encoded;
		List<String> possibleFirstWords = new ArrayList<String>();
		//FindWords(currentCode, possibleFirstWords, "");
		FindWords(currentCode, "");
		finalEvaluation();
	}
	
	private void finalEvaluation()
	{
		//for(String var : answersGiven)
		//{
		//	finalResults tron = new finalResults();
		//	tron.sentence = var;
		//	tron.value = checkFrequency (var);
		//	finalPrime.add(tron);
		//}
		
		if(finalPrime.size() != 0)
		{
			Collections.sort(finalPrime);
			int size = finalPrime.size();
			int index = 0;
			
			while(index < 20 && index < size)
			{
				finalResults current = finalPrime.get(index);
				System.out.println((index+1) + ". " + current.sentence + " grade: " + current.value);
				index++;
			}
			
			if(index < 20)
			{
				System.out.println("Input size maybe to large or FreqThresh is to low");
			}
			
		}
		else
		{
			System.out.println("No results were found. Input size maybe to large or FreqThresh is to low");
		}
	}
	
	private void FindWords(String currentCode, String sentence)
	{
		if(sentence.isEmpty() || checkFrequency(sentence) > 0.0001f)
		{
			if(currentCode.isEmpty()) 
			{
				finalPrime.add(new finalResults(sentence));
				//answersGiven.add(sentence);
				//System.out.println(sentence);
			}
			else
			{
				int index = 0;
				int length = currentCode.length();
				while(index <= length)
				{
					String currentChar = currentCode.substring(0,index);
					Set<String> possibleWords =  dd.getWordsForCode(currentChar);
					
					if(possibleWords != null)
					{
						String remainingMorseCode = currentCode.substring((index));
						
						for(String var : possibleWords)
						{
								String newSentence  = sentence + var + " ";
								FindWords(remainingMorseCode, newSentence);
						}
					}
					
					index++;
				}
			}
		}
	}
	
	private float checkFrequency(String sentence)
	{
		float result = 1;
		
		String[] split = sentence.split(" ");
		
		String firstWord = "";
		
		for(String var : split)
		{
			if(firstWord.isEmpty())
			{
				firstWord = var;
			}
			else
			{
				result *= (dd.frequencyOfFollowingWord(firstWord, var) / 10000.0f);
				firstWord = var;
			}
		}
		
		return result;
	}

	private Boolean evaluateArrayList(List<String> words)
	{
		Boolean results = false;
		
		int totalValue = 0;
		String lastWord = "";
		
		for(String var : words)
		{
			if(lastWord.length() != 0)
			{
				int frequency = dd.frequencyOfFollowingWord(lastWord, var);
				totalValue += (frequency / threshHold);
			}
			else
			{
				lastWord = var;
			}
		}
		
		if(totalValue >= threshHold)
		{
			results = true;
		}
		
		return results;
	}

}
