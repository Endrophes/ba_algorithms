package exponetial;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

public class ExponetialTest {

	private Exponetial exp = new Exponetial();
	
	@Test
	public void test() {
		Assert.assertEquals(1, exp.factorial(0));
		Assert.assertEquals(1, exp.factorial(1));
		Assert.assertEquals(6, exp.factorial(3));
		Assert.assertEquals(40320, exp.factorial(8));
	}

}
