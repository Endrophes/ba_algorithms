package exponetial;

public class Exponetial {

	/// n! n(n-1)*(n-2)....(2)*(1)
	public int factorial(int n)
	{
		int result = 1;
		
		if(n != 0 && n != 1)
		{
			while(n > 0)
			{
				result *= n;
				n -= 1;
			}
		}
		
		return result;
	}
	
	public int recurseFactorial(int n)
	{
		int result = 1;
		
		if(n != 0 && n != 1)
		{
			result = n * recurseFactorial(n - 1);
		}
		
		return result;
	} 
}
