package tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;

import edu.neumont.io.Bits;
import HuffmanTree.HuffmanCompressor;
import HuffmanTree.HuffmanTree;

public class HuffmanTreeTest {

	genericHelpers hp = new genericHelpers();
	HuffmanTree target;
	HuffmanCompressor compress;
	byte[] testCollection;
	
	public class valueWeight
	{
		byte value;
		float weight;
	}
	
	private void setup()
	{
		testCollection = new byte[4];
		testCollection[0] = 0;
		testCollection[1] = 0;
		testCollection[2] = 0;
		testCollection[3] = 0;
		
		printCurrentTestArray(testCollection);
		target = new HuffmanTree(testCollection);
	}
	
	private void printCurrentTestArray(byte[] array)
	{
		for(byte var : array)
		{
			System.out.print(var + ", ");
		}
		System.out.println();
	}
	
	//@Test
	public void buildtest() {
		setup();
		printCurrentTestArray(testCollection);
		target = new HuffmanTree(testCollection);
		assertTrue(target != null);
	}
	
	//@Test
	public void listValtest() {
		setup();
		target.printList();
		assertTrue(target != null);
	}
	
	//@Test
	public void listBulder() {
		testCollection = new byte[4];
		testCollection[0] = 1;
		testCollection[1] = 2;
		testCollection[2] = 3;
		testCollection[3] = 3;
		
		printCurrentTestArray(testCollection);
		target = new HuffmanTree(testCollection);
		target.printList();
		
		assertTrue(target != null);
	}
	
	//@Test
	public void getTreeInOrder()
	{
		testCollection = new byte[4];
		testCollection[0] = 1;
		testCollection[1] = 2;
		testCollection[2] = 3;
		testCollection[3] = 3;
		
		//printCurrentTestArray(testCollection);
		target = new HuffmanTree(testCollection);
		
		ArrayList<Byte> results = target.getInorder();
		
		for(Byte var : results)
		{
			hp.println(var+"");
		}
		
		assertTrue(target != null);
	}
	
	//@Test
	//@Test
	public void getBitsTest()
	{
		testCollection = new byte[4];
		testCollection[0] = 1;
		testCollection[1] = 2;
		testCollection[2] = 3;
		testCollection[3] = 3;
		
		target = new HuffmanTree(testCollection);
		
		Bits results = new Bits();
		
		target.fromByte((byte)3, results);
		
		for(Boolean var : results)
		{
			hp.print(var + ", ");
		}
		hp.println("");
		results.clear();
		
		target.fromByte((byte)1, results);
		
		for(Boolean var : results)
		{
			hp.print(var + ", ");
		}
		hp.println("");
		results.clear();
		
		target.fromByte((byte)2, results);
		
		for(Boolean var : results)
		{
			hp.print(var + ", ");
		}
		hp.println("");
		results.clear();
		
		assertTrue(target != null);
	}

	//@Test
	//@Test
	public void getByteTest()
	{
		testCollection = new byte[4];
		testCollection[0] = 1;
		testCollection[1] = 2;
		testCollection[2] = 3;
		testCollection[3] = 3;
		target = new HuffmanTree(testCollection);
		
		Bits code = new Bits();
		byte result = 0;
		
		code.clear();
		code.add(false);
		result = target.toByte(code);
		assertTrue(result == 3);
		hp.println(result + "");
		
		code.clear();
		code.add(true);
		code.add(false);
		result = target.toByte(code);
		assertTrue(result == 1);
		hp.println(result + "");

		code.clear();
		code.add(true);
		code.add(true);
		result = target.toByte(code);
		assertTrue(result == 2);
		hp.println(result + "");
	}

	//@Test
	public void compresserTest()
	{
		testCollection = new byte[4];
		testCollection[0] = 1;
		testCollection[1] = 2;
		testCollection[2] = 3;
		testCollection[3] = 3;
		target = new HuffmanTree(testCollection);
		compress = new HuffmanCompressor();
		
		testCollection = new byte[13];
		testCollection[0] = 1;
		testCollection[1] = 1;
		testCollection[2] = 1;
		testCollection[3] = 1;
		
		testCollection[4] = 2;
		testCollection[5] = 2;
		testCollection[6] = 2;
		
		testCollection[7] = 3;
		testCollection[8] = 3;
		testCollection[9] = 3;
		testCollection[10] = 3;
		testCollection[11] = 3;
		testCollection[12] = 3;
		
		byte[] result = compress.compress(target, testCollection);
		
		for(byte var : result)
		{
			hp.print(var + ", ");
		}
		
		hp.println("");
		
		double resultSize = result.length;
		double originlSize = testCollection.length;
		hp.println("resultSize: " + resultSize);
		hp.println("originlSize: " + originlSize);
		double reduceby = originlSize - resultSize;
		double finalCompresstion = (resultSize / originlSize) * 100;
		finalCompresstion = Math.ceil(finalCompresstion);
		hp.println("Compression by: " + finalCompresstion + "%");
	}
	
	//@Test
	public void decompresserTest()
	{
		testCollection = new byte[4];
		testCollection[0] = 1;
		testCollection[1] = 2;
		testCollection[2] = 3;
		testCollection[3] = 3;
		target = new HuffmanTree(testCollection);
		compress = new HuffmanCompressor();
		
		testCollection = compress.compress(target, testCollection);
		
		testCollection = compress.decompress(target, 4, testCollection);
		
		for(byte var : testCollection)
		{
			hp.print(var + ", ");
		}
		
	}

	private int[] getIntArray()
	{
		return new int[] {423, 116, 145, 136, 130, 165, 179, 197, 148, 125, 954, 156, 143, 145, 164, 241, 107, 149, 176, 153, 121, 164, 144, 166, 100, 138, 157, 140, 119, 138, 178, 289, 360, 120, 961, 195, 139, 147, 129, 192, 119, 146, 138, 184, 137, 196, 163, 331, 115, 160, 127, 172, 176, 181, 149, 194, 138, 154, 163, 167, 196, 174, 250, 354, 142, 169, 170, 209, 205, 179, 147, 245, 108, 179, 148, 186, 131, 160, 112, 219, 118, 204, 164, 154, 154, 175, 189, 239, 126, 145, 185, 179, 149, 167, 152, 244, 189, 257, 234, 208, 179, 170, 171, 178, 184, 189, 203, 184, 204, 208, 187, 163, 335, 326, 206, 189, 210, 204, 230, 202, 415, 240, 275, 295, 375, 308, 401, 608, 2099, 495, 374, 160, 130, 331, 107, 181, 117, 133, 476, 129, 137, 106, 107, 237, 184, 143, 122, 143, 1596, 205, 121, 170, 123, 124, 150, 132, 143, 133, 178, 308, 96, 102, 114, 176, 159, 149, 123, 199, 1156, 119, 144, 237, 131, 155, 143, 225, 92, 125, 117, 138, 135, 154, 124, 137, 121, 143, 149, 141, 177, 159, 247, 384, 302, 120, 95, 140, 87, 1460, 155, 199, 111, 198, 147, 182, 91, 148, 119, 233, 445, 1288, 138, 133, 122, 170, 156, 257, 143, 149, 180, 174, 132, 151, 193, 347, 91, 119, 135, 182, 124, 152, 109, 175, 152, 159, 166, 224, 126, 169, 145, 220, 119, 148, 133, 158, 144, 185, 139, 168, 244, 145, 167, 167, 262, 214, 293, 402};
	}
	
	@Test
	public void finalTest() throws IOException
	{
		int[] control = getIntArray();
		
		target = new HuffmanTree(control);
		compress = new HuffmanCompressor();
		
		
		Path file1 = Paths.get("D:\\compressed.huff");
		Files.readAllBytes(file1);
		
		byte[] b = Files.readAllBytes(file1);
		byte[] result = compress.decompress(target, 54679, b);
		
	    Path File2 = Paths.get("D:\\FinalTest" + ".jpg");
	    Files.write(File2, result);
	}
	
	//@Test
	public void bitCheck()
	{
		int[] control = getIntArray();
		
		target = new HuffmanTree(control);
		
		target.printQueue();
	}
	
	private int unsignedToBytes(byte b) 
	 {
	    return b & 0xFF;
	 }
	
	//@Test
	public void conceptTest()
	{
		int result = unsignedToBytes((byte)-80);
		hp.print("result: " + result);
	}
	
	
}