package tests;

public class genericHelpers {
	
	//Helper Functions
	public static void println(String message)
	{
		System.out.println(message);
	}
	
	public static void print(String message)
	{
		System.out.print(message);
	}
	
	public static <T> void printArray(T[] array)
	{
		for(T var : array)
		{
			System.out.print(var + ", ");
		}
	}
}
