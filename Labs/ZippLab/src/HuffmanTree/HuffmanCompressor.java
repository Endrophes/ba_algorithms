package HuffmanTree;

import java.util.ArrayList;
import java.util.List;

import edu.neumont.io.Bits;

public class HuffmanCompressor {
	
	private int binaryToByte(String binaryCode)
	{
		int length = binaryCode.length();
		int lastIndex = length - 1;
		int maxValue = 1;
		int decimal = 0;
		
		for(int stepper = 0; stepper < length; stepper++)
		{
			char bit = binaryCode.charAt(lastIndex - stepper);
			if(bit == '1')
			{
				decimal += maxValue;
			}
			
			//set for the next step
			maxValue *= 2;
		}
		
		return decimal;
	}
	
	public byte[] compress(HuffmanTree tree, byte[] b)
	{
		ArrayList<Byte> results = new ArrayList<Byte>();
		
		Bits fullSet = new Bits();
		
		for(byte var : b)
		{
			try{
				tree.fromByte(var, fullSet);
			}catch(NullPointerException e)
			{
				
			}
			
		}
		
		int size = fullSet.size();
		
		while(size < 8)
		{
			fullSet.addLast(false);
			size = fullSet.size();
		}
		
		int sections = (int) Math.floor(size/8);
		
		int end = sections + 1;
		
		//System.out.println("sections: " + sections);
		
		for(int split = 0; split < sections; split++)
		{
			int startIndex = (split * 8);
			int endIndex = (split + 1) * 8;
			
			//System.out.println("startIndex: " + startIndex + " endIndex: "  + endIndex);
			
			List<Boolean> section = new ArrayList<Boolean>();
			
			for(int step = startIndex; step < endIndex; step++)
			{
				section.add(fullSet.get(step));
			}
			
			String binaryRep = "";
			
			for(Boolean var : section)
			{
				if(var)
				{
					binaryRep += "1";
				}
				else
				{
					binaryRep += "0";
				}
			}
			
			int intResult = binaryToByte(binaryRep);
			int half = intResult;
			results.add((byte)half);
		}
		
		
		int max = results.size();
		
		byte[] finalResults = new byte[max];
		
		for(int step = 0; step < max; step++)
		{
			finalResults[step] = results.get(step);
		}
		
		return finalResults;
	}
	
	private String decToBinary(int number)
	{
		String result = "";
		
		if(number == 0)
		{
			result = "0";
		}
		else
		{
			while(number != 0)
			{
				result += (number % 2);
				number = number/2;
			}
			
			String reverse = "";
			
			int index = result.length() - 1;
			
			while(index >= 0)
			{
				reverse += result.charAt(index--);
			}
			
			result = reverse;
		}
		
		return result;
	}
	
	public byte[] decompress(HuffmanTree tree, int uncompressedLength, byte[] b)
	{
		//Convert to Bits
		String fullBits = "";
		
		for(byte var : b)
		{
			fullBits += String.format("%8s", Integer.toBinaryString(var & 0xFF)).replace(' ', '0');
		}
		
		Bits bits = new Bits();
		int size = fullBits.length();
		
		for(int step = 0; step < size; step++)
		{
			char bit = fullBits.charAt(step);
			
			if(bit == '1')
			{
				bits.add(true);
			}
			else
			{
				bits.add(false);
			}
		}
		
		//convert to byte
		ArrayList<Byte> convertToBytes = new ArrayList<Byte>();
		Bits byteToBits = new Bits();
		int currentCount = 0;
		int stop = bits.size() / 2 ;
		
		while(bits.size() > 0 && convertToBytes.size() < uncompressedLength)
		{
			convertToBytes.add(tree.toByte(bits));
		}
		
		Object[] current = convertToBytes.toArray();
		byte[] finalResults = new byte[current.length];
		
		for(int step = 0; step < current.length; step++)
		{
			finalResults[step] = (byte) current[step];
		}
		
		return finalResults;
	}

	
}
