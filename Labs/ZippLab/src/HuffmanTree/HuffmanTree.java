package HuffmanTree;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import edu.neumont.io.Bits;

public class HuffmanTree {
	
	private class Node implements Comparable<Node>
	{
		public Byte value;
		public float frquency = 1.0f;
		
		public Node left;
		public Node right;
		
		Boolean[] bits;
		
		Node()
		{
			value = 0;
			frquency = 1;
		}
		
		Node(float newFrq)
		{
			value = 0;
			frquency = newFrq;
		}
		
		Node(Byte newVal)
		{
			value = newVal;
			frquency = 1.0f;
		}
		
		@Override
		public int compareTo(Node o) {
			float frqA = frquency;
			float frqB = o.frquency;
			
			int result = Float.compare(frqA, frqB);
			
			
			
			return result;
		}
		
		@Override
		public String toString()
		{
			String result = "";
			if(bits != null)
			{
				result = "Value: " + value + " Frequency: " + frquency + " Num Bits:" + bits.length;
			}
			else
			{
				result = "Value: " + value + " Frequency: " + frquency;
			}
			return result;
		}

	}
	
	Node root;
	
	PriorityQueue<Node> ordered = new PriorityQueue<Node>();
	ArrayList<Node> collectionOfInfo = new ArrayList<Node>();
	Map<Byte, Node> arragedNodes = new HashMap<Byte, Node>();
	Map<Boolean[], Node> NodesByBytes = new HashMap<Boolean[], Node>();
	
	int[] values = new int[256];
	
	public void printList()
	{
		for(Node var : collectionOfInfo)
		{
			//System.out.println(var.toString());
		}
	}

	public ArrayList<Byte> getInorder()
	{
		ArrayList<Byte> collection = new ArrayList<Byte>();
		Node start = root;
		
		traverseInOrder(start, collection);
		
		return collection;
	}
	
	private void traverseInOrder(Node currentNode, ArrayList<Byte> values)
	{
		if(currentNode.left != null)
		{
			traverseInOrder(currentNode.left, values);
		}
		
		values.add(currentNode.value);
		
		if(currentNode.right != null)
		{
			traverseInOrder(currentNode.right, values);
		}
	}
	
	private void traversePreOrder(Node currentNode, ArrayList<Byte> values)
	{
		values.add(currentNode.value);
		
		if(currentNode.left != null)
		{
			traverseInOrder(currentNode.left, values);
		}
		
		if(currentNode.right != null)
		{
			traverseInOrder(currentNode.right, values);
		}
	}
	
	private void traversePostOrder(Node currentNode, ArrayList<Byte> values)
	{
		if(currentNode.left != null)
		{
			traverseInOrder(currentNode.left, values);
		}
		
		if(currentNode.right != null)
		{
			traverseInOrder(currentNode.right, values);
		}
		
		values.add(currentNode.value);
	}
	
	public void printQueue()
	{
		Set<Byte> setOfKeys = arragedNodes.keySet();
		for ( Byte key : setOfKeys ) 
		{
			Node currentNode = arragedNodes.get(key);
			System.out.println(currentNode.toString());
		}
	}
	
	//byte is from 0 to 256
 	public HuffmanTree(byte[] array)
	{		
		addValuesB(array);
		calculateFrequencyB(array.length);
		buildTree();
		setupDictionary();
	}
 	
 	public HuffmanTree(int[] array)
 	{
 		//This is for testing
 		for(int step = -128; step < 128; step++)
 		{
 			Node temp = new Node();
 			temp.value = (byte) step;
 			int trueStep = step + 128;
 			float frq = array[trueStep];
 			temp.frquency = frq;
 			ordered.add(temp);	
 		}
 		buildTree();
		setupDictionary();
 	}

 	private void setupDictionary()
 	{
 		binaryTraverserWriter(root, new Bits());
 	}
 	
 	private void binaryTraverserWriter(Node currentNode, Bits b)
	{
 		//Brake refrence in better way
 		if(currentNode.left == null && currentNode.right == null)
		{
 			//found leaf
 			int size = b.size();
 			Boolean[] cyberBits = new Boolean[size];
 			for(int step = 0; step < size; step++)
 			{
 				cyberBits[step] = b.get(step);
 			}
 			
 			currentNode.bits = cyberBits;
 	 		arragedNodes.put(currentNode.value, currentNode);
 	 		NodesByBytes.put(currentNode.bits, currentNode);
		}
 		else
 		{	 			
			if(currentNode.left != null)//go left
			{
				b.add(false);
				binaryTraverserWriter(currentNode.left, b);
				b.pollLast();
			}
			
			if(currentNode.right != null)//go right
			{
				b.add(true);
				binaryTraverserWriter(currentNode.right, b);
				b.pop();
			}
 		}
	}
 	
	private void addValuesB(byte[] array)
	{
		for(byte var : array)
		{
			int index = (var + 128);
			values[index] += 1;
		}
	}
 	
	private void calculateFrequencyB(int sizeOforignialCollection)
	{
		for(int step = 0; step < 256; step++)
		{
			if(values[step] != 0)
			{
				Node temp = new Node();
				int trueValue = step - 128;
				temp.value = (byte) trueValue;
				float frq = ((float)values[step]/(float)sizeOforignialCollection);
				temp.frquency = frq;
				ordered.add(temp);
			}
		}
	}
	
 	private void addValues(byte[] array)
	{
		for(byte var : array)
		{
			Node temp = new Node();
			temp.value = var;
			temp.frquency = 1.0f;
			int collectionsSize = collectionOfInfo.size();
			boolean valueNotFound = true;
			
			for(int step = 0; step < collectionsSize && valueNotFound; step++)
			{
				Node currentNode = collectionOfInfo.get(step);
				if(currentNode.value.compareTo(temp.value) == 0)
				{
					valueNotFound = false;
					currentNode.frquency += 1;
				}
			}
			
			if(valueNotFound)
			{
				collectionOfInfo.add(temp);
			}
		}
	}
	
	private void calculateFrequency(int sizeOforignialCollection)
	{
		for(Node var : collectionOfInfo)
		{
			var.frquency = (var.frquency /  sizeOforignialCollection);
			ordered.add(var);
		}
	}
	
	private int getSize()
	{
		
		return 0;
	}
	
	private void buildTree()
	{
		if(ordered.size() == 1)
		{
			root = ordered.poll();
		}
		else
		{
			builder();
		}
	}
	
	private void builder()
	{
		if(ordered.size() == 2)
		{
			Node left = ordered.poll();
			Node right = ordered.poll();
			
			Node head = new Node(left.frquency + right.frquency);
			head.left = left;
			head.right = right;
			
			root = head;
		}
		else
		{
			Node left = ordered.poll();
			Node right = ordered.poll();
			
			Node head = new Node(left.frquency + right.frquency);
			
			head.left = left;
			head.right = right;
			
			ordered.add(head);
			builder();
		}
	}
	
	public byte toByte(Bits bits)
	{	
		byte result = 0;
		
		result = toByteHelper(root, bits);
		
		return result;
	}
	
	private byte toByteHelper(Node currentNode, Bits bits)
	{
		byte result = 0;
		if(bits.size() < 0 || (currentNode.left == null && currentNode.right == null))
		{
			result = currentNode.value;
		}
		else
		{
			Boolean right = bits.poll();
			
			if(right)
			{
				result = toByteHelper(currentNode.right, bits);
			}
			else
			{
				result = toByteHelper(currentNode.left, bits);
			}
		}
		
		return result;
	}
	
	public void fromByte(byte b, Bits bits)
	{
		//binaryTraverserWriter(root, bits, b);
		Node info = arragedNodes.get(b);
		for(Boolean var : info.bits )
		{
			bits.add(var);
		}
	}
	
}
